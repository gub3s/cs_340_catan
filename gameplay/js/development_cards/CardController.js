//STUDENT-EDITABLE-BEGIN
/**
    This is the namespace for development cards
    @module catan.devCards
    @namespace devCards
*/

var catan = catan || {};
catan.devCards = catan.devCards || {};

catan.devCards.Controller = (function(){

	var Controller = catan.core.BaseController;
	var Definitions = catan.definitions;
	
	var DevCardController = (function card_namespace(){
		
		core.forceClassInherit(DevCardController,Controller);

		core.defineProperty(DevCardController.prototype, "BuyView");

		var _soldierAction;
		var _roadAction;


		/**
		 * @class DevCardController
		 * @constructor
		 * @extends misc.BaseController
		 * @param {devCards.DevCardView} view
		 * @param {devCards.BuyCardView} buyView
		 * @param {models.ClientModel} clientModel
		 * @param {function} soldierAction
		 * @param {function} roadAction
		 */
		function DevCardController(view, buyView, clientModel, soldierAction, roadAction){
			Controller.call(this,view,clientModel);
			this.setBuyView(buyView);
			_soldierAction = soldierAction;
			_roadAction = roadAction;
		}

		DevCardController.prototype.updateModel = function(model)
		{
			var thisPlayer = catan.models.ClientModel.playerModel();
			var thisPlayerObj = catan.models.ClientModel.playerObj();

			var counts = {};
			for (var card in thisPlayer.newDevCards)
				counts[card] = thisPlayer.newDevCards[card];
			for (var card in thisPlayer.oldDevCards)
				counts[card] += thisPlayer.oldDevCards[card];

			var canPlayCard = catan.models.ClientModel.isLocalsTurn()
								&& !catan.models.ClientModel.playedDevCardThisTurn;
			var canPlayMonument = (thisPlayer.newDevCards.monument || thisPlayer.oldDevCards.monument)
									&& catan.models.ClientModel.isLocalsTurn();

			for (var card in thisPlayer.oldDevCards)
				this.View.updateAmount(card, counts[card]);
			for (var card in thisPlayer.oldDevCards)
				this.View.setCardEnabled(card, thisPlayer.oldDevCards[card] && canPlayCard);
			this.View.setCardEnabled("monument", canPlayMonument);
		};
		
		/**
		 * Called when the player buys a development card
		 * @method buyCard
		 * @return void
		 */
		DevCardController.prototype.buyCard = function(){
			catan.serverProxy.buyDevCard(catan.models.ClientModel.orderNumber());
		}
        
		/**
		 * Called when the player plays a year of plenty card
		 * @method useYearOfPlenty
		 * @param {String} resource1 The first resource to obtain
		 * @param {String} resource2 The second resource to obtain
		 * @return void
		 */
		DevCardController.prototype.useYearOfPlenty = function(resource1, resource2){
			catan.serverProxy.yearOfPlenty(catan.models.ClientModel.orderNumber(), resource1, resource2);
			catan.models.ClientModel.playedDevCardThisTurn = true;
			this.View.closeModal();
		}
        
		/**
		 * Called when the player plays a monopoly card
		 * @method useMonopoly
		 * @param {String} resource the resource to obtain
		 * @return void
		 */
		DevCardController.prototype.useMonopoly= function(resource){
			catan.serverProxy.monopoly(catan.models.ClientModel.orderNumber(), resource);
			catan.models.ClientModel.playedDevCardThisTurn = true;
			this.View.closeModal();
		}
        
		/**
		 * Called when the player plays a monument card
		 * @method useMonument
		 * @return void
		 */
		DevCardController.prototype.useMonument = function(){
			catan.serverProxy.monument(catan.models.ClientModel.orderNumber());
			this.View.closeModal();
		}
        
		/**
		 * Called when the player plays a soldier card
		 * @method useSoldier
		 * @return void
		 */
		DevCardController.prototype.useSoldier= function(){
			catan.models.ClientModel.playedDevCardThisTurn = true;
			catan.models.ClientModel.usingSoldier = true;
			_soldierAction();
			this.View.closeModal();
		}
        
		/**
		 * Called when the player plays the road building card
		 * @method useRoadBuild
		 * @return void
		 */
		DevCardController.prototype.useRoadBuild = function(resource){
			catan.models.ClientModel.playedDevCardThisTurn = true;
			catan.models.ClientModel.freeRoads = 2;
			_roadAction();
			this.View.closeModal();
		}

		return DevCardController;
	}());
	
	return DevCardController;
}());


//STUDENT-EDITABLE-BEGIN

// STUDENT EDIT:
// We have opted not to use this class. #kthxbye #suchnotuse #verydiscard

/**
	This module contains the top-level client model class
	
	@module		catan.models
	@namespace models
*/

var catan = catan || {};
catan.models = catan.models || {};

catan.models.ClientModel  = (function clientModelNameSpace(){
    /** 
	This the top-level client model class that contains the local player, map contents, etc.
	
	@class ClientModel
	@constructor
	@param {integer} playerID The id of the local player, extracted from the cookie
    */
	var ClientModel = (function ClientModelClass(){

		var _playerID, _interval, _orderNumber, _lastRevision, _playerObj;
		var _model = {};
        
		function ClientModel(playerID){
			_playerID = playerID;
		}
        
        /**
         * This is called to fetch the game state from the server the very first time.
         * It should: 1) fetch the game state JSON from the server, 2) update the client model with the
         * returned data, 3) notify all client model listeners that the model has changed, and 4) invoke
         * the success callback function with the object received from the server.
         * 
         * @method initFromServer
         * @param {function} success - A callback function that is called after the game state has been fetched from the server and the client model updated. This function is passed a single parameter which is the game state object received from the server.
         * */
		ClientModel.prototype.initFromServer = function(success)
		{
			var pollSuccess = function(data)	// This happens after EVERY successful call to the server
			{
				if (data.revision === _lastRevision)
					return;
				
				_model = data;
				_lastRevision = data.revision;
				
				for (var player in data.players)
				{
					if (data.players[player].playerID === _playerID)
					{
						_orderNumber = data.players[player].orderNumber;
						break;
					}
				}

				if (!_playerObj)
					_playerObj = new catan.game.Player(data.players[_orderNumber]);
				else
					_playerObj.update(data.players[_orderNumber]);

				$(catan).trigger('newmodel', data);
				catan.map2.update(data);
			};

			var successOneTime = function(data)	// This happens after only the initialization
			{
				catan.map2 = new catan.map.Map(data);
				pollSuccess(data);
				_lastRevision = undefined;
				success(data);
			};

			var failure = function(jqxhr, status, msg)
			{
				if (!msg)
					console.log("No response from server; it may have stopped.");
				else
				{
					alert("Wow. Very error. So mistake: " + status + " (" + msg + ")");
					location.reload();
				}
			};

			new catan.game.ServerProxy(successOneTime, failure).model();
			catan.serverProxy = new catan.game.ServerProxy(pollSuccess, failure);
			_interval = setInterval(catan.serverProxy.model, 1500);
		};

		ClientModel.prototype.playerID = function() { return _playerID; };
		ClientModel.playerID = function() { return _playerID; }

		ClientModel.prototype.orderNumber = function() { return _orderNumber; };
		ClientModel.orderNumber = function() { return _orderNumber; };

		ClientModel.prototype.model = function() { return _model; };
		ClientModel.model = function() { return _model; };

		ClientModel.prototype.playerModel = function() { return _model.players[_orderNumber]; };
		ClientModel.playerModel = function() { return _model.players[_orderNumber]; };

		ClientModel.playerObj = function() { return _playerObj; };

		ClientModel.isLocalsTurn = function() { return _model.turnTracker.currentTurn == _orderNumber; };
        
		return ClientModel;
	}());
	
	return ClientModel;
}());


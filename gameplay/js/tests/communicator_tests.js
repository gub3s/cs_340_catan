// Tests for Communicator.js
var host = 'localhost';
var port = 8081;

test("Constructor", function () {
	var communicator = new catan.game.Communicator(host, port, function() {}, function() {});
	ok(communicator.serverURL === "localhost", "Host is set");
	ok(communicator.serverPort === 8081, "Port is set");
	ok(typeof communicator.success === 'function', "Success callback function is set");
	ok(typeof communicator.failure === 'function', "Failure callback function is set");
});

asyncTest("Test Communicator GET Should Succeed", function(){
	expect(1);

	var communicator = new catan.game.Communicator(host, port, function(){
		console.log('Success');
		ok(true, "Request Succeeded!");
		start();
	}, function() {
		console.log('Fail');
		ok(false, "Request Failed! It should have passed");
		start();
	});
	
	communicator.get('/games/list');
});

asyncTest("Test Communicator GET Should Not Succeed", function(){
	expect(1);

	var communicator = new catan.game.Communicator(host, port, function() {
		ok(false, "Request Succeeded! It should have failed");
		start();
	}, function() {
		ok(true, "Request Failed!");
		start();
	});

	communicator.get('/bad/url');
});

asyncTest("Test Communicator POST Should Succeed", function(){
	expect(1);

	var communicator = new catan.game.Communicator(host, port, function(){
		ok(true, "Request Succeeded!");
		start();
	}, function() {
		ok(false, "Request Failed! It should have Succeeded!");
		start();
	});

	communicator.postForm('/user/login', {username : 'Brooke', password : 'brooke'}, function(){
		communicator.postForm('/games/join', {color : 'red', id : '0'}, function(){
			communicator.post('/moves/sendChat', "{type : 'sendChat', playerIndex : 0, content : 'This is a good chat with good url'}");
		}, function(){});
	}, function(){});
});


asyncTest("Test Communicator POST Should Not Succeed (Good Message, Bad URL)", function(){
	expect(1);

	var communicator = new catan.game.Communicator(host, port, function(){
		ok(false, "Request Succeeded! It should have failed");
		start();
	}, function() {
		ok(true, "Request Failed!");
		start();
	});

	var data = "{type: 'sendChat', playerIndex: 0, content: 'This is a good chat with bad url'}";
	communicator.post('/bad/url',data);
});

asyncTest("Test Communicator POST Should Not Succeed (Bad Message, Good URL)", function(){
	expect(1);

	var communicator = new catan.game.Communicator(host, port, function(){
		ok(false, "Request Succeeded! It should have failed");
		start();
	}, function() {
		ok(true, "Request Failed!");
		start();
	});

	var data = "{type: 'sendChat', playerIndex: -1, content: 'This is a bad chat with good url'}";
	communicator.post('/moves/sendChat', data);
});

asyncTest("Test Communicator POST Should Not Succeed (Bad Message, Bad URL)", function(){
	expect(1);

	var communicator = new catan.game.Communicator(host, port, function(){
		ok(false, "Request Succeeded! It should have failed");
		start();
	}, function() {
		ok(true, "Request Failed!");
		start();
	});

	var data = "{type: 'sendChat', playerIndex: -1, content: 'This is a bad chat with bad url'}";
	communicator.post('/bad/url', data);
});
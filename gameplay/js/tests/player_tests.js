var def = catan.definitions;

module("Player", {
	setup: function()
	{
		player = new catan.game.Player(playerModel);
	},
	teardown: function()
	{
		catan.game.TurnTracker.currentTurn = -1;
	}
});

test("Instantiation", function()
{
	expect(2);
	var player = new catan.game.Player(playerModel);

	ok(typeof player === 'object',
		"Returned an object");

	ok(typeof player.canUseDevCard === 'function',
		"Has visible methods");
});

test("Development cards", function()
{
	expect(4);

	player.model.playedDevCard = false;

	ok(player.canUseDevCard(def.MONUMENT) === true,
		"Can play Monument dev card");

	ok(player.canUseDevCard(def.ROAD_BUILD) === false,
		"Can NOT play a Road Building dev card");

	player.model.playedDevCard = true;

	ok(player.canUseDevCard(def.MONUMENT) === false,
		"Having already played a development card, can NOT play Monument card");

	throws(function() { player.canUseDevCard("bad type"); },
		"Throws when trying to play a bad dev card");

	// Reset
	player.model.playedDevCard = false;
});

test("Resource cards", function()
{
	expect(15);

	var has = player.hasResources({
		"brick": 1,
		"wood": 0,
		"sheep": -5,
		"wheat": 3,
		"ore": 2
	});
	ok(has.brick === true, "Has 1 brick");
	ok(has.wood === true, "Has 0 wood");
	ok(has.sheep === true, "Has -5 sheep");
	ok(has.wheat === true, "Has 3 wheat");
	ok(has.ore === true, "Has 2 ore");

	var has = player.hasResources({
		"brick": 14,
		"wood": 13,
		"sheep": 15,
		"wheat": 10,
		"ore": 8
	});
	ok(has.brick === true, "Has 14 brick");
	ok(has.wood === true, "Has 13 wood");
	ok(has.sheep === true, "Has 15 sheep");
	ok(has.wheat === true, "Has 10 wheat");
	ok(has.ore === true, "Has 8 ore");

	var has = player.hasResources({
		"brick": 99,
		"wood": 102,
		"sheep": 5
	});
	ok(has.brick === false, "Does NOT have 99 brick");
	ok(has.wood === false, "Does NOT have 102 wood");
	ok(has.sheep === true, "Has 5 sheep");

	var hasAll = player.hasAllResources({
		"brick": 99,
		"wood": 102,
		"sheep": 5
	});
	ok(!hasAll,
		"hasAllResources returns false when not all resources are had");

	var hasAll = player.hasAllResources({
		"brick": 1,
		"wood": 2,
		"sheep": 3
	});
	ok(hasAll,
		"hasAllResources returns true when all resources are had");
});

test("What the player can buy", function()
{
	expect(6);

	var buyable = player.canBuy();
	ok(buyable.road, "Can buy road");
	ok(buyable.settlement, "Can buy settlement");
	ok(buyable.city, "Can buy city");
	ok(buyable.devcard, "Can buy devcard");

	player.model.resources.brick = 0;
	buyable = player.canBuy();
	ok(!buyable.road, "With no brick, player can NOT buy road");

	player.model.cities = 4;
	buyable = player.canBuy();
	ok(!buyable.city,"With max cities, player can NOT buy city");

	// Reset
	player.model.resources.brick = 14;
	player.model.cities = 2;
});

test("Discarding", function()
{
	expect(3);

	ok(player.mustDiscard() == 30,
		"Player must discard with too many resource cards");

	player.model.resources.ore = 11;

	ok(player.mustDiscard() == 31,
		"mustDiscard rounds down");

	player.model.resources = {
		"brick": 1,
		"wood": 1,
		"sheep": 1,
		"wheat": 1,
		"ore": 3
	};

	ok(player.mustDiscard() == 0,
		"Player need not discard resources with less than 8 in hand");

	// Reset
	player.model.resources = {
		"brick": 14,
		"wood": 13,
		"sheep": 15,
		"wheat": 10,
		"ore": 8
	};
});

test("Dice roll", function()
{
	expect(11);

	throws(function() { player.roll(); },
		"Throws exception if it's not the player's turn");

	catan.game.TurnTracker.currentTurn = player.model.playerID;

	for (var i = 0; i < 10; i++)
		ok(player.roll() >= 2, "Roll should yield value between 2 and 12 inclusive");
});
/*
test("Collect resources", function()
{
	expect(1);

	return;	// TODO...

	player.collectResources(player.roll());
	
	var actual = player.getResources();
	
	var expected = {
		"brick": 15,
		"wood": 13,
		"sheep": 15,
		"wheat": 11,
		"ore": 8
	};

	ok(actual.brick == expected.brick,
		"Collected 1 brick resource");
	ok(actual.wood == expected.wood,
		"Collected 0 wood resources");
	ok(actual.sheep == expected.sheep,
		"Collected 0 sheep resources");
	ok(actual.wheat == expected.wheat,
		"Collected 1 wheat resource");
	ok(actual.ore == expected.ore,
		"Collected 0 ore resources");

	// TODO: reset
});
*/

test("Propose trade", function()
{
	expect(1);	// TODO: Test actual implementation

	var tradeWithPlayer = 1;
	var tradeRes = {
		brick: 1,
		sheep: 3
	};

	throws(function() { player.proposeTrade(tradeWithPlayer, tradeRes); },
		"Can't trade when it's not your or the other player's turn");

	catan.game.TurnTracker.currentTurn = player.model.playerID;

	// TODO: reset
});


test("Build", function()
{
	expect(1);	// TODO: Test actual implementation

	var what = "road";
	var details = {
		"type": "buildRoad",
		"playerIndex": -1,
		"roadLocation": {
			"x": -1,
			"y": -1,
			"direction": ""
		},
		"free": false
	};

	throws(function() { player.build(what, details); },
		"Can't trade when it's not your or the other player's turn");

	catan.game.TurnTracker.currentTurn = player.model.playerID;

	return;	// TODO...?

	// TODO: reset
});

var player;
var playerModel = {
	"MAX_GAME_POINTS": 10,
	"resources": {
		"brick": 14,
		"wood": 13,
		"sheep": 15,
		"wheat": 10,
		"ore": 8
	},
	"oldDevCards": {
		"yearOfPlenty": 0,
		"monopoly": 0,
		"soldier": 2,
		"roadBuilding": 0,
		"monument": 1
	},
	"newDevCards": {
		"yearOfPlenty": 0,
		"monopoly": 0,
		"soldier": 1,
		"roadBuilding": 1,
		"monument": 0
	},
	"roads": 8,
	"cities": 2,
	"settlements": 4,
	"soldiers": 1,
	"victoryPoints": 7,
	"monuments": 0,
	"longestRoad": true,
	"largestArmy": false,
	"playedDevCard": true,
	"discarded": false,
	"playerID": 0,
	"orderNumber": 0,
	"name": "Sam",
	"color": "orange"
};
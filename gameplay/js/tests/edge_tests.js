var edge = new catan.map.Edge();

module("Edge Class", {
	setup: function()
	{
	}
});

test("Check Edge initilization", function()
{
	ok(edge.isOwned() === false, "Should have not been owned.");
	ok(edge.getEdgeValue().getOwner() === -1, "Owner should start at -1");
});


test("Order Functionality", function()
{	
	ok(edge.getEdgeValue().setOwner(2) === true, "Function failed erroneously");
	ok(edge.getEdgeValue().getOwner() === 2, "OwnerID did not change to 2");
	ok(edge.getEdgeValue().setOwner(4) === false, "Function passed erroneously");
	ok(edge.getEdgeValue().setOwner(2) === false, "Function passed erroneously");
});
var host = "localhost";
var port = 8081;

asyncTest("Login Test Fail", function(){
	var success = function(){
		ok(false, "Login Succeeded! Should have failed!");
		start();
	}
	var failure = function(){
		ok(true, "Login Failed! Should have failed!");
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.login('Sam', 'dummy');
});

asyncTest("Login Test Succeed", function(){
	var success = function(){
		ok(true, "Login Succeeded! Should have Succeeded!");
		start();
	}
	var failure = function(){
		ok(false, "Login Failed! Should have Succeeded!");
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.login('Sam', 'sam');
});


asyncTest("Join Game Test Fail", function(){
	var success = function(){
		ok(false, "Join Succeeded! Should have failed!");
		start();
	}
	var failure = function(){
		ok(true, "Join Failed! Should have failed!");
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.joinGame({'color' : 'red', 'id' : -1});
});

asyncTest("Join Game Test Succeed", function(){
	var success = function(){
		ok(true, "Join Succeeded! Should have succeeded!");
		start();
	}
	var failure = function(){
		ok(false, "Join Failed! Should have succeeded!");
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.joinGame({'color' : 'red', 'id' : 0});
});

//Get Model
asyncTest("Get Model Test Succeed", function() {
	var success = function() {
		ok(true, 'Get Model Succeeded!');
		start();
	};
	var failure = function(){
		ok(false, 'Get Model Failed! It should have Succeeded!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	
	serverProxy.model();
});

//Roll Number
asyncTest("Roll Number Test Succeed", function(){
	var success = function() {
		ok(true, 'Roll Number Succeeded!');
		start();
	};
	var failure = function(){
		ok(false, 'Roll Number Failed! It should have Succeeded!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);

	serverProxy.rollNumber(0, 2);
});

//Send Chat
asyncTest("Send Chat Test Succeed", function(){
	var success = function() {
		ok(true, 'Send Chat Succeeded!');
		start();
	};
	var failure = function(){
		ok(false, 'Send Chat Failed! It should have Succeeded!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);

	serverProxy.sendChat(0, 'This is a good chat with good url')
});

//Buy Dev Card
asyncTest("Buy Dev Card Test Succeed", function(){
	var success = function() {
		ok(true, 'Buy Dev Card Succeeded!');
		start();
	};
	var failure = function(){
		ok(false, 'Buy Dev Card Failed! It should have Succeeded!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);

	serverProxy.buyDevCard(0);
});

//Year of Plenty

asyncTest("Year of Plenty Fail", function(){
	var success = function() {
		ok(false, 'Succeeded! It should have failed');
		start();
	};
	var failure = function(){
		ok(true, 'Failed! It should have failed!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.yearOfPlenty(0, "wood", "sheep");
});

//Road Building
asyncTest("Road Building Test Fail", function(){
	var success = function() {
		ok(false, 'Succeeded! It should have failed');
		start();
	};
	var failure = function(){
		ok(true, 'Failed! It should have failed!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.roadBuilding(0);
});

//Soldier
asyncTest("Soldier Succeed", function(){
	var success = function() {
		ok(true, 'Succeeded! It should have succeeded');
		start();
	};
	var failure = function(){
		ok(true, 'Failed! It should have succeeded!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.soldier(0, 1, 3, 3);
});

//Monopoly
asyncTest("Monopoly Succeed", function(){
	var success = function() {
		ok(true, 'Succeeded! It should have succeeded');
		start();
	};
	var failure = function(){
		ok(true, 'Failed! It should have succeeded!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.soldier(0, "wood");
});

//Monument
asyncTest("Monument Succeed", function(){
	var success = function() {
		ok(true, 'Succeeded! It should have succeeded');
		start();
	};
	var failure = function(){
		ok(true, 'Failed! It should have succeeded!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.monument(0);
});

//Build Road
asyncTest("Build Road Succeed", function(){
	var success = function() {
		ok(true, 'Succeeded! It should have succeeded');
		start();
	};
	var failure = function(){
		ok(true, 'Failed! It should have succeeded!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.buildRoad(0, 1, -2, "N", true);
});

//Build Settlement
asyncTest("Build Settlement Succeed", function(){
	var success = function() {
		ok(true, 'Succeeded! It should have succeeded');
		start();
	};
	var failure = function(){
		ok(true, 'Failed! It should have succeeded!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.buildSettlement(0, 1, 0, "NE", true);
});

//Build City
asyncTest("Build City Succeed", function(){
	var success = function() {
		ok(true, 'Succeeded! It should have succeeded');
		start();
	};
	var failure = function(){
		ok(true, 'Failed! It should have succeeded!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.buildCity(2, 1, 0, "NW");
});

//Offer Trade
asyncTest("Offer Trade Succeed", function(){
	var success = function() {
		ok(true, 'Succeeded! It should have succeeded');
		start();
	};
	var failure = function(){
		ok(true, 'Failed! It should have succeeded!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.offerTrade(0, 1, 0, 0, 0, 0, 2);
});

//Accept Trade
asyncTest("Accept Trade Succeed", function(){
	var success = function() {
		ok(true, 'Succeeded! It should have succeeded');
		start();
	};
	var failure = function(){
		ok(true, 'Failed! It should have succeeded!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.acceptTrade(0, true);
});

//Maritime Trade
asyncTest("Maritime Trade Fail Test", function(){
	var success = function() {
		ok(false, 'Succeeded! It should have failed');
		start();
	};
	var failure = function(){
		ok(true, 'Failed! It should have failed!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.maritimeTrade(0, 3, 0, 0, 0, 0, "brick", "wood");
});

//Rob Player
asyncTest("Rob Player Success Test", function(){
	var success = function() {
		ok(false, 'Succeeded! It should have failed');
		start();
	};
	var failure = function(){
		ok(true, 'Failed! It should have failed!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.robPlayer(0, 1, "0", "-2");
});

//Discard Cards
asyncTest("Discard Cards Success Test", function(){
	var success = function() {
		ok(false, 'Succeeded! It should have failed');
		start();
	};
	var failure = function(){
		ok(true, 'Failed! It should have failed!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.discardCards(0, 1, 1, 1, 1, 1);
});

//Finish Turn
asyncTest("Finish Turn Test Succeed", function(){
	var success = function() {
		ok(true, 'Succeeded! It should have succeeded');
		start();
	};
	var failure = function(){
		ok(false, 'Failed! It should have succeeded!');
		start();
	}
	var serverProxy = new catan.game.ServerProxy(host, port, success, failure);
	serverProxy.finishTurn(0);
});

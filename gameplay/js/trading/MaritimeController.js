//STUDENT-EDITABLE-BEGIN
/**
    This is the namespace for maritime trading
    @module catan.trade
    @submodule catan.trade.maritime
    @namespace maritime
*/

var catan = catan || {};
catan.trade = catan.trade || {};
catan.trade.maritime = catan.trade.maritime || {};

catan.trade.maritime.Controller = (function trade_namespace(){
    
	var Definitions = catan.definitions;
	var ResourceTypes = Definitions.ResourceTypes;
    
	var MaritimeController = ( function MaritimeController_Class() {

        var Controller = catan.core.BaseController;
		var player, playerPorts, playerResources, resourceArray = [], allResources = [], tradeOptions = [], tradeRatio, giveResource, getResource;
		allResources[0] = "wood";
		allResources[1] = "brick";
		allResources[2] = "sheep";
		allResources[3] = "wheat";
		allResources[4] = "ore";
        
        /**
		@class MaritimeController
		@constructor 
		@extends misc.BaseController
		@param {maritime.View} view
		@param {models.ClientModel} clientModel
		*/
		function MaritimeController(view,clientModel){
			Controller.call(this,view,clientModel);
		};
        
		MaritimeController.prototype = core.inherit(Controller.prototype);


		MaritimeController.prototype.updateModel = function(model)
		{
			// TODO: call showGiveOptions() based on player's ports and resources?
			this.View.hideGetOptions();
			this.View.hideGiveOptions();
			this.View.enableTradeButton(false);
			
			// Update player, ports, and resources
			player = catan.models.ClientModel.playerObj();
			playerPorts = catan.map2.getPlayerPorts(catan.models.ClientModel.orderNumber());
			playerResources = player.getResources();
			buildResourceArray();

			if (catan.models.ClientModel.isLocalsTurn()) {
				if (resourceArray.length === 0) {
					this.View.setMessage("You don't have enough resources");
					this.View.showGiveOptions(resourceArray);
				}
				else {
					this.View.setMessage("Select resources");
					this.View.showGiveOptions(resourceArray);
				}
			}
			else {
				this.View.setMessage("Not your turn");
			}
		};

		/**
         * Called by the view when the player "undoes" their give selection
		 * @method unsetGiveValue
		 * @return void
		 */
		MaritimeController.prototype.unsetGiveValue = function(){
			console.log("Unset give value");
			// reset to updateModel state
			this.View.setMessage("Choose what to give");
			this.View.hideGetOptions();
			this.View.showGiveOptions(resourceArray);
			this.View.enableTradeButton(false);
			giveResource = undefined;
			tradeOptions = [];
		};
        
		/**
         * Called by the view when the player "undoes" their get selection
		 * @method unsetGetValue
		 * @return void
		 */
		MaritimeController.prototype.unsetGetValue = function(){
			console.log("Unset get value");
			// reset to state before clicked getValue
			this.View.setMessage("Choose what to get");
			this.View.showGetOptions(tradeOptions);
			this.View.enableTradeButton(false);
			getResource = undefined;
		};
        
		/**
         * Called by the view when the player selects which resource to give
		 * @method setGiveValue
		 * @param{String} resource The resource to trade ("wood","brick","sheep","wheat","ore")
		 * @return void
		 */
		MaritimeController.prototype.setGiveValue = function(resource){
			console.log("Set give value:", resource);
			var i;
			for (i = 0; i < allResources.length; i++) {
				if (allResources[i] !== resource) {
					tradeOptions.push(allResources[i]);
				}
			}
			
			if (hasSpecialPort(resource)) {
				tradeRatio = 2;
			}
			else if (hasGenericPort()) {
				tradeRatio = 3;
			}
			else {
				tradeRatio = 4;
			}
			giveResource = resource;
			this.View.setMessage("Choose what to get");
			this.View.selectGiveOption(resource, tradeRatio);
			this.View.showGetOptions(tradeOptions);
		};
        
		/**
         * Called by the view when the player selects which resource to get
		 * @method setGetValue
		 * @param{String} resource The resource to trade ("wood","brick","sheep","wheat","ore")
		 * @return void
		 */
		MaritimeController.prototype.setGetValue = function(resource){
			console.log("Set get value:", resource);
			getResource = resource;
			this.View.setMessage("Trade!");
			this.View.selectGetOption(resource, 1);
			this.View.enableTradeButton(true);
		};
        
        function capFirst(str){
            return str[0].toUpperCase() + str.slice(1);
        }
		
		function buildResourceArray() {
			var i, ratio;
			
			for (var key in playerResources) {
				if (hasSpecialPort(key)) {
					ratio = 2;
				}
				else {
					if (hasGenericPort()) {
						ratio = 3;
					}
					else {
						ratio = 4;
					}
				}
				
				if (playerResources[key] >= ratio) {
					resourceArray.push(key);
				}
			}
		}
		
		function hasGenericPort() {
			var i;
			for (i = 0; i < playerPorts.length; i++) {
				if (playerPorts[i].getInputResource() === undefined) {
					return true;
				}
			}
			return false;
		}
		
		function hasSpecialPort(type) {
			var i;
			for (i = 0; i < playerPorts.length; i++) {
				if (typeof playerPorts[i].getInputResource() !== "undefined" && type === playerPorts[i].getInputResource().toLowerCase()) {
					return true;
				}
			}
			return false;
		}
        
		/** Called by the view when the player makes the trade
		 * @method makeTrade
		 * @return void
		 */
		MaritimeController.prototype.makeTrade= function(){
			console.log("Make trade");
			catan.serverProxy.maritimeTrade(catan.models.ClientModel.orderNumber(), tradeRatio, giveResource, getResource);
			resourceArray = [];
			tradeOptions = [];
			tradeRatio = undefined;
			giveResource = undefined;
			getResource = undefined;
		}
		
       return MaritimeController;
	}());

	return MaritimeController;
}());



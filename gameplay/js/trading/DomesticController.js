//STUDENT-EDITABLE-BEGIN
/**
    This is the namespace for domestic trading
    @module catan.trade
    @submodule catan.trade.domestic
    @namespace domestic
*/

var catan = catan || {};
catan.trade = catan.trade ||{};
catan.trade.domestic = catan.trade.domestic ||{};

catan.trade.domestic.Controller= (function trade_namespace(){

	var Controller = catan.core.BaseController;
	var Definitions = catan.definitions;
	var ResourceTypes = Definitions.ResourceTypes;
    
	var DomesticController = ( function DomesticController_Class() {

		var _trade = {
			outbound: new catan.game.ResourceHand(),
			inbound: new catan.game.ResourceHand(),
			sendrecv: {}
		};
		var _toPlayer = -1;

		/** 
		@class DomesticController
		@constructor 
		@extends misc.BaseController
		@param {domestic.View} view
		@param {misc.WaitOverlay} waitingView
		@param {domestic.AcceptView} acceptView
		@param {models.ClientModel} clientModel
		*/
		function DomesticController(view,waitingView,acceptView,clientModel){
			Controller.call(this,view,clientModel);
			this.waitingView = waitingView;
			this.acceptView = acceptView;

			var players = [];
			for (var i in clientModel.model().players)
			{
				var p = clientModel.model().players[i];
				if (p.playerID == catan.models.ClientModel.playerID())
					continue;
				players.push({
					name: p.name,
					color: p.color,
					index: p.orderNumber
				});
			}
			this.View.setPlayers(players);
		};
        
		DomesticController.prototype = core.inherit(Controller.prototype);
         

		DomesticController.prototype.updateModel = function(model)
		{
			this.View.setTradeButtonEnabled(false);

			if (catan.models.ClientModel.isLocalsTurn())
			{
				this.View.setPlayerSelectionEnabled(false);
				this.View.setResourceSelectionEnabled(true);
				this.View.setStateMessage("Select resources");
			}
			else
			{
				this.View.setPlayerSelectionEnabled(false);
				this.View.setResourceSelectionEnabled(false);
				this.View.setStateMessage("Not your turn");
			}

			var o = model.tradeOffer;
			if (o && o.receiver == catan.models.ClientModel.orderNumber())
			{
				this.acceptView.setPlayerName(model.players[o.sender].name);
				for (var res in o.offer)
				{
					if (o.offer[res] < 0)
					{
						this.acceptView.addGetResource(res, o.offer[res]);
						if (model.players[catan.models.ClientModel.orderNumber()].resources[res]
								< o.offer[res] * -1)
							this.acceptView.setAcceptEnabled(false);
					}
					else if (o.offer[res] > 0)
					{
						this.acceptView.addGiveResource(res, o.offer[res] * -1);
					}
				}
				this.acceptView.showModal();
			}
			else if (!o)
			{
				this.acceptView.closeModal();
				this.waitingView.closeModal();
			}
		};


         
		/******** Methods called by the Domestic View *********/
        
        /**
        * @method setResourceToSend
        * @param{String} resource the resource to send ("wood","brick","sheep","wheat","ore")
        * @return void
        */
		DomesticController.prototype.setResourceToSend = function(resource){
			this.View.setResourceAmountChangeEnabled(resource,
						_trade.outbound[resource] < catan.models.ClientModel.playerModel().resources[resource],
						_trade.outbound[resource] > 0);
			this.View.setResourceAmount(resource, _trade.outbound[resource]);
			_trade.sendrecv[resource] = "send";

			this.View.setStateMessage("Choose a player");
			this.View.setPlayerSelectionEnabled(true);
		};
        
		/**
		 * @method setResourceToReceive
		 * @param{String} resource the resource to receive ("wood","brick","sheep","wheat","ore")
		 * @return void
		 */
		 DomesticController.prototype.setResourceToReceive = function(resource){
		 	this.View.setResourceAmountChangeEnabled(resource,
						true,
						_trade.outbound[resource] > 0);
			this.View.setResourceAmount(resource, _trade.outbound[resource]);
			_trade.sendrecv[resource] = "recv";

			this.View.setStateMessage("Choose a player");
			this.View.setPlayerSelectionEnabled(true);
		};
        
		/**
		  * @method unsetResource
		  * @param{String} resource the resource to clear ("wood","brick","sheep","wheat","ore")
		  * @return void
		  */
		DomesticController.prototype.unsetResource = function(resource){
			this.View.setResourceAmountChangeEnabled(resource, false, false);
			delete _trade.sendrecv[resource];
			_trade.outbound[resource] = 0;
			_trade.inbound[resource] = 0;

			if (Object.keys(_trade.sendrecv).length == 0)
			{
				this.View.setStateMessage("Select resources");
				this.View.setPlayerSelectionEnabled(false);
			}
		};
        
		/**
		 * @method setPlayerToTradeWith
		 * @param{int} playerNumber the player to trade with
		 * @return void
		 */
		DomesticController.prototype.setPlayerToTradeWith = function(playerNumber){
			_toPlayer = playerNumber;
			if (playerNumber > -1)
			{
				this.View.setStateMessage("Request trade");
				this.View.setTradeButtonEnabled(true);
			}
			else
			{
				this.View.setStateMessage("Choose a player");
				this.View.setTradeButtonEnabled(false);
			}
		};
        
		/**
		* Increases the amount to send or receive of a resource
		* @method increaseResourceAmount
		* @param{String} resource ("wood","brick","sheep","wheat","ore")
		* @return void
		*/
		DomesticController.prototype.increaseResourceAmount = function(resource){
			var hand = _trade.sendrecv[resource] == "send" ? _trade.outbound : _trade.inbound;
			this.View.setResourceAmount(resource, hand.addCard(resource));

			if (_trade.sendrecv[resource] == "send")
			{
				this.View.setResourceAmountChangeEnabled(resource,
						_trade.outbound[resource] < catan.models.ClientModel.playerModel().resources[resource],
						_trade.outbound[resource] > 0);
			}
			else
			{
				this.View.setResourceAmountChangeEnabled(resource,
						true,
						true);
			}
		};
        
		/**
		 * Decreases the amount to send or receive of a resource
		 * @method decreaseResourceAmount
		 * @param{String} resource ("wood","brick","sheep","wheat","ore")
		 * @return void
		 */
		DomesticController.prototype.decreaseResourceAmount = function(resource){
			var hand = _trade.sendrecv[resource] == "send" ? _trade.outbound : _trade.inbound;
			this.View.setResourceAmount(resource, hand.removeCard(resource));

			if (_trade.sendrecv[resource] == "send")
			{
				// sending

				this.View.setResourceAmountChangeEnabled(resource,
						_trade.outbound[resource] < catan.models.ClientModel.playerModel().resources[resource],
						_trade.outbound[resource] > 0);
			}
			else
			{
				// receiving

				this.View.setResourceAmountChangeEnabled(resource,
						true,
						_trade.inbound[resource] > 0);
			}
		};
        
		/**
		  * Sends the trade offer to the accepting player
		  * @method sendTradeOffer
		  * @return void
		  */
		DomesticController.prototype.sendTradeOffer = function() {
			var brick = _trade.outbound.brick || _trade.inbound.brick * -1;
			var ore = _trade.outbound.ore || _trade.inbound.ore * -1;
			var sheep = _trade.outbound.sheep || _trade.inbound.sheep * -1;
			var wheat = _trade.outbound.wheat || _trade.inbound.wheat * -1;
			var wood = _trade.outbound.wood || _trade.inbound.wood * -1;
			catan.serverProxy.offerTrade(catan.models.ClientModel.orderNumber(),
				brick, ore, sheep, wheat, wood, _toPlayer);
			this.View.clearTradeView();
			this.waitingView.showModal();
		};
        
        
		/******************* Methods called by the Accept Overlay *************/
		 
        /**
        * Finalizes the trade between players
        * @method acceptTrade
        * @param{Boolean} willAccept
        * @return void
		*/
		DomesticController.prototype.acceptTrade = function(willAccept){
			catan.serverProxy.acceptTrade(catan.models.ClientModel.orderNumber(), willAccept);
			this.acceptView.closeModal();
		};
            
		return DomesticController;
    }());
			
	return DomesticController;
}());



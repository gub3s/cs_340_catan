/**
	The Commands module holds all the POST commands to the server
	@namespace commands
	@module catan.commands
**/

var catan = catan || {};
catan.commands = catan.commands || {};


/**
	<pre>
		A bundle of commands to send to the server.
		Pass one or more Command objects into the function
		(not as a sequence, but as multiple arguments) to
		get the resulting Command.

		Constructor:
			new catan.commands.Commands(Command...)

			 Command... {Command} One or more command objects
			 	(multiple arguments; not a sequence)

		Returns:
			This class is an object representing the list of Commands.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.Commands
**/
catan.commands.Commands = function() {
	var cmds = [];
	for (var i = 0; i < arguments.length; i++)
		cmds.push(arguments[i]);
	return cmds;
};


/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.SendChat(playerIndex, content)

			playerIndex {int} The player index
			content {string} The content of the chat

		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.SendChat
**/
catan.commands.SendChat = function(playerIndex, content) {
	return {
		"type": "sendChat",
		"playerIndex": playerIndex,
		"content": content
	};
};

/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.RollNumber(playerIndex, number)

			playerIndex {int} The player index
			number {string} The number rolled

		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.RollNumber
**/
catan.commands.RollNumber = function(playerIndex, num) {
	return {
		"type": "rollNumber",
		"playerIndex": playerIndex,
		"number": num
	};
};

/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.FinishTurn(playerIndex, content)

			playerIndex {int} The player index

		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.FinishTurn
**/
catan.commands.FinishTurn = function(playerIndex) {
	return {
		"type": "finishTurn",
		"playerIndex": playerIndex
	};
};

/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.BuyDevCard(playerIndex, content)

			playerIndex {int} The player index

		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.BuyDevCard
**/
catan.commands.BuyDevCard = function(playerIndex) {
	return {
		"type": "buyDevCard",
		"playerIndex": playerIndex
	};
};

/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.YearOfPlenty(playerIndex, content)

			playerIndex {int} The player index
			res1 {string} Resource 1
			res2 {string} Resource 2

		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.YearOfPlenty
**/
catan.commands.YearOfPlenty = function(playerIndex, res1, res2) {
	return {
		"type": "Year_of_Plenty",
		"playerIndex": playerIndex,
		"resource1": res1,
		"resource2": res2
	};
};

/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.RoadBuilding(playerIndex, content)

			playerIndex {int} The player index
			x1 {integer} spot1 x
			y1 {integer} spot1 y
			dir1 {string} spot1 dir
			x2 {integer} spot2 x
			y2 {integer} spot2 y
			dir1 {string} spot2 dir


		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.RoadBuilding
**/
catan.commands.RoadBuilding = function(playerIndex, x1, y1, dir1, x2, y2, dir2) {
	return {
		"type": "Road_Building",
		"playerIndex": playerIndex,
		"spot1": {
			"x": x1,
			"y": y1,
			"direction": dir1
		},
		"spot2": {
			"x": x2,
			"y": y2,
			"direction": dir2
		}
	};
};

/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.Soldier(playerIndex, content)

			playerIndex {int} The player index
			victimIndex {int} The victim index
			x {integer} robber spot x
			y {integer} robber spot y

		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.Soldier
**/
catan.commands.Soldier = function(playerIndex, victimIndex, x, y) {
	return {
		"type": "Soldier",
		"playerIndex": playerIndex,
		"victimIndex": victimIndex,
		"location": {
			"x": x,
			"y": y
		}
	};
};

/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.Monopoly(playerIndex, res)

			playerIndex {int} The player index
			res {string} The resource

		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.Monopoly
**/
catan.commands.Monopoly = function(playerIndex, res) {
	return {
		"type": "Monopoly",
		"resource": res,
		"playerIndex": playerIndex
	};
};

/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.Monument(playerIndex)

			playerIndex {int} The player index

		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.Monument
**/
catan.commands.Monument = function(playerIndex) {
	return {
		"type": "Monument",
		"playerIndex": playerIndex
	};
};

/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.BuildRoad(playerIndex, x, y, dir, free)

			playerIndex {int} The player index
			x {integer} The road location's edge x
			y {integer} The road location's edge y
			dir {string} The direction
			free {boolean} Whether it's a free build

		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.BuildRoad
**/
catan.commands.BuildRoad = function(playerIndex, x, y, dir, free) {
	return {
		"type": "buildRoad",
		"playerIndex": playerIndex,
		"roadLocation": {
			"x": x,
			"y": y,
			"direction": dir
		},
		"free": free
	};
};

/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.BuildSettlement(playerIndex, x, y, dir, free)

			playerIndex {int} The player index
			x {integer} The settlement location's vertex x
			y {integer} The settlement location's vertex y
			dir {string} The direction
			free {boolean} Whether it's a free build

		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.BuildSettlement
**/
catan.commands.BuildSettlement = function(playerIndex, x, y, dir, free) {
	return {
		"type": "buildSettlement",
		"playerIndex": playerIndex,
		"vertexLocation": {
			"x": x,
			"y": y,
			"direction": dir
		},
		"free": free
	};
};

/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.BuildCity(playerIndex, x, y, dir)

			playerIndex {int} The player index
			x {integer} The city location's vertex x
			y {integer} The city location's vertex y
			dir {string} The direction

		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.BuildCity
**/
catan.commands.BuildCity = function(playerIndex, x, y, dir) {
	return {
		"type": "buildCity",
		"playerIndex": playerIndex,
		"vertexLocation": {
			"x": x,
			"y": y,
			"direction": dir
		},
		"free": false
	};
};

/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.OfferTrade(playerIndex, brick, ore, sheep, wheat, wood, rcv)

			playerIndex {int} The player index
			brick {int} How many of this resource being offered
			ore {int} How many of this resource being offered
			sheep {int} How many of this resource being offered
			wheat {int} How many of this resource being offered
			wood {int} How many of this resource being offered
			rcv {int} The player index receiving the offer

		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.OfferTrade
**/
catan.commands.OfferTrade = function(playerIndex, brick, ore, sheep, wheat, wood, rcv) {
	return {
		"type": "offerTrade",
		"playerIndex": playerIndex,
		"offer": {
			"brick": brick,
			"ore": ore,
			"sheep": sheep,
			"wheat": wheat,
			"wood": wood
		},
		"receiver": rcv
	};
};

/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.AcceptTrade(playerIndex, accept)

			playerIndex {int} The player index
			accept {boolean} Whether it was accepted or not

		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.AcceptTrade
**/
catan.commands.AcceptTrade = function(playerIndex, accept) {
	return {
		"type": "acceptTrade",
		"playerIndex": playerIndex,
		"willAccept": accept
	};
};

/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.MaritimeTrade(playerIndex, ratio, inres, outres)

			playerIndex {int} The player index
			ratio {number} The benefit ratio
			inres {string} The the resource coming in
			outres {string} The the resource going out

		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.MaritimeTrade
**/
catan.commands.MaritimeTrade = function(playerIndex, ratio, inres, outres) {
	return {
		"type": "maritimeTrade",
		"playerIndex": playerIndex,
		"ratio": ratio,
		"inputResource": inres,
		"outputResource": outres
	};
};

/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.RobPlayer(playerIndex, victimIndex, x, y)

			playerIndex {int} The player index
			victimIndex {int} The victim index
			x {string} Robber x coord
			y {string} Robber y coord

		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.RobPlayer
**/
catan.commands.RobPlayer = function(playerIndex, victimIndex, x, y) {
	return {
		"type": "robPlayer",
		"playerIndex": playerIndex,
		"victimIndex": victimIndex,
		"location": {
			"x": x,
			"y": y
		}
	};
};

/**
	<pre>
		A bundled Command to send to the server.

		Constructor:
			new catan.commands.DiscardCards(playerIndex, brick, ore, sheep, wheat, wood)

			playerIndex {int} The player index
			brick {int} How many of this resource being discarded
			ore {int} How many of this resource being discarded
			sheep {int} How many of this resource being discarded
			wheat {int} How many of this resource being discarded
			wood {int} How many of this resource being discarded

		Returns:
			This class is an object representing the Command.

		Pre-condition:
			None
		Post-condition:
			No changes
	</pre>

	@class commands.DiscardCards
**/
catan.commands.DiscardCards = function(playerIndex, brick, ore, sheep, wheat, wood) {
	return {
		"type": "discardCards",
		"playerIndex": playerIndex,
		"discardedCards": {
			"brick": brick,
			"ore": ore,
			"sheep": sheep,
			"wheat": wheat,
			"wood": wood
		}
	};
};

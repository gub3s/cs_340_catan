/**
	The Game module contains various components of the gameplay logic
	@namespace game
	@module catan.game
**/

var catan = catan || {};
catan.map = catan.map || {};

/**
	The Port class represents a port in the game.
			
	<pre>
		Constructor:
			new catan.game.Port(jsonModel)
				jsonModel: The JSON object representing a port that was sent by the server

				Pre-condition: The JSON model provided is valid and complete
				Post-condition: A Port object will represent that port
	</pre>
	
	@class game.Port
	@constructor
	@param {String} inInputResource 
	@param {HexLocation} inLocation
	@param {String} inOrientation
	@param {Number} inRatio
	@param {VertexLocation} inVV1
	@param {VertexLocation} inVV2
**/
catan.map.Port = function(jsonModel) {
	var inputResource;
	if (jsonModel.inputResource) {
		inputResource = jsonModel.inputResource;
	}
	else {
		inputResource = undefined;
	}
	var location = catan.map.HexLocation(jsonModel.location);
	var orientation = jsonModel.orientation;
	var ratio = jsonModel.ratio;
	var validVertex1 = new catan.map.VertexLocation(jsonModel.validVertex1.x, jsonModel.validVertex1.y, jsonModel.validVertex1.direction);
	var validVertex2 = catan.map.VertexLocation(jsonModel.validVertex2.x, jsonModel.validVertex2.y, jsonModel.validVertex2.direction);
	var ownerID = new catan.map.OrderID();
	
	return {
		/**
			Gets the input resource for this port
	
			@method getInputResource
			@return {String} Gets the input resource for this port
		**/		
		getInputResource: function() {
			return inputResource;
		},
		
		/**
			Sets the input resource for this port
	
			@method getInputResource
			@param {String} resource The resource to set
		**/	
		setInputResource: function(resource) {
			inputResource = resource;
		},
		
		/**
			Gets the location for this port
	
			@method getLocation
			@return {HexLocation} Gets the location for this port
		**/		
		getLocation: function() {
			return location;
		},
		
		/**
			Sets the location for this port
	
			@method getLocation
			@param {HexLocation} newLocation The location to set
		**/	
		setLocation: function(newLocation) {
			location = newLocation;
		},
		
		/**
			Gets the ratio of trading at this port
	
			@method getRatio
			@return {Number} Gets the ratio of trading at this port
		**/
		getRatio: function() {
			return ratio;
		},
		
		/**
			Sets the ratio of trading at this port
	
			@method getRatio
			@param {Number} newRatio The ration to set
		**/
		setRatio: function(newRatio) {
			ratio = newRatio;
		},
		

		/**
			Gets the orientation for this port
	
			@method getOrientation
			@return {string} Gets the orientation for this port
		**/				
		getOrientation: function() {
			var result;
			switch (parseInt(orientation)) {
				case "N":
					result = 1;
					break;
				case "NE":
					result = 2;
					break;
				case "SE":
					result = 3;
					break;
				case "S":
					result = 4;
					break;
				case "SW":
					result = 5;
					break;
				case "NE":
					result = 6;
					break;
				default:
					Console.log("Not a valid orientation.");
					break;
			}
			return result;
		},
		
		setOrientation: function(newOrientation) {
			orientation = newOrientation;
		},
		
		/**
			Gets the valid verteci for this port
	
			@method getVertex1
			@return {VertexLocation} Gets the valid verteci for this port
		**/		
		getVertex1: function() {
			return validVertex1;
		},
		
		/**
			Sets the valid verteci for this port
	
			@method setVertex1
			@param {VertexLocation} v1 The vertex1 for this port
		**/
		setVertex1: function(v1) {
			validVertex1 = v1;
		},
		
		/**
			Gets the valid verteci for this port
	
			@method getVertex1
			@return {VertexLocation} Gets the valid verteci for this port
		**/		
		getVertex2: function() {
			return validVertex2;
		},
		
		/**
			Sets the valid verteci for this port
	
			@method setVertex2
			@param {VertexLocation} v2 The vertex1 for this port
		**/
		setVertex2: function(v2) {
			validVertex2 = v2;
		},
		
		
		/**
			<pre>
			Returns the ID of the player that owns this Port object.
				Pre-condition:
					None
				Post-condition:
					result = ownerID
			</pre>
			@method getOwnerID
			@return {OrderID} ID of the player that owns this object, -1 if no one has built here.
		*/
		getOwnerID: function() {
			return ownerID;
		},
		
		/**
			<pre>
			Sets the current ownerID to the specified OrderID object
				Pre-condition:
					newOwner is between 0 - 3 inclusive
				Post-condition:
					ownerID === newOwner
			</pre>
			@method setOwnerID
			@param {OrderID} newOwner The ID of the player that now owns the Port
			@return {Void}
		*/
		setOwnerID: function(newOwner) {
			ownerID = newOwner;
		}
	}
}
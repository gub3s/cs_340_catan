/**
	The Map module contains various Map-related components
	@namespace map
	@module catan.map
**/

var catan = catan || {};
catan.map = catan.map || {};

/**
The HexGrid Class
<pre>
	Domain:
		hexes: Sequence&lt;Hex&gt;
		offsets: Offsets
		radius: Radius
		x0: Number
		y0: Number
	
	Invariants:
		x0 and y0 are bounded between 0 and 6
	
	Constructor Specifications
		HexGrid(hexes, offsets, radius, x0, y0).Pre-conditions:
			hexes &ne; null AND
			offsets &ne; null AND
			radius &ne; null AND
			x0 &ne; null AND
			0 <= x0 <=6 AND
			y0 &ne; null AND
			0 <= y0 <=6
		HexGrid(hexes, offsets, radius, x0, y0).Post-conditions:
			var hexes = inHexes AND
			var offsets = inOffsets AND
			var radius = inRadius AND
			var x0 = inX0 AND
			var y0 = inY0
</pre>
@class map.HexGrid
@constructor
@param {JSON} jsonModel The JSON model of the HexGrid
*/
catan.map.HexGrid = function HexGrid(jsonModel) {

	var _hexes = []; // 2D array
	for (var i = 0; i < jsonModel.hexes.length; i++) {
		var hexRow = [];
		var row = jsonModel.hexes[i];
		for (var j = 0; j < row.length; j++) {
			hexRow.push(new catan.map.Hex(row[j]));
		}
		_hexes.push(hexRow);
	}
	var offsets = jsonModel.offsets;
	var radius = jsonModel.radius;
	var x0 = jsonModel.x0;
	var y0 = jsonModel.y0;

	/**
		@param {VertexLocation OR HexLocation} loc
		@return {Object} Contains (i,j) indices of desired Hex in the _hexes array
			ex. _hexes[i][j]
	*/
	function convert_cord_to_indexes(loc)
	{
		var i = loc.getY() + 3;
		var myOffset = 0;
		if (i > 3)
			myOffset = i - 3;
		var j = loc.getX() + loc.getY() - myOffset + 3;
		return [i, j];
	}
	
	return {
		/**
			<pre>
			Gets the Hex at the specified index
				Pre-condition:
					HexGrid is a valid HexGrid object.
					Index is not less than 0 or greater than the size of the sequence of Hexes
				Post-condition:
					The sequence of hexes for this map is returned.
			</pre>
			@method update
			@param {JSON} jsonModel The JSON to update the HexGrid from
			@return {void}
		*/
		update: function(jsonModel) {
			var i, j;
			for (i = 0; i < _hexes.length; i++) {
				for (j = 0; j < _hexes[i].length; j++) {
					_hexes[i][j].update(jsonModel.hexes[i][j]);
				}
			}
			return;
		},
		
		/**
		<pre>
		Gets the Hex at the specified index
			Pre-condition:
				HexGrid is a valid HexGrid object.
				Index is not less than 0 or greater than the size of the sequence of Hexes
			Post-condition:
				The sequence of hexes for this map is returned.
		</pre>
		@method getHex
		@param {Number} index The index of the desired Hex
		@return {Sequence&lt;Hex&gt;} The Hex at the specified index
		*/
		getHex: function(index1, index2) {
			return _hexes[index1][index2];
		},
		
		/**
		<pre>
		Gets the number of hexes in the grid
			Pre-condition:
				HexGrid is a valid HexGrid object.
			Post-condition:
				result = the number of hexes
		</pre>
		@method getNumHexes
		@return {Number} The number of hexes in the grid
		*/
		getNumHexes: function() {
			return _hexes.length;
		},
		
		/**
		<pre>
		Gets the offsets
			Pre-condition:
				None
			Post-condition:
				None
		</pre>
		@method getOffsets
		@return {Offsets} The offsets of the hex grid
		*/
		getOffsets: function() {
			return offsets;
		},
		
		/**
		<pre>
		Gets the radius
			Pre-condition:
				None
			Post-condition:
				None
		</pre>
		@method getRadius
		@return {Radius} The radius of the hex grid
		*/
		getRadius: function() {
			return radius;
		},
		
		/**
		<pre>
		Gets x0
			Pre-condition:
				None
			Post-condition:
				None
		</pre>
		@method getX0
		@return {Number} The row in the hex grid that (x,0) refers to
		*/
		getX0: function() {
			return x0;
		},
		
		/**
		<pre>
		Gets y0
			Pre-condition:
				None
			Post-condition:
				None
		</pre>
		@method getY0 
		@return {Number} The column in the hex grid that (0,y) refers to
		*/
		getY0: function() {
			return y0;
		},
		
		/**
			<pre>
				Determines if the specified player owns the specified port
			</pre>
			@method playerOwnsPort
			@param {Number} playerID The player ID
			@param {Port} port The specified port
			@return {Boolean} True if player owns port, false otherwise
		*/
		playerOwnsPort: function(playerID, port) {
			var hexLoc, index, hex, direction1, direction2, vertex1, vertex2;
			
			hexLoc = port.getLocation();
			index = convert_cord_to_indexes(hexLoc);
			hex = _hexes[index[0]][index[1]];
			direction1 = port.getVertex1().getDirectionAsNum();
			direction2 = port.getVertex2().getDirectionAsNum();
			vertex1 = hex.getVertex(direction1);
			vertex2 = hex.getVertex(direction2);
			
			// Check if direction1 on hex is owned by player
			if (playerID === vertex1.getOwner().getOwnerID()) {
				return true;
			}
			
			// Check if direction2 on hex is owned by player
			if (playerID === vertex2.getOwner().getOwnerID()) {
				return true;
			}
			return false;
		},
		
		/**
			<pre>
			Determines if the specified player can place a settlement on the specified Vertex
			Hex.
				Pre-conditions
					hexLocation is a valid HexLocation
					direction is a real direction, specified in the VertexLocation class AND
				Post-conditions
					None
			</pre>
			@method canPlaceSettlement
			@param {VertexLocation} vertexLocation The vertex to be checked
			@param {String} direction The direction of the vertex to be checked
			@return {Boolean} True if player can place a settlement at the specified Hex,
				false otherwise
		*/
		canPlaceSettlement: function(vertexLocation, isSetup) {
			var i, j, hex, hexNorth, hexToCheck;
			var northX, northY;
			var loc = vertexLocation;
			if (loc.getX() < -3 || loc.getX() > 3 || loc.getY() < -3 || loc.getY() > 3 || loc.getX() + loc.getY() < -3 || loc.getX() + loc.getY() > 3)
				return false;
			var result = convert_cord_to_indexes(loc);
			hex = _hexes[result[0]][result[1]];
			
			if (typeof hex === "undefined") {
				return false;
			}

			if (vertexLocation.getDirection() === "W" && hex.getVertex(0).isOccupied())
				return false;
			if (vertexLocation.getDirection() === "E" && hex.getVertex(3).isOccupied())
				return false;
			
			// Find hexNorth
			if (vertexLocation.getDirection() === "W") {
				northX = loc.getX() - 1;
				northY = loc.getY();
				var result = convert_cord_to_indexes(new catan.map.VertexLocation(northX, northY, "W"));
				if (result[0] < 0 || result[1] < 0 || result[0] == 6)
					return false;
				hexNorth = _hexes[result[0]][result[1]];

			}
			else { // direction === "E"
				northX = loc.getX() + 1;
				northY = loc.getY() - 1;
				var result = convert_cord_to_indexes(new catan.map.VertexLocation(northX, northY, "E"));
				if (result[0] < 0 || result[1] < 0 || result[0] + result[1] == 9)
					return false;
				hexNorth = _hexes[result[0]][result[1]];
			}
			
			if (typeof hexNorth === "undefined") {
				return false;
			}
			
			 // During game, not building next to your own road means you can't build here
			if (!isSetup) {
				var playerID = catan.models.ClientModel.orderNumber();
				if (vertexLocation.getDirection() === "W") {
					// Check hex.getEdges(0), hex.getEdges(5), hexNorth.getEdges(4)
					if (hex.getEdge(0).getEdgeValue().getOwner() !== playerID &&
						  hex.getEdge(5).getEdgeValue().getOwner() !== playerID &&
						  hexNorth.getEdge(4).getEdgeValue().getOwner() !== playerID) {
						return false;
					}
				}
				else {
					// Check hex.getEdges(2), hex.getEdges(3), hexNorth.getEdges(4)
					if (hex.getEdge(2).getEdgeValue().getOwner() !== playerID &&
						  hex.getEdge(3).getEdgeValue().getOwner() !== playerID &&
						  hexNorth.getEdge(4).getEdgeValue().getOwner() !== playerID) {
						return false;
					}
				}
			}
			
			// Check if adjacent to any other city
			if (vertexLocation.getDirection() === "W") {
				// Check if hex.getVertex(1), hex.getVertex(5), hexNorth.getVertex(4) isOccupied
				if (hex.getVertex(1).isOccupied() ||
					  hex.getVertex(5).isOccupied() ||
					  hexNorth.getVertex(5).isOccupied()) {
					return false;
				}
			}
			else {
				// Check if hex.getVertex(2), hex.getVertex(4), hexNorth.getVertex(5) isOccupied
				if (hex.getVertex(2).isOccupied() ||
					  hex.getVertex(4).isOccupied() ||
					  hexNorth.getVertex(4).isOccupied()) {
					return false;
				}
			}
			return true;
		},
		
		/**
			<pre>
			Determines if the specified player can place a city on the specified Vertex
				Pre-conditions:
					Map is a valid map object AND
					playerID is between 0 - 3, inclusive AND
					hexLocation is a valid HexLocation AND
					direction is a real direction, specified in the VertexLocation class
				Post-conditions
					None
			</pre>
			@method canPlaceCity
			@param {OrderID} playerID The player desiring to place a city
			@param {HexLocation} hexLocation The hex to be checked
			@param {String} direction The direction of the vertex to be checked
			@return {Boolean} True if player can place a city at the specified Hex,
				false otherwise
		*/
		canPlaceCity: function(loc) {
			var i;
			if (loc.getX() < -3 || loc.getX() > 3 || loc.getY() < -3 || loc.getY() > 3 || loc.getX() + loc.getY() < -3 || loc.getX() + loc.getY() > 3)
				return false;
			var result = convert_cord_to_indexes(loc);
			var hex = _hexes[result[0]][result[1]];
			if (loc.getDirection() === "W")
				return hex.getVertex(0).getOwner().getOwnerID() === catan.models.ClientModel.orderNumber() && hex.getVertex(0).getValue().getWorth() === 1;
			else
				return hex.getVertex(3).getOwner().getOwnerID() === catan.models.ClientModel.orderNumber() && hex.getVertex(3).getValue().getWorth() === 1; 
			console.log(_hexes[result[0]][result[1]]);
			for (i = 0; i < hexes.length; i++) {
				if (hexes[i].getLocation().getX() === hexLocation.getX() &&
					hexes[i].getLocation().getY() === hexLocation.getY()) 
				{
					if (hexes[i].canPlaceCity(playerID, direction)) {
						return true;
					}
					else {
						return false;
					}
				}
			}
		},
		
		/**
			<pre>
			Determines if the specified player can place a road on the specified Edge.
				Pre-conditions:
					Map is a valid map object AND
					hexLocation is a valid HexLocation AND
					direction is a real direction, specified in the EdgeLocation class
				Post-conditions:
					None
			</pre>
			@method canPlaceRoad
			@param {HexLocation} hexLocation The hex to be checked
			@param {String} direction The direction of the vertex to be checked
			@return {Boolean} True if player can place the road, false otherwise
		*/
		canPlaceRoad: function(loc, setup) {
			var hexLocation = new catan.map.HexLocation(loc);
			if (loc.getX() < -3 || loc.getX() > 3 || loc.getY() < -3 || loc.getY() > 3 || loc.getX() + loc.getY() < -3 || loc.getX() + loc.getY() > 3)
				return false;
			var result = convert_cord_to_indexes(loc);
			var player = catan.models.ClientModel.orderNumber();
			if (result[0] + result[1] > 9)
				return false;
			if (loc.getDirection() == "SE") { // SE Case
				if (_hexes[result[0]][result[1]].getEdges()[3].getEdgeValue().getOwner() != -1)
					return false;
				var nextHex = convert_cord_to_indexes(new catan.map.VertexLocation(loc.getX() + 1, loc.getY(), loc.getDirection()));
				if (typeof _hexes[nextHex[0]] != "undefined" && typeof _hexes[nextHex[0]][nextHex[1]] != "undefined")
				{
					if (_hexes[result[0]][result[1]].isLand() || _hexes[nextHex[0]][nextHex[1]].isLand())
					{
						// Check if road is a valid location
						// Check Vertexes 3 and 4
						if (setup)
						{
							var check = _hexes[result[0]][result[1]].getVertexes()[4].getOwner().getOwnerID()
							if (check == player)
							{
								if (_hexes[result[0]][result[1]].getEdges()[4].getEdgeValue().getOwner() == -1 &&
									_hexes[nextHex[0]][nextHex[1]].getEdges()[5].getEdgeValue().getOwner() == -1)
									return true;
								else
									return false;
							}
							else
							{
								check = _hexes[result[0]][result[1]].getVertexes()[3].getOwner().getOwnerID()
								if (check == player)
								{
									if (_hexes[result[0]][result[1]].getEdges()[2].getEdgeValue().getOwner() == -1 &&
										_hexes[nextHex[0]][nextHex[1]].getEdges()[1].getEdgeValue().getOwner() == -1)
										return true;
									else
										return false;
								}
								else
									return false;
							}
						}
						else
						{
							var check = _hexes[result[0]][result[1]].getVertexes()[4].getOwner().getOwnerID()
							if (check == player || check == -1)
							{
								if (check != -1)
									return true;
								if (_hexes[result[0]][result[1]].getEdges()[4].getEdgeValue().getOwner() == player ||
									_hexes[nextHex[0]][nextHex[1]].getEdges()[5].getEdgeValue().getOwner() == player)
									return true;
							}
							check = _hexes[result[0]][result[1]].getVertexes()[3].getOwner().getOwnerID()
							if (check == player || check == -1)
							{
								if (check != -1)
									return true;
								if (_hexes[result[0]][result[1]].getEdges()[2].getEdgeValue().getOwner() == player ||
									_hexes[nextHex[0]][nextHex[1]].getEdges()[1].getEdgeValue().getOwner() == player)
									return true;
								else
									return false;
							}
							else
								return false;
						}
					}
					else
						return false;
				}
				else
					return false;
			}
			if (loc.getDirection() == "S") { // S Case
				if (_hexes[result[0]][result[1]].getEdges()[4].getEdgeValue().getOwner() != -1)
					return false;
				var nextHex = convert_cord_to_indexes(new catan.map.VertexLocation(loc.getX(), loc.getY() + 1, loc.getDirection()));
				if (typeof _hexes[nextHex[0]] != "undefined" && typeof _hexes[nextHex[0]][nextHex[1]] != "undefined")
				{
					if (_hexes[result[0]][result[1]].isLand() || _hexes[nextHex[0]][nextHex[1]].isLand())
					{
						// Check if road is a valid location
						// Check Vertexes 4 and 5
						if (setup)
						{
							var check = _hexes[result[0]][result[1]].getVertexes()[4].getOwner().getOwnerID()
							if (check == player)
							{
								if (_hexes[result[0]][result[1]].getEdges()[3].getEdgeValue().getOwner() == -1 &&
									_hexes[nextHex[0]][nextHex[1]].getEdges()[2].getEdgeValue().getOwner() == -1)
									return true;
								else
									return false;
							}
							else
							{
								check = _hexes[result[0]][result[1]].getVertexes()[5].getOwner().getOwnerID()
								if (check == player)
								{
									if (_hexes[result[0]][result[1]].getEdges()[5].getEdgeValue().getOwner() == -1 &&
										_hexes[nextHex[0]][nextHex[1]].getEdges()[0].getEdgeValue().getOwner() == -1)
										return true;
									else
										return false;
								}
								else
									return false;
							}
						}
						else
						{
							var check = _hexes[result[0]][result[1]].getVertexes()[4].getOwner().getOwnerID()
							if (check == player || check == -1)
							{
								if (check != -1)
									return true;
								if (_hexes[result[0]][result[1]].getEdges()[3].getEdgeValue().getOwner() == player ||
									_hexes[nextHex[0]][nextHex[1]].getEdges()[2].getEdgeValue().getOwner() == player)
									return true;
							}
							check = _hexes[result[0]][result[1]].getVertexes()[5].getOwner().getOwnerID()
							if (check == player || check == -1)
							{
								if (check != -1)
									return true;
								if (_hexes[result[0]][result[1]].getEdges()[5].getEdgeValue().getOwner() == player ||
									_hexes[nextHex[0]][nextHex[1]].getEdges()[0].getEdgeValue().getOwner() == player)
									return true;
								else
									return false;
							}
							else
								return false;
						}
					}
					else
						return false;
				}
				else
					return false;
			}
			if (loc.getDirection() == "SW") { // SW Case
				if (_hexes[result[0]][result[1]].getEdges()[5].getEdgeValue().getOwner() != -1)
					return false;
				var nextHex = convert_cord_to_indexes(new catan.map.VertexLocation(loc.getX() - 1, loc.getY() + 1, loc.getDirection()));
				if (typeof _hexes[nextHex[0]] != "undefined" && typeof _hexes[nextHex[0]][nextHex[1]] != "undefined")
				{
					if (_hexes[result[0]][result[1]].isLand() || _hexes[nextHex[0]][nextHex[1]].isLand())
					{
						// Check if road is a valid location
						// Check Vertexes 5 and 0
						if (setup)
						{
							var check = _hexes[result[0]][result[1]].getVertexes()[5].getOwner().getOwnerID()
							if (check == player)
							{
								if (_hexes[result[0]][result[1]].getEdges()[4].getEdgeValue().getOwner() == -1 &&
									_hexes[nextHex[0]][nextHex[1]].getEdges()[3].getEdgeValue().getOwner() == -1)
									return true;
								else
									return false;
							}
							else
							{
								check = _hexes[result[0]][result[1]].getVertexes()[0].getOwner().getOwnerID()
								if (check == player)
								{
									if (_hexes[result[0]][result[1]].getEdges()[0].getEdgeValue().getOwner() == -1 &&
										_hexes[nextHex[0]][nextHex[1]].getEdges()[1].getEdgeValue().getOwner() == -1)
										return true;
									else
										return false;
								}
								else
									return false;
							}
						}
						else
						{
							var check = _hexes[result[0]][result[1]].getVertexes()[5].getOwner().getOwnerID()
							if (check == player || check == -1)
							{
								if (check != -1)
									return true;
								if (_hexes[result[0]][result[1]].getEdges()[4].getEdgeValue().getOwner() == player ||
									_hexes[nextHex[0]][nextHex[1]].getEdges()[3].getEdgeValue().getOwner() == player)
									return true;
							}
							check = _hexes[result[0]][result[1]].getVertexes()[0].getOwner().getOwnerID()
							if (check == player || check == -1)
							{
								if (check != -1)
									return true;
								if (_hexes[result[0]][result[1]].getEdges()[0].getEdgeValue().getOwner() == player ||
									_hexes[nextHex[0]][nextHex[1]].getEdges()[1].getEdgeValue().getOwner() == player)
									return true;
								else
									return false;
							}
							else
								return false;
						}
					}
					else
						return false;
				}
				else
					return false;
			}
			return true;
		},
		
		/**
			<pre>
			</pre>
			@method getRobbablePlayer
			@param {HexLocation} hexLoc The location to check for robbable players
			@return {Array of orderID}
		*/
		getRobbablePlayers: function(hexLoc) {
			var index = convert_cord_to_indexes(hexLoc);
			var hex = _hexes[index[0]][index[1]];
			return hex.getPlayersOnHex();
		},
		
		/**
			<pre>
			
			</pre>
			@method canPlaceRobber
			@param {Object} 
		*/
		canPlaceRobber: function(loc) {
			// Catch out of bounds hexes
			if (loc.getX() < -3 || loc.getX() > 3 || loc.getY() < -3 || loc.getY() > 3 || loc.getX() + loc.getY() < -3 || loc.getX() + loc.getY() > 3)
				return false;
			
			// Check if loc is a land hex
			var index = convert_cord_to_indexes(loc);
			var hex = _hexes[index[0]][index[1]];
			if (hex.isLand()) {
				return true;
			}
			else {
				return false;
			}
		}
		
	}
};
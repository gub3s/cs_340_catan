/**
	The Game module contains various components of the gameplay logic
	@namespace game
	@module catan.game
**/

var catan = catan || {};
catan.game = catan.game || {};

/**
	<pre>
		The Deck class represents the deck of development cards in the game.

		Domain:
			model: DevelopmentCardHand
			
		Invariant:
			none
	
		Constructor:
			new catan.game.Deck(jsonModel)
				jsonModel: The JSON object representing a deck of development card that was sent by the server

				Pre-condition:
					The JSON model provided is valid and complete
				Post-condition:
					A Deck object will represent that deck
	</pre>
	
	@class game.Deck
**/
catan.game.Deck = function(jsonModel) {

	var _model;

	$(catan).on('newmodel', function(event, model)
	{
		_model = model;
		for (var i in model.deck)
			this[model.deck[i]] = model.deck[i];
	});

	/**
		Number of year of plenty cards
		@property yearOfPlenty
		@type int
		@default 0
	**/

	/**
		Number of monopoly cards
		@property monopoly
		@type int
		@default 0
	**/

	/**
		Number of soldier cards
		@property soldier
		@type int
		@default 0
	**/

	/**
		Number of road building cards
		@property roadBuilding
		@type int
		@default 0
	**/

	/**
		Number of monument cards
		@property monument
		@type int
		@default 0
	**/

	for (var i in catan.definitions.CardTypes)
		this[catan.definitions.CardTypes[i]] = jsonModel[catan.definitions.CardTypes[i]] || 0;
	
	return {
		
		/**
			<pre>
				Updates the deck after a new game model is received from the server
				
				Pre-condition:
					The JSON model provided is valid and complete
				Post-condition:
					The deck is updated to match the model
			</pre>
	
			@method updateDeck
			@param {string} updatedJson The updated json from the server
		**/
		updateDeck: function(updatedJson) {
			for (var i in catan.definitions.CardTypes)
				this[catan.definitions.CardTypes[i]] = updatedJson[catan.definitions.CardTypes[i]];
		},
		
		/**
			<pre>
				Whether the player can draw a development card from the deck
				
				Pre-condition:
					none
				Post-condition:
					The deck is unchanged
			</pre>
			@method canDrawCard
			@return {boolean} Whether the player can draw a development card from the deck
		**/
		canDrawCard: function() {
			for (var i in catan.definitions.CardTypes)
				if (this[catan.definitions.CardTypes[i]] > 0)
					return true;
			return false;
		}
	}
}
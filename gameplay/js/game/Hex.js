/**
	The Map module contains various Map-related components
	@namespace map
	@module catan.map
**/

var catan = catan || {};
catan.map = catan.map || {};

/**
The Hex Class
<pre>
	Domain:
		location: HexLocation
		edges: Sequence&lt;Edge&gt;
		vertexes: Sequence&lt;Vertex&gt;
		hexType: 
		isLand: Boolean
	
	Invariants:
		once isLand is set, it never changes
		
	Constructor Specifications
		Hex(location, edges, vertexes).Pre-conditions:
			locations &ne; null AND
			edges &ne; null AND
			size of edges === 6 AND
			vertexes &ne; null AND
			size of vertexes === 6
		Hex(location, edges, vertexes).Post-conditions:
			var location === inLocation AND
			var edges === inEdges AND
			var vertexes === inVertexes AND
			var hexType === inHexType AND
			var isLand === inIsLand
</pre>
@class map.Hex
@constructor
@param {HexLocation} location The location of the hex on the hex grid
@param {Sequence&lt;Edge&gt;} edges The hex's edges
@param {Sequence&lt;Vertex&gt;} vertexes The hex's vertexes
@param {String} inHexType The type of hex this object will be, types defined in catan.definitions.HexTypes
@param {Boolean} inIsLand Whether or not the hex is a land hex
*/
catan.map.Hex = function Hex(jsonModel) {
	var location = new catan.map.HexLocation(jsonModel.location);
	
	var edges = [];
	for (var i = 0; i < jsonModel.edges.length; i++) {
		edges.push(new catan.map.Edge(jsonModel.location.x, jsonModel.location.y, i));
	}
	
	var vertexes = [];
	for (var j = 0; j < jsonModel.vertexes.length; j++) {
		vertexes.push(new catan.map.Vertex(jsonModel.location.x, jsonModel.location.y, j));
	}
	
	var hexType;
	if (jsonModel.landType) {
		hexType = jsonModel.landType;
	}
	else {
		hexType = undefined;
	}
	
	var isLand = jsonModel.isLand;
	
	return {
		/**
			<pre>
			Updates the Hex and its dependencies from the specified JSON object
				Pre-condition:
					Hex is a valid Hex object
				Post-condition:
					The Hex is updated
			</pre>
			@method update
			@param {JSON} jsonModel The JSON to update the Hex from
			@return {void}
		*/
		update: function(jsonModel) {
			var i;
			// Update vertexes
			for (i = 0; i < vertexes.length; i++) {
				vertexes[i].update(jsonModel.vertexes[i]);
			}
			// Update edges
			for (i = 0; i < edges.length; i++) {
				edges[i].update(jsonModel.edges[i]);
			}
			return;
		},
		
		/**
		<pre>
		Returns the Hex's location
			Pre-condition:
				Hex is a valid Hex object
			Post-condition:
				The HexLocation of this object is returned.
		</pre>
		@method getLocation
		@return {HexLocation} Hex's location as an object
		*/
		getLocation: function() {
			return location;
		},
		
		/**
		<pre>
		Returns the Hex's edges
			Pre-condition:
				Hex is a valid Hex object
			Post-condition:
				The edges of this object are returned.
		</pre>
		@method getEdges
		@return {Sequence&lt;edge&gt;} The edges of this Hex
		*/
		getEdges: function() {
			return edges;
		},
		
		/**
		<pre>
		Returns the Edge specified by the given index
			Pre-condition:
				Hex is a valid Hex object
				Index is not less than 0 or greater than 5
			Post-condition:
				result = the Edge at index
		</pre>
		@method getEdge
		@param {Number} index The index of the desired Edge
		@return {Edge} The Edge at index
		*/
		getEdge: function(index) {
			return edges[index];
		},
		
		/**
		<pre>
		Returns the Hex's vertexes
			Pre-condition:
				Hex is a valid Hex object
			Post-condition:
				The verteci of this object are returned.
		</pre>
		@method getVertexes
		@return {Sequence&lt;vertex&gt;} The verteci of this Hex
		*/
		getVertexes: function() {
			return vertexes;
		},
		
		/**
		<pre>
		Returns the Vertex specified by the given index
			Pre-condition:
				Hex is a valid Hex object
				Index is not less than 0 or greater than 5
			Post-condition:
				result = the Vertex at index
		</pre>
		@method getVertex
		@param {Number} index The index of the desired Vertex
		@return {Vertex} The Vertex at index
		*/
		getVertex: function(index) {
			return vertexes[index];
		},
		
		/**
		<pre>
		Finds what players have municipalities on this hex
			Pre-condition:
				Hex is a valid Hex object
				Index is not less than 0 or greater than 5
			Post-condition:
				result = the Vertex at index
		</pre>
		@method getPlayersOnHex
		@return {Sequence&lt;Number&gt;} A sequence of player IDs
		*/
		getPlayersOnHex: function() {
			var i, owner, playerList = [];
			for (i = 0; i < vertexes.length; i++) {
				owner = vertexes[i].getOwner().getOwnerID();
				if (owner !== -1 && playerList.indexOf(owner) == -1) {
					playerList.push(owner);
				}
			}
			return playerList;
		},
		
		/**
		<pre>
		Returns the Hex's type
			Pre-condition:
				Hex is a valid Hex object
			Post-condition:
				result === hexType
		</pre>
		@method getHexType
		@return {String} The type of hex object is 
		*/
		getHexType: function() {
			return hexType;
		},
		
		/**
		<pre>
		Returns whether or not the hex is a land hex
			Pre-condition:
				Hex is a valid Hex object
			Post-condition:
				
		</pre>
		@method getVertexes
		@return {Sequence&lt;vertex&gt;} The verteci of this Hex
		*/
		isLand: function() {
			return isLand;
		},
		
		/**
			<pre>
			Determines if the specified player can place a settlement on the specified Vertex
			Hex.
				Pre-conditions
					hexLocation is a valid HexLocation
					direction is a real direction, specified in the VertexLocation class AND
				Post-conditions
					None
			</pre>
			@method canPlaceSettlement
			@param {String} direction The direction of the vertex to be checked
			@return {Boolean} True if player can place a settlement at the specified Hex,
				false otherwise
		*/
		canPlaceSettlement: function(direction) {
			var i;
			for (i = 0; i < vertexes.length; i++) {
				if (vertexes[i].getDirection() === direction) {
					if (vertexes[i].isOccupied()) {
						return false;
					}
					else {
						return true;
					}
				}
			}
		},
		
		/**
			<pre>
			Determines if the specified player can place a city on the specified Vertex
				Pre-conditions:
					Map is a valid map object AND
					playerID is between 0 - 3, inclusive AND
					direction is a real direction, specified in the VertexLocation class
				Post-conditions
					None
			</pre>
			@method canPlaceCity
			@param {OrderID} playerID The player desiring to place a city
			@param {String} direction The direction of the vertex to be checked
			@return {Boolean} True if player can place a city at the specified Hex,
				false otherwise
		*/
		canPlaceCity: function(playerID, direction) {
			var i;
			for (i = 0; i < vertexes.length; i++) {
				if (vertexes[i].getDirection() === direction) {
					if (vertexes[i].getOwner().getOwnerID() === playerID.getOwnerID()) {
						return true;
					}
					else {
						return false;
					}
				}
			}
		},
		
		/**
			<pre>
			Determines if the specified player can place a road on the specified Edge.
				Pre-conditions:
					Map is a valid map object AND
					hexLocation is a valid HexLocation AND
					direction is a real direction, specified in the EdgeLocation class
				Post-conditions:
					None
			</pre>
			@method canPlaceRoad
			@param {String} direction The direction of the vertex to be checked
			@return {Boolean} True if player can place the road, false otherwise
		*/
		canPlaceRoad: function(direction) {
			var i;
			for (i = 0; i < vertexes.length; i++) {
				if (vertexes[i].getDirection() === direction) {
					if (vertexes[i].isOccupied()) {
						return false;
					}
					else {
						return true;
					}
				}
			}
		}
		
	}
};
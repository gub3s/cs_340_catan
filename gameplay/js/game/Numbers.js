/**
	The Map module contains various Map-related components
	@namespace map
	@module catan.map
**/

var catan = catan || {};
catan.map = catan.map || {};

/**
The Numbers Class
<pre>
	Domain:
		chitLocations: Sequence<HexLocation>
		
	Invariants:
		Each HexLocation in chitLocatioins is mapped to one number between 2 - 12
		
	Constructor Specifications
		Numbers().Pre-conditions:
			chitLocations &ne; null AND
			size of chitLocations === 10
		Numbers().Post-conditions:
			var chitLocations === chitLocations
</pre>
@class map.Numbers
@constructor
@param {Sequence &lt;HexLocation&gt;} chitLocations The location of the number chits on the hex grid
*/
catan.map.Numbers = function Numbers(jsonModel) {
	var chitLocations = [];
	for (var i = 2; i < 13; i++) {
		var chit = [];
		var chitList = jsonModel[i];
		for (var j = 0; j < chit.length; j++) {
			chit.push(new catan.map.HexLocation(chit[j]));
		}
		chitLocations[i] = chit;
	}
	
	return {
		/**
		<pre>
		Returns the hex location at the specified index.  The index corresponds to a dice roll
		with two die, so it must be between 2 - 12.  These numbers determine which chits yield
		resources on a roll of of the dice.
			Pre-condition:
				index is a number between 2 - 12
			Post-condtion:
				result = HexLocation stored at the specified index
		</pre>			
		@method getChitLocation
		@param {Number} chitNumber The chit number desired. Ranges from 2 -12
		@param {Number} index The index of the location desired.
			If chitNumber = 2 or 12, index can only be 0
			Otherwise, index can be 0 or 1
		@return {HexLocation} The hex location requested.
		*/
		getChitLocation: function(chitNumber, index) {
			return chitLocations[chitNumber][index];
		}
	}
};
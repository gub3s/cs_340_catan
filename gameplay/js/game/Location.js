/**
	The Game module contains various components of the gameplay logic
	@namespace game
	@module catan.game
**/

var catan = catan || {};
catan.game = catan.game || {};

/**
	<pre>
		The Location class represents a location of items (ie a port) in the game.
			
		Domain:
			x: int
			y: int

		Constructor:
			new catan.game.Location(jsonModel)
				jsonModel: The JSON object sent by the server that represents a location

				Pre-condition:
					The JSON model provided is valid and complete
				Post-condition:
					A Location object will represent an item's location
	</pre>
	
	@class game.Location
**/
catan.game.Location = function(jsonModel)
{
	var model = jsonModel;
	
	return {
	
		/**
			<pre>
				Gets the X value of the location

				Pre-condition:
					None
				Post-condition:
					No changes
			</pre>

			@method getX
			@return {Integer} The x value of the location
		**/
		getX: function() {
			return //x point for this location
		},

		/**
			<pre>
				Gets the Y value of the location

				Pre-condition:
					None
				Post-condition:
					No changes
			</pre>
	
			@method getY
			@return {Integer} The y value of the location
		**/
		getY: function() {
			return //y point for this location
		}
	}
};
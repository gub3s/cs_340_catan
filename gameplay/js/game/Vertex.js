/**
	The Map module contains various Map-related components
	@namespace map
	@module catan.map
**/

var catan = catan || {};
catan.map = catan.map || {};

/**
The Vertex Class
<pre>
	Domain:
		value: VertexValue
		isOwned: Boolean expression
		direction: VertexLocation
		
	Invariants:
		isOwned is always initialized as "false"
		inDirection must be between 0 - 5, inclusive
		
	Constructor Specifications
		Vertex().Pre-conditions:
			N/A
		Vertex().Post-conditions:
			var value === a new VertexValue object AND
			var isOwned === false
			var direction === new VertexValue with the string format of the given number
</pre>
@class map.Vertex
@param {Number} inDirection
@constructor
*/
catan.map.Vertex = function Vertex(x, y, inDirection) {
	var value = new catan.map.VertexValue();
	var isOccupied = false;
	var direction = new catan.map.VertexLocation(x, y, inDirection);
	
	return {
		/**
			<pre>
			Updates the Vertex and its dependencies from the specified JSON object
				Pre-condition:
					Vertex is a valid Vertex object
				Post-condition:
					The Vertex is updated
			</pre>
			@method update
			@param {JSON} jsonModel The JSON to update the Vertex from
			@return {void}
		*/
		update: function(jsonModel) {
			// Update value
			value.update(jsonModel.value);
			return;
		},
		
		/**
			<pre>
			Returns the VertexValue of this object.
				Pre-condition:
					None
				Post-condition:
					result = this object's value
			</pre>
			@method getValue
			@return {VertexValue} Value of this object.
		*/
		getValue: function() {
			return value;
		},
		
		/**
			<pre>
			Returns the owner of this object.
				Pre-condition:
					None
				Post-condition:
					result = this object's owner
			</pre>
			@method getOwner
			@return {OrderID} Value of this object.
		*/
		getOwner: function() {
			return value.getOwnerID();
		},
		
		/**
			<pre>
			Sets the current ownerID to the specified OrderID object
				Pre-condition:
					ownerID is between 0 - 3 inclusive
				Post-condition:
					The specified player now owns this vertex and 
					has a municipality on it
			</pre>
			@method setOwner
			@param {OrderID} ownerID The ID of the player that now owns the Vertex
			@return {Boolean} True for success, false otherwise
		*/
		setOwner: function(ownerID) {
			if (value.setOwnerID(ownerID)) {
				return true;
			}
			else {
				return false;
			}
		},
		
		/**
			<pre>
			Returns if a structure is built here or not.
				Pre-condition:
					None
				Post-condition:
					result = boolean
			</pre>
			@method isOccupied
			@return {Boolean} Returns true if there is no municipality at
				this vertex, false otherwise.
		*/
		isOccupied: function() {
			return (value.getOwnerID().getOwnerID() !== -1);
		},
		
		/**
			<pre>
			Gets the direction of the vertex
				Pre-condition:
					None
				Post-condition:
					result = direction
			</pre>
		@method getDirection 
		@return {String} The direction of the vertex
		*/
		getDirection: function() {
			return direction.getDirection();
		}
		
	}
};
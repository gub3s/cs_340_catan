/**
	The Map module contains various Map-related components
	@namespace map
	@module catan.map
**/

var catan = catan || {};
catan.map = catan.map || {};

/**
The EdgeValue Class
<pre>
	Domain:
		ownerID: OrderID
		
	Invariants:

	Constructor Specifications
		EdgeValue().Pre-conditions:
			N/A
		EdgeValue().Post-conditions:
			var ownerID === a new OrderID object
</pre>
@class map.EdgeValue
@constructor
@param {OrderID} ownerID The ID of the edge's owner
*/
catan.map.EdgeValue = function() {
	var ownerID = new catan.map.OrderID();
	
	return {
		/**
			<pre>
			Updates the EdgeValue and its dependencies from the specified JSON object
				Pre-condition:
					EdgeValue is a valid EdgeValue object
				Post-condition:
					The EdgeValue is updated
			</pre>
			@method update
			@param {JSON} jsonModel The JSON to update the EdgeValue from
			@return {void}
		*/
		update: function(jsonModel) {
			ownerID.setOwnerID(jsonModel.ownerID);
			return;
		},
		
		/**
			<pre>
			Returns the ID of the owner of this EdgeValue (-1 if unowned).
				Pre-condition:
					None
				Post-condition:
					result = ownerID
			</pre>
			@method getOwner
			@return {Number} Value of the current OwnerID.
		*/
		getOwner: function() {
			return ownerID.getOwnerID();
		},
		
		/**
			<pre>
			Sets the owner of this EdgeValue to that specified by the given OrderID
				Pre-condition:
					owner.id between 0 - 3
				Post-condition:
					ownerID === newOwnerID
			</pre>
			@method: setOwner
			@param {OrderID} newOwnerID The ID of the new owner of this Edge
			@return {Boolean} True for success, false otherwise
		*/
		setOwner: function(newOwnerID) {
			return ownerID.setOwnerID(newOwnerID);
		}
	}
};
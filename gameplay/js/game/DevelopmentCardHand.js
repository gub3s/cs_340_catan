/**
	The Game module contains various components of the gameplay logic
	@namespace game
	@module catan.game
**/

var catan = catan || {};
catan.game = catan.game || {};


/**
	The DevelopmentCardHand class represents a group of Development cards
	<pre>
		Domain:
			yearOfPlenty:  int
			monopoly: int
			soldier: int
			roadBuilding: int
			monument:   int
			
		Invariant:
			0 &lt;= yearOfPlenty &lt;= 2
			0 &lt;= monopoly &lt;= 2
			0 &lt;= soldier &lt;= 14
			0 &lt;= roadBuilding &lt;= 2
			0 &lt;= monument &lt;= 5

	    Constructor:
	        new catan.game.DevelopmentCardHand()
		
			Pre-condition:
				none
	        Post-condition:
	        	cards of all types are at 0
	</pre>
	@class game.DevelopmentCardHand
	@constructor
**/
catan.game.DevelopmentCardHand = function()
{
	/**
		Number of year of plenty cards
		@property yearOfPlenty
		@type int
		@default 0
	**/

	/**
		Number of monopoly cards
		@property monopoly
		@type int
		@default 0
	**/

	/**
		Number of soldier cards
		@property soldier
		@type int
		@default 0
	**/

	/**
		Number of road building cards
		@property roadBuilding
		@type int
		@default 0
	**/

	/**
		Number of monument cards
		@property monument
		@type int
		@default 0
	**/

	for (var i in catan.definitions.CardTypes)
		this[catan.definitions.CardTypes[i]] = 0;
		
	return {
		/**
			<pre>					
				Pre-condition:
					object is a valid DevelopmentCardHand
				Post-condition:
					The specified card type will not exceed it's card type limit.
			</pre>
			@method addCard
			@param {catan.definitions.CardTypes} cardToAdd The card being added to this hand
			@return {boolean} True for success, False for failure
		*/
		addCard: function(cardToAdd) {
			return // TODO
		},
		
		/**
			<pre>					
				Pre-condition:
					object is a valid DevelopmentCardHand
				Post-condition:
					The specified card type will not be below zero.
			</pre>
			@method removeCard
			@param {catan.definitions.CardTypes} cardToRemove The card being removed from this hand
			@return {boolean} True for success, False for failure
		*/
		removeCard: function(cardToRemove) {
			return // TODO
		}
	}
};
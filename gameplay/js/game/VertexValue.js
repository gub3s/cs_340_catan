/**
	The Map module contains various Map-related components
	@namespace map
	@module catan.map
**/

var catan = catan || {};
catan.map = catan.map || {};

/**
The VertexValue Class
<pre>
	Domain:
		ownerID: OrderID
		worth: Worth
		
	Invariants:
	
	Constructor Specifications
		VertexValue().Pre-conditions:
			N/A
		VertexValue().Post-conditions:
			var value === a new VertexValue object AND
			var worth === a new Worth object
</pre>
@class map.VertexValue
@constructor
*/
catan.map.VertexValue = function VertexValue() {
	var ownerID = new catan.map.OrderID();
	var worth = new catan.map.Worth();
	
	return {
		/**
			<pre>
			Updates the VertexValue from the specified JSON object
				Pre-condition:
					VertexValue is a valid VertexValue object
				Post-condition:
					The VertexValue is updated
			</pre>
			@method update
			@param {JSON} jsonModel The JSON to update the VertexValue from
			@return {void}
		*/
		update: function(jsonModel) {
			ownerID.setOwnerID(jsonModel.ownerID);
			worth.setWorth(jsonModel.worth);
			return;
		},
		
		/**
			<pre>
			Returns the ID of the player that owns this VertexValue object.
				Pre-condition:
					None
				Post-condition:
					result = ownerID
			</pre>
			@method getOwnerID
			@return {OrderID} ID of the player that owns this object, -1 if no one has built here.
		*/
		getOwnerID: function() {
			return ownerID;
		},
		
		/**
			<pre>
			Sets the current ownerID to the specified OrderID object
				Pre-condition:
					newOwner is between 0 - 3 inclusive
				Post-condition:
					ownerID === newOwner
			</pre>
			@method setOwnerID
			@param {OrderID} newOwner The ID of the player that now owns the Vertex
			@return {Void}
		*/
		setOwnerID: function(newOwner) {
			ownerID = newOwner;
			return;
		},
		
		/**
			<pre>
			Returns the worth of any municipalities built on the Vertex this
			object represents.
				Pre-condition:
					None
				Post-condition:
					result = number value of worth
			</pre>
			@method getValue
			@return {Number} Worth of this object.
		*/
		getWorth: function() {
			return worth.getWorth();
		},
		
		/**
		<pre>
		Sets the worth of the vertex.
			Pre-condition:
				None
			Post-condtion:
				this.worth.value === newValue
		</pre>
		@method setWorth
		@param {Number} newValue The new worth of the Vertex
		@return {Void}
		*/
		setWorth: function(newValue) {
			return worth = newValue;
		}
	}
};
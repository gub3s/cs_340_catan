/**
	The Game module contains various components of the gameplay logic
	@namespace game
	@module catan.game
**/

var catan = catan || {};
catan.game = catan.game || {};

/**
	<pre>
		The ResourceCard class represents a resource
		card in the game.
	
		Domain:
			model: object
			
		Invariant:
			none

		Constructor:
			new catan.game.ResourceCard(jsonModel)
				jsonModel: The string defined in catan.definitions.CardTypes that represents
					this resource card.

				Pre-condition:
					The JSON model is part of catan.definitions.ResourceTypes
				Post-condition:
					This ResourceCard will be filled out by the model
	</pre>
	
	@class game.ResourceCard
**/
catan.game.ResourceCard = function(jsonModel)
{
	var type = jsonModel;
	
	return {
		/**
			<pre>
				Returns the type of this card

				Pre-condition:
					jsonModel used in constructor was vaild
				Post-condition:
					No changes
			</pre>
	
			@method getCardType
			@return {catan.definitions.ResourceTypes} Name of ResourceCard type
		**/
		getCardType: function() {
			return type;
		}
	}
}
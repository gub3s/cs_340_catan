/**
	The Map module contains various Map-related components
	@namespace map
	@module catan.map
**/

var catan = catan || {};
catan.map = catan.map || {};

/**
The Worth Class
Worth corresponds to the worth of a Vertex. A 0 means there
are no municipalities at that Vertex. A 1 is a settlement.
A 2 is a city.
<pre>
	Domain:
		value: Number
		
	Invariants:
		value is always initialized as 0
		value must be either 0, 1, or 2
		
	Constructor Specifications
		Worth().Pre-conditions:
			N/A
		Worth().Post-conditions:
			var value === 0
</pre>
@class map.Worth
@constructor
*/
catan.map.Worth = function Worth() {
	var value = 0;
	
	return {
		/**
		<pre>
		Gets the worth of the vertex.
			Pre-condition:
				None
			Post-condtion:
				result = value
		</pre>
		@method getWorth
		@return {Number} The worth of the Vertex
		*/
		getWorth: function() {
			return value;
		},
		
		/**
		<pre>
		Sets the worth of the vertex.
			Pre-condition:
				newValue must be between 0 - 2, inclusive
			Post-condtion:
				value === newValue
		</pre>
		@method setWorth
		@param {Number} newValue The new worth value of the Vertex
		@return {void}
		*/
		setWorth: function(newValue) {
			value = newValue;
			return;
		}
	}
};
/**
	The Game module contains various components of the gameplay logic
	@namespace game
	@module catan.game
**/

var catan = catan || {};
catan.game = catan.game || {};

/**
	The Communicator class manages communication with the server

	<pre>
		Domain:
			url: string
			port: integer

		Constructor:
			new Communicator(url,port)
				url: The url of the server
				port: The port number of the server
				success: The success callback (update the model!)
				failure: The failure callback

				Pre-condition:
					The url is a valid host and port number is a valid port number (integer)
				Post-condition:
					The Communicator will have url and port set, ready to issue commands and requests to the server
	</pre>
	
	@class game.Communicator
**/

catan.game.Communicator = function(success, failure) {
	var self = this;
	this.success = success;
	this.failure = failure;
	
	/**
		<pre>
			Performs a GET request to the server

			Pre-condition:
				The path and data passed to the function are valid
			Post-condition:
				Either the success or failure callback will be executed asychronously
				The success callback will be passed the server's response
		</pre>

		@method get
		@param path {string} The path for the server handler
	**/			
	this.get = function(path) {
		$.getJSON(path).done(self.success).fail(self.failure);
	};
		
	/**
		<pre>
			Performs a POST request to the server with JSON data

			Pre-condition:
				The path and data passed to the function are valid
			Post-condition:
				Either the success or failure callback will be executed asychronously
				The success callback will be passed the server's response
		</pre>

		@method post
		@param path {string} The path for the server handler
		@param data {string} The data for the server handler to process
	**/
	this.post = function(path, data) {
		$.ajax({
			type: 'POST',
			contentType: 'application/json',
			dataType: 'json',
			url: path,
			data: typeof data === "string" ? data : JSON.stringify(data)
		}).done(self.success).fail(self.failure);
	};

	/**
		<pre>
			Performs a POST request to the server with form-urlencoded data

			Pre-condition:
				The path and data passed to the function are valid
			Post-condition:
				Either the success or failure callback will be executed asychronously
				The success callback will be passed the server's response
		</pre>

		@method postForm
		@param path {string} The path for the server handler
		@param data {string} The data for the server handler to process
	**/
	this.postForm = function(path, data) {
		$.ajax({
			type: 'POST',
			url: path,
			data: data
		}).done(self.success).fail(self.failure);
	};
}
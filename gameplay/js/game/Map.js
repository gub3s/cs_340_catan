/**
	The Map module contains various Map-related components
	@namespace map
	@module catan.map
**/

var catan = catan || {};
catan.map = catan.map || {};

/**
The Map Class
<pre>
	Domain:
		hexGrid: HexGrid
		numbers: Numbers
		ports: Sequence&lt;Port&gt;
		radius: Radius
		robber: HexLocation
		lasRobberLoc: HexLocation
	
	Invariants:
		Each object in the domain will not be null.
	
	Constructor Specifications
		Map(hexGrid, numbers, ports, radius, robber).Pre-condition:
			hexGrid &ne; null AND
			numbers &ne; null AND
			ports &ne; null AND
			radius &ne; null AND
			robber &ne; null
		Map(hexGrid, numbers, ports, radius, robber).Post-condition:
			var hexGrid === hexGrid AND
			var numbers === numbers AND
			var ports === ports AND
			var radius === radius AND
			var robber === robber AND
			var lasRobberLoc === null AND
			var currentLongestRoad === -1
</pre>
@class map.Map
@constructor
@param {HexGrid} hexGrid Defines the grid layout for all Hexes
@param {Numbers} numbers Defines the location of number chits
@param {Sequence&lt;Port&gt;} ports Ports on the map
@param {Radius} radius The map radius
@param {HexLocation} robber The hex location of the robber
*/
catan.map.Map = function (jsonModel) {
	var hexGrid = new catan.map.HexGrid(jsonModel.map.hexGrid);
	var numbers = new catan.map.Numbers(jsonModel.map.numbers);
	var ports = [];
	// Add ports
	for (var i = 0; i < jsonModel.map.ports.length; i++) {
		ports.push(new catan.map.Port(jsonModel.map.ports[i]));
	}
	var radius = new catan.map.Radius(jsonModel.map.radius);
	var robber = new catan.map.HexLocation(jsonModel.map.robber);
	var currentLongestRoad = new catan.map.OrderID();
	
	// Needed?
	var _model;

	$(catan).on('newmodel', function(event, data)
	{
		_model = data;
		hexGrid.update(data.map.hexGrid);
	});


	
	return {
		/**
			<pre>
			Updates the map and all its dependencies from the specified JSON
				Pre-conditions:
					Map is a valid map object
					Map has been initialized previous to calling this method
				Post-conditions:
					The map and dependencies are updated to the newest model
			</pre>
			@method upateMap
			@param {JSON} jsonModel The JSON model to update information from
		*/
		update: function(jsonModel) {
			hexGrid.update(jsonModel.map.hexGrid);
			robber.update(jsonModel.map.robber);
			return;
		},
		
		/**
			<pre>
			Returns the map's HexGrid object
				Pre-conditions:
					Map is a valid map object
				Post-conditions:
					result = hexGrid
			</pre>
			@method getHexGrid
			@return {HexGrid} The HexGrid of this map
		*/
		getHexGrid: function() {
			return hexGrid;
		},
		
		/**
			<pre>
			Returns the map's Numbers object
				Pre-conditions:
					Map is a valid map object
				Post-conditions:
					result = numbers
			</pre>
			@method getNumbers
			@return {Numbers} The Numbers of this map
		*/
		getNumbers: function() {
			return numbers;
		},
		
		/**
			<pre>
			Returns the map's Ports object
				Pre-conditions:
					Map is a valid map object
				Post-conditions:
					result = ports
			</pre>
			@method getPort
			@return {Ports} The Ports of this map
		*/
		getPorts: function() {
			return ports;
		},
		
		/**
			<pre>
			Returns the map's Ports object
				Pre-conditions:
					Map is a valid map object
					Index is between 0 - 8, inclusive
				Post-conditions:
					result = ports[index]
			</pre>
			@method getPort
			@param {Number} index The index of the desired port in the sequence
			@return {Ports} The Port at the specified index
		*/
		getPort: function(index) {
			return ports[index];
		},
		
		/**
			<pre>
			Returns the map's radius
				Pre-conditions:
					Map is a valid map object
				Post-conditions:
					result = radius
			</pre>
			@method getRadius
			@return {number} The Radius of this map
		*/
		getRadius: function() {
			return radius;
		},
		
		/**
			<pre>
			Returns the location of the map's robber
				Pre-conditions:
					Map is a valid map object
				Post-conditions:
					result = robber
			</pre>
			@method getRobber
			@return {HexLocation} The Robber of this map
		*/
		getRobber: function() {
			return robber;
		},
		
		/**
			<pre>
			Returns the ID of the player that owns the current longest road
				Pre-conditions:
					Map is a valid map object
				Post-conditions:
					result = currentLongestRoad
			</pre>
			@method getCurrentLongestRoad
			@return {OrderID} The ID of the player that has the current longest road
		*/
		getCurrentLongestRoad: function() {
			return currentLongestRoad;
		},
		
		/**
			<pre>
			Returns the ID of the player that owns the current longest road
				Pre-conditions:
					Map is a valid map object
					newOwner.id is between 0 - 3, inclusive
				Post-conditions:
					currentLongestRoad === newOwner
			</pre>
			@method setcurrentLongestRoad
			@param {OrderID} newOwner The ID of the player that now owns the longes road
			@return {void}
		*/
		setCurrentLongestRoad: function(newOwner) {
			currentLongestRoad = newOwner;
		},
		
		/**
			<pre>
			Determines if the specified player can place a settlement on the specified Vertex
			Hex.
				Pre-conditions
					Map is a valid map object AND
					hexLocation is a valid HexLocation AND
					direction is a real direction, specified in the VertexLocation class
				Post-conditions
					None
			</pre>
			@method canPlaceSettlement
			@param {VertexLocation} vertexLocation The vertex to be checked
			@param {Boolean} isSetup Whether or not in the setup round
			@return {Boolean} True if player can place a settlement at the specified Hex,
				false otherwise
		*/
		canPlaceSettlement: function(vertexLocation, isSetup) {
			if (hexGrid.canPlaceSettlement(vertexLocation, isSetup)) {
				return true;
			}
			else {
				return false;
			}
		},
		
		/**
			<pre>
			Determines if the specified player can place a city on the specified Vertex
				Pre-conditions:
					Map is a valid map object AND
					playerID is between 0 - 3, inclusive AND
					hexLocation is a valid HexLocation AND
					direction is a real direction, specified in the VertexLocation class
				Post-conditions
					None
			</pre>
			@method canPlaceCity
			@param {OrderID} playerID The player desiring to place a city
			@param {HexLocation} hexLocation The hex to be checked
			@param {String} direction The direction of the vertex to be checked
			@return {Boolean} True if player can place a city at the specified Hex,
				false otherwise
		*/
		canPlaceCity: function(playerID, hexLocation, direction) {
			if (hexGrid.canPlaceCity(playerID, hexLocation, direction)) {
				return true;
			}
			else {
				return false;
			}
		},
		
		/**
			<pre>
			Determines if the specified player can place a road on the specified Edge.
				Pre-conditions:
					Map is a valid map object AND
					hexLocation is a valid HexLocation AND
					direction is a real direction, specified in the EdgeLocation class
				Post-conditions:
					None
			</pre>
			@method canPlaceRoad
			@param {HexLocation} hexLocation The hex to be checked
			@param {String} direction The direction of the vertex to be checked
			@return {Boolean} True if player can place the road, false otherwise
		*/
		canPlaceRoad: function(loc, setup) {
			if (hexGrid.canPlaceRoad(loc, setup)) {
				return true;
			}
			else {
				return false;
			}
		},
		
		/**
			<pre>
			Determines if the specified HexLocation is a valid location to place the robber
				Pre-conditions:  
					Map is a valid map object AND
					newLocation exists on the Map
				Post-conditions:  
					The HexLocation of the Robber is returned.
			</pre>
			@method canPlaceRobber
			@param {HexLocation} newLocation The HexLocation to check as a valid location to
				place the robber
			@return {Boolean} True if specified location is a valid place to move the robber,
				false otherwise
		*/
		canPlaceRobber: function(newLocation) {
			if (newLocation.x === robber.getX() && newLocation.y === robber.getY()) {
				return false;
			}
			else {
				var location = new catan.map.HexLocation(newLocation); // This is so convert_cord_to_indexes in HexGrid works
				return hexGrid.canPlaceRobber(location);
			}
		},
		
		/**
            <pre>
            Changes the robber's location to the specified hex.
                Pre-conditions:
                    Map is a valid map object, newLocation is a valid HexLocation
                Post-conditions:
                    robber === newLocation
            </pre>
            @method placeRobber
            @param {Object} newLocation Object with x and y values that represents the new robber location
            @return {Void}
        */
        placeRobber: function(newLocation) {
            var newRobber = new catan.map.HexLocation(newLocation);
            robber = newRobber;
        },
        
        /**

		
		/**
			<pre>
			Determines which players have municipalities on a specified hex where the robber may be placed.
			If the robber is placed on that hex, those players are considered robbable.  Returns a set of
			player ID's that are robbable based on the specified hex.
				Pre-conditions:
					Map is a valid map object
				Post-conditions:
					result = a set of robbable players
			</pre>
			@method getRobbablePlayers
			@param {Object} loc Object with x and y values that represent the location to check
			@return {Set&lt;OrderID&gt;} Set of player ID's that are robbable
		*/
		getRobbablePlayers: function(loc) {
			var hexLoc = new catan.map.HexLocation(loc);
			return hexGrid.getRobbablePlayers(hexLoc);
		},
		
		/**
			<pre>
			Gets Map of players and their ports.  The player ID, represented as an OrderID is the key,
			and the Ports owned by a player are the values.
				Pre-conditions:
					Map is a valid map object
				Post-conditions:
					result = Map&lt;OrderID,Port&gt;
			</pre>
			@method getPlayerPorts
			@param {Number} playerID The ID of the player whose ports are being requested
			@return {Sequence&lt;Port&gt;} All ports owned by the player
		*/
		getPlayerPorts: function(playerID) {
			var i, playerPorts = [];
			
			for (i = 0; i < ports.length; i++) {
				if (hexGrid.playerOwnsPort(playerID, ports[i])) {
					playerPorts.push(ports[i]);
				}
			}
			return playerPorts;
		}
	}
};
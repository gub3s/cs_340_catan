/**
	The Game module contains various components of the gameplay logic
	@namespace game
	@module catan.game
**/

var catan = catan || {};
catan.game = catan.game || {};


/**
	The ResourceHand class represents a group of resource cards
	<pre>
		Domain:
			wood:  int
			brick: int
			sheep: int
			wheat: int
			ore:   int
		Invariant:
			none

	    Constructor:
	        new catan.game.ResourceHand()
		
			Pre-condition:
				none
	        Post-condition:
	        	resources of all types are at 0
	</pre>
	@class game.ResourceHand
	@constructor
**/
catan.game.ResourceHand = function()
{
	var self = this;
	/**
		Number of wood resources
		@property wood
		@type int
		@default 0
	**/

	/**
		Number of brick resources
		@property brick
		@type int
		@default 0
	**/

	/**
		Number of sheep resources
		@property sheep
		@type int
		@default 0
	**/

	/**
		Number of wheat resources
		@property wheat
		@type int
		@default 0
	**/

	/**
		Number of ore resources
		@property ore
		@type int
		@default 0
	**/

	for (var i in catan.definitions.ResourceTypes)
		this[catan.definitions.ResourceTypes[i]] = 0;
		
	/**
		<pre>					
			Pre-condition:
				object is a valid ResourceHand
			Post-condition:
				none
		</pre>
		@method addCard
		@param {catan.definitions.Resources} cardToAdd The card being added to this hand
		@return {boolean} True for success, False for failure
	*/
	this.addCard = function(cardToAdd) {
		self[cardToAdd] = (self[cardToAdd] || 0) + 1;
		return self[cardToAdd];
	};
			
	/**
		<pre>					
			Pre-condition:
				object is a valid ResourceHand
			Post-condition:
				none
		</pre>
		@method removeCard
		@param {catan.definitions.Resources} cardToRemove The card being removed from this hand
		@return {boolean} True for success, False for failure
	*/
	this.removeCard = function(cardToRemove) {
		self[cardToRemove] = Math.max((self[cardToRemove] || 1) - 1, 0);
		return self[cardToRemove];
	};
		
	/**
		<pre>					
			Pre-condition:
				object is a valid ResourceHand
			Post-condition:
				both objects are unchanged
		</pre>
		@method hasCardSet
		@param {ResourceHand} compareTo The cards this hand is being checked for
		@return {boolean} True for owning all cards in compareTo, False otherwise
	*/
	this.hasCardSet = function (compareTo) {
		return // TODO
	};
};
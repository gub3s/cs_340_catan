/**
	The Map module contains various Map-related components
	@namespace map
	@module catan.map
**/

var catan = catan || {};
catan.map = catan.map || {};

/**
The VertexLocation Class
<pre>
	Domain:
		direction: String
		
	Invariants:
		direction will be 'NW' or 'N' or 'NE' or 'SE' or 'S' or 'SW'
		inDirection must be between 0 - 5, inclusive
		
	Constructor Specifications
		VertexLocation().Pre-conditions:
			N/A
		VertexLocation().Post-conditions:
			var direction === inDirection in string format
</pre>
@class map.VertexLocation
@param {Number} inDirection The direction of the vertex
@constructor
*/
catan.map.VertexLocation = function VertexLocation(inX, inY, inDirection) {
	var x = inX;
	var y = inY;
	var direction = inDirection;
	
	return {
	
		getX: function() {
			return x;
		},
		
		getY: function() {
			return y;
		},
		
		/**
			<pre>
			Gets the direction of the vertex
				Pre-condition:
					None
				Post-condition:
					result = direction
			</pre>
			@method getDirection 
			@return {String} The direction of the vertex
		*/
		getDirection: function() {
			if (typeof direction === "string") {
				return direction;
			}
			else {
				var result;
				switch (direction) {
					case 0:
						result = "W";
						break;
					case 1:
						result = "NW";
						break;
					case 2:
						result = "NE";
						break;
					case 3:
						result = "E";
						break;
					case 4:
						result = "SE";
						break;
					case 5:
						result = "SW";
						break;
					default:
						console.log("Not a valid direction");
						break;
				}
				return result;
			}
		},
		
		getDirectionAsNum: function() {
			if (typeof direction === "number") {
				return direction;
			}
			else {
				var result;
				switch (direction) {
					case "W":
						result = 0;
						break;
					case "NW":
						result = 1;
						break;
					case "NE":
						result = 2;
						break;
					case "E":
						result = 3;
						break;
					case "SE":
						result = 4;
						break;
					case "SW":
						result = 5;
						break;
					default:
						console.log("Not a valid direction");
						break;
				}
				return result;
			}
		},
		
		/**
			<pre>
			Sets the direction to the specified value
				Pre-condition:
					newDirection must be between 0 - 5 inclusive
				Post-condition:
					None
			</pre>
			@method setDirection 
			@param {Number} newDirection The number representation of the direction
			@return {String} The string representation of the direction
		*/
		setDirection: function(newDirection) {
			direction = newDirection;
		}
	}
};
/**
	The Map module contains various Map-related components
	@namespace map
	@module catan.map
**/

var catan = catan || {};
catan.map = catan.map || {};

/**
The Edge Class
<pre>
	Domain:
		value: EdgeValue
		isOccupied: Boolean
		direction: EdgeLocation
		
	Invariants:
		isOwned is always initialized as "false"
		Direction can never be altered
		Direction is specified as an enumerable
	
	Constructor Specifications
		Edge().Pre-conditions:
			N/A
		Edge().Post-conditions:
			var value === a new EdgeValue object
</pre>
@class map.Edge
@constructor
@param {Number} inDirection The direction of the edge
*/
catan.map.Edge = function Edge(x, y, inDirection) {
	var _value = new catan.map.EdgeValue();
	var isOccupied = false;
	var direction = new catan.map.EdgeLocation(x, y, inDirection);
	
	return {
		/**
			<pre>
			Updates the Edge and its dependencies from the specified JSON object
				Pre-condition:
					Edge is a valid Edge object
				Post-condition:
					The Edge is updated
			</pre>
			@method update
			@param {JSON} jsonModel The JSON to update the Edge from
			@return {void}
		*/
		update: function(jsonModel) {
			// Update value
			_value.update(jsonModel.value);
			return;
		},
		
		/**
			<pre>
			Returns EdgeValue object for this Edge.
				Pre-condition:
					None
				Post-condition:
					result = value
			</pre>
			@method getEdgeValue
			@return {EdgeValue} The EdgeValue object is returned.
		*/
		getEdgeValue: function() {
			return _value;
		},
		
		/**
			<pre>
			Returns if this edge is owned or not.
				Pre-condition:
					None
				Post-condition:
					result = Boolean
			</pre>
			@method isOwned
			@return {Boolean} True if this Edge is owned by a player, false otherwise
		*/
		isOccupied: function() {
			return isOccupied;
		},
		
		/**
			<pre>
			Gets the direction of the edge
				Pre-condition:
					None
				Post-condition:
					result = direction's string form
			</pre>
			@method getDirection
			@return {String} The direction of the edge
		*/
		getDirection: function() {
			return direction.getDirection();
		}
	}
	
};
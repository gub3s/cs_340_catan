/**
	The Game module contains various components of the gameplay logic
	@namespace game
	@module catan.game
**/

var catan = catan || {};
catan.game = catan.game || {};


/**
	<pre>
		The Message class represents either a log message or a
		chat message in the game.

		Domain:
			source: string
			message: string
	
		Constructor:
			new catan.game.Message(jsonModel)
				jsonModel: The JSON object representing either a log message
							or a chat message that was sent by the server.

				Pre-condition:
					The JSON model provided is valid and complete
				Post-condition:
					The source and message will be filled out, and the Message
					object will represent that message 
	</pre>
	
	@class game.Message
**/
catan.game.Message = function(jsonModel)
{
	var model = jsonModel;
	
	return {
		
		/**
			<pre>
				Gets the source of the message

				Pre-condition:
					None
				Post-condition:
					No changes
			</pre>
	
			@method getSource
			@return {string} The source of the message
		**/
		getSource: function() {
			return; //source of message
		},
		
		/**
			<pre>
				Gets the message text of the message

				Pre-condition:
					None
				Post-condition:
					No changes
			</pre>
	
			@method getMessage
			@return {string} The message text of the message
		**/		
		getMessage: function() {
			return // message text of message
		}
	}
};
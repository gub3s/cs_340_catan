/**
	The Game module contains various components of the gameplay logic
	@namespace game
	@module catan.game
**/

var catan = catan || {};
catan.game = catan.game || {};


/**
	The TurnTracker class contains a few static items of state related to gameplay
	<pre>
		Domain
			status: string
			currentTurn: int

		Constructor
			new catan.game.TurnTracker()

			Pre-condition:
				None
			Post-condition:
				None
	</pre>
	@class game.TurnTracker
**/
catan.game.TurnTracker = (function() {

	var _currentTurn;
	var _status;
	var _model;

	$(catan).on('newmodel', function(event, data)
	{
		_model = data;
		_currentTurn = data.turnTracker.currentTurn;
		_status = data.turnTracker.status;
	});

	return {
		/**
			The current status of the game/player
			@property status
			@type string
		**/
		status: function() { return _status; },

		/**
			The player ID (0-3) whose current turn it is, or -1 if nobody's (game not started)
			@property currentTurn
			@type int
		**/
		currentTurn: function() { return _currentTurn; }
	};
})();
/**
	The Map module contains various Map-related components
	@namespace map
	@module catan.map
**/

var catan = catan || {};
catan.map = catan.map || {};

/**
The HexLocation Class
<pre>
	Domain:
		x: Number
		y: Number
	
	Invariants:
		x and y cannot be less than -3
		or greater than 3
	
	Constructor Specifications
		HexLocation(x, y).Pre-conditions:
			x &ne; null AND
			-3 <= x <= 3 AND
			y &ne; null AND
			-3 <= y <= 3
		HexLocation(x, y).Post-conditions:
			var x === x AND
			var y === y
</pre>
@class map.HexLocation
@constructor
@param {Number} inX The x coordinate
@param {Number} inY The y coordinate
*/
catan.map.HexLocation = function HexLocation(jsonModel) {
	var x = jsonModel.x;
	var y = jsonModel.y;
	
	return {
		/**
			<pre>
			Updates the HexLocation from the specified JSON
				Pre-conditions:
					HexLocation is a valid HexLocation object
					HexLocation has been initialized previous to calling this method
				Post-conditions:
					The HexLocation and dependencies are updated to the newest model
			</pre>
			@method update
			@param {JSON} jsonModel The JSON model to update information from
		*/
		update: function(jsonModel) {
			x = jsonModel.x;
			y = jsonModel.y;
		},
		
		/**
		<pre>
		Returns the HexLocation x-coordinate
			Pre-condition:
				A valid X was passed into the HexLocation constructor.
			Post-condition:
				A number representing the x-coordinate is returned.
		</pre>
		@method getX
		@return {Number} The x-coordinate is returned.
		*/
		getX: function() {
			return x;
		},
		
		/**
		<pre>
		Returns the HexLocation y-coordinate
			Pre-condition:
				A valid Y was passed into the HexLocation constructor.
			Post-condition:
				A number representing the y-coordinate is returned.
		</pre>
		@method getY
		@return {Number} The y-coordinate is returned.
		*/
		getY: function() {
			return y;
		},

		x: x,
		y: y
	}
};
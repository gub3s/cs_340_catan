/**
	The Map module contains various Map-related components
	@namespace map
	@module catan.map
**/

var catan = catan || {};
catan.map = catan.map || {};

/**
The EdgeLocation Class
<pre>
	Domain: 
		direction: String
		
	Invariants:
		direction will be 'W' or 'NW' or 'NE' or 'E' or 'SE' or 'SW'
		inDirection must be between 0 - 5, inclusive
		
	Constructor Specifications
		EdgeLocation().Pre-conditions:
			N/A
		EdgeLocation().Post-conditions:
			var direction === inDirection in string format
</pre>
@class map.EdgeLocation
@param {Number} inX The x-coordinate of the hex this edge belongs to
@param {Number} inY The y-coordinate of the hex this edge belong to
@param {Number} inDirection The direction of the edge
@constructor
*/
catan.map.EdgeLocation = function EdgeLocation(inX, inY, inDirection) {
	var x = inX;
	var y = inY;
	var direction = inDirection;

	return {
		/**
		<pre>
		Gets the direction of the Edge
			Pre-condition:
				None
			Post-condition:
				result = direction
		</pre>
		@method getDirection
		@return {String} The direction of the vertex
		*/
		getDirection: function() {
			switch (direction) {
				case 0:
					return "NW";
				case 1:
					return "N";
				case 2:
					return "NE";
				case 3:
					return "SE";
				case 4:
					return "S";
				case 5:
					return "SW";
				default:
					throw("Not a valid direction");
			}
		},

		equals: function(other) {
			return false;	// TODO
		},
		
		/**
		<pre>
		Sets the direction to the specified value
			Pre-condition:
				newDirection must be between 0 - 5 inclusive
			Post-condition:
				None
		</pre>
		@method setDirection 
		@param {Number} newDirection The number representation of the direction
		@return {String} The string representation of the direction
		*/
		setDirection: function(newDirection) {
			direction = newDirection;
		}
	}
};
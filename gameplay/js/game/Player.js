/**
	The Game module contains various components of the gameplay logic
	@namespace game
	@module catan.game
**/

var catan = catan || {};
catan.game = catan.game || {};


/**
	The Player class represents a player of the game

	<pre>
		Domain
			MAX_GAME_POINTS: int
			resources: ResourceHand
			oldDevCards: DevCardHand
			newDevCards: DevCardHand
			roads: int
			cities: int
			settlements: int
			soldiers: int
			victoryPoints: int
			monuments: int
			longestRoad: bool
			largestArmy: bool
			playedDevCard: bool
			discarded: bool
			playerID: int
			orderNumber: int
			name: string
			color: string


		Constructor
			new catan.game.Player(jsonModel)
				jsonModel: The JSON object representing a player that was sent by the server

			Pre-condition:
				The provided model is valid and complete
			Post-condition:
				The Player object will be filled out will values from the model

			Invariants
				jsonModel is not null (obviously)
	</pre>

	@class game.Player
**/
catan.game.Player = function(jsonModel)
{
	var self = this;
	this.model = jsonModel;

	this.update = function(jsonModel) {
			self.model = jsonModel;
		};
	/**
		<pre>
			Resources currently in the player's hand

			Pre-condition:
				None
			Post-condition:
				No changes
		</pre>
		@method getResources
		@return {object} An object of card counts, keyed by resources
	**/
	this.getResources = function()
	{
		return self.model.resources;	// NOTE: This is a reference, not a copy!
	};


	/**
		<pre>
			Whether a player has resources of the given type and quantity

			Pre-condition:
				res is an object of ints keyed by string
			Post-condition:
				No changes
		</pre>
		@method hasResources
		@param {object} res An object of resource quantities, keyed by resource type
		@return {object} An object of booleans keyed by resource type
	**/
	this.hasResources = function(res)
	{
		// Pre-condition is validated implicitly
		var answer = {};
		for (var key in res)
			answer[key] = self.model.resources[key] >= res[key]
		return answer;
	};


	/**
		<pre>
			Whether a player has ALL specified resources of the given type and quantity

			Pre-condition:
				res is an object of ints keyed by string
			Post-condition:
				No changes
		</pre>
		@method hasAllResources
		@param {object} res An object of resource quantities, keyed by resource type
		@return {boolean} True, if the player has all queried resources; false otherwise
	**/
	this.hasAllResources = function(res)
	{
		var has = self.hasResources(res);
		for (var key in has)
			if (!has[key])
				return false;
		return true;
	};



	/**
		<pre>
			Determines what the player can buy between roads, settlements, cities, and dev cards

			Pre-condition:
				None
			Post-condition:
				No changes
		</pre>
		@method canBuy
		@return {object} An object of boolean values keyed by items the player can buy
	**/
	this.canBuy = function()
	{
		var costs = {
			Roads: { brick: 1, wood: 1 },
			Settlements: { brick: 1, wood: 1, wheat: 1, sheep: 1 },
			Cities: { ore: 3, wheat: 2 },
			BuyCard: { sheep: 1, wheat: 1, ore: 1 }
		};
		var buyable = {};
		
		for (var key in costs)
			buyable[key] = self.hasAllResources(costs[key]);

		if (buyable.Roads)
			buyable.Roads = self.model.roads > 0;
		if (buyable.Settlements)
			buyable.Settlements = self.model.settlements > 0;
		if (buyable.Cities)
			buyable.Cities = self.model.cities > 0;

		return buyable;
	};


	/**
		<pre>
			Calculates the number of cards this player must discard
		
			Pre-condition:
				None
			Post-condition:
				No changes
		</pre>
		@method mustDiscard
		@return {int} The number of cards the player must discard
	**/
	this.mustDiscard = function()
	{
		var sum = 0;
		for (var key in self.model.resources)
			sum += self.model.resources[key];
		return sum > 7 ? Math.floor(sum / 2) : 0;
	};


	/**
		<pre>
			Rolls the dice on behalf of this player
		
			Pre-condition:
				It is the player's turn and the player has not rolled
			Post-condition:
				The turn leaves the pre-roll phase
		</pre>
		@method roll
		@return {int} The sum of the two dice that were rolled
	**/
	this.roll = function()
	{
		if (!catan.models.ClientModel.isLocalsTurn())
			throw("It is not your turn to roll");

		var min = 1, max = 6;
		var d1 = Math.floor(Math.random() * (max - min + 1) + min);
		var d2 = Math.floor(Math.random() * (max - min + 1) + min);

		return d1 + d2;
	};
};
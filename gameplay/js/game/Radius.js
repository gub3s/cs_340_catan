/**
	The Map module contains various Map-related components
	@namespace map
	@module catan.map
**/

var catan = catan || {};
catan.map = catan.map || {};

/**
The Radius Class
<pre>
	Domain:
		value: Number
		
	Invariants:
		
	Constructor Specifications
		Radius().Pre-conditions:
			value &ne; null
		Radius().Post-conditions:
			var value === value
</pre>
@class map.Radius
@constructor
@param {Number} inValue The radius of the map
*/
catan.map.Radius = function Radius(inValue) {
	var value = inValue;
	
	return {
		/**
		<pre>
		Gets the radius' value
			Pre-condition:
				None
			Post-condtion:
				result = value
		</pre>
		@method getRadius
		@return {Number} The radius' value
		*/
		getRadius: function() {
			return value;
		}
	}
};
/**
	The Game module contains various components of the gameplay logic
	@namespace game
	@module catan.game
**/

var catan = catan || {};
catan.game = catan.game || {};

/**
	The ServerProxy class is a facade of the server's functionality

	<pre>
		Domain:
			communicator: Communicator

		Constructor:
			new ServerProxy(success, failure)
				success: The callback success function
				failure: The callback failure function

				Pre-condition:
					The url is a valid host and port number is a valid port number (integer)
				Post-condition:
					The ServerProxy will have Communicator set, ready to issue commands and requests to the server
	</pre>
	
	@class game.ServerProxy
**/


catan.game.ServerProxy = function(success, failure) {
	var communicator = new catan.game.Communicator(success, failure);
	
	return {
		/**
			<pre>
				Logs a player into the client

				Pre-condition:
					None
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>

			@method login
			@param username {string} The username for the user
			@param password {string} The password for the user
		**/
		login: function(username, password) {
			communicator.postForm('/user/login', {'username' : username, 'password' : password});
		},
		
		/**			
			<pre>
				Registers a username and password to the client

				Pre-condition:
					None
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
	
			@method register
			@param username {string} The username for the user
			@param password {string} The password for the user
		**/
		register: function(username, password) {
			communicator.postForm('/user/register', {'username' : username, 'password' : password});
		},
		
		/**
			
			<pre>
				Lists all games on the server

				Pre-condition:
					The User must be logged into the client
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
	
			@method list
		**/
		list: function(success, failure) {
			communicator.get('/games/list');
		},
		
		/**
			<pre>
				Creates a new game.

				Pre-condition:
					The User must be logged into the client
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
	
			@method create
			@param {json} a json representation of a create command
		**/
		create: function(data) {
			communicator.post('/games/create', data);
		},
		
		/**
			<pre>
				Joins a client to a game

				Pre-condition:
					The User must be logged into the client
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
	
			@method joinGame			
			@param {json} a json representation of a join game command
		**/
		joinGame: function(data) {
			communicator.postForm('/games/join', data);
		},
		
		/**
			<pre>
				A model of the currently joined game
				Pre-condition:
					The User must be logged into the client and have joined a game
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>

			@method model
		**/
		model: function() {
			communicator.get('/game/model');
		},
		
		/**
			<pre>
				Resets the model of a game
				Pre-condition:
					The User must be logged into the client and have joined a game
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
	
			@method reset
		**/
		reset: function() {
			communicator.post('/game/reset', {});
		},
		
		/**
			<pre>
				Sends a list of moves to the server
				Pre-condition:
					The User must be logged into the client and have joined a game
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>

			@method commands
			@param listOfCommands {Sequence of JSON Objects} The list of commands performed
		**/
		commands: function(listOfCommands) {
			var command = catan.commands.Commands(listOfCommands)
			communicator.post('/game/commands', command);
		},
		
		/**
			<pre>
				Gets a list of moves to the server
				Pre-condition:
					The User must be logged into the client and have joined a game
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
	
			@method commands
			@return {string} A list of commands that have been performed
		**/
		commands: function() {
			communicator.get('/game/commands');
		},
		
		/**
			<pre>
				Adds an AI player to your game
				Pre-condition:
					The User must be logged into the client and have joined a game 
					There must also be less than 4 players joined to the current game
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
	
			@method addAI
			@param data {Form} Form from the UI
		**/
		addAI: function(data) {
			communicator.postForm('/game/addAI', data);
		},
		
		/**
			<pre>
				Gets a list of AI players for your game
				Pre-condition:
					The User must be logged into the client and have joined a game
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
	
			@method listAI
		**/
		listAI: function(success, failure) {
			communicator.get('/game/listAI');
		},
		
		/**
			<pre>
				Set's the server's log level
				(ALL, SEVERE, WARNING, INFO, CONFIG,FINE, FINER, FINEST, OFF)
				Pre-condition:
					The User must be logged into the client
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
				</pre>
	
			@method changeLogLevel
			@param data {Form} The new log level from UI
		**/
		changeLogLevel: function(data) {
			communicator.postForm('/util/changeLogLevel', data);
		},
		
		/**
			<pre>
				Sends a chat message.
				
				Pre-condition:
					The User must be logged into the client and have joined a game
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>

			@method sendChat
			@param chatJSON {object} <pre>The chat message to be sent
				Example:
				{
					"type": "sendChat",
					"playerIndex": -1,
					"content": ""
				}</pre>
		**/
		sendChat: function(playerIndex, content) {
			var command = catan.commands.SendChat(playerIndex, content);
			communicator.post('/moves/sendChat', command);
		},
		
		/**
			<pre>
				Used to roll a number at the beginning of your turn.
				
				Pre-condition:
					The User must be logged into the client and have joined a game
					It must also be the players turn
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
			
			@method rollNumber
			@param {object} rollJSON <pre>Example:
				{
					"type": "rollNumber",
					"playerIndex": -1,
					"number": -1
				}</pre>
		**/
		rollNumber: function(playerIndex, num) {
			var command = catan.commands.RollNumber(playerIndex, num);
			communicator.post('/moves/rollNumber', command);
		},
		
		/**
			<pre>
				Used to finish your turn
				
				Pre-condition:
					The User must be logged into the client and have joined a game
					It must also be the user's turn for them to end it
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
			
			@method finishTurn
			@param {object} finishJSON <pre>Example:
				{
					"type": "finishTurn",
					"playerIndex": -1
				}</pre>
		**/
		finishTurn: function(playerIndex) {
			var command = catan.commands.FinishTurn(playerIndex)
			communicator.post('/moves/finishTurn', command);
		},
		
		/**
			<pre>
				Buys a development card for yourself
				
				Pre-condition:
					The User must be logged into the client and have joined a game
					It must also be the user's turn
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
			
			@method buyDevCard
			@param {object} buyDevCardJSON <pre>Example:
				{
					"type": "buyDevCard",
					"playerIndex": -1
				}</pre>
		**/
		buyDevCard: function(playerIndex) {
			var command = catan.commands.BuyDevCard(playerIndex);
			communicator.post('/moves/buyDevCard', command);
		},
		
		/**
			<pre>
				Plays a 'Year of Plenty' card from your hand to gain the two specified resources.
				
				Pre-condition:
					The User must be logged into the client and have joined a game
					It must also be the user's turn
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
			
			@method yearOfPlenty
			@param {object} yearOfPlentyJSON <pre>Example:
				{
					"type": "Year_of_Plenty",
					"playerIndex": -1,
					"resource1": "Resource 1",
					"resource2": "Resource 2"
				}</pre>
		**/
		yearOfPlenty: function(playerId, res1, res2) {
			var command = catan.commands.YearOfPlenty(playerId, res1, res2);
			communicator.post('/moves/Year_of_Plenty', command);
		},
		
		/**
			<pre>
				Plays a 'Road Building' card from your hand to build at the two spots specified.
				
				Pre-condition:
					The User must be logged into the client and have joined a game
					It must also be the user's turn
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
			
			@method roadBuilding
			@param {object} roadBuildingJSON <pre>Example:
				{
					"type": "Road_Building",
					"playerIndex": -1,
					"spot1": {
						"x": -1,
						"y": -1,
						"direction": ""
					},
					"spot2": {
						"x": -1,
						"y": -1,
						"direction": ""
					}
				}</pre>
		**/
		roadBuilding: function(playerIndex, x1, y1, dir1, x2, y2, dir2) {
			var command = catan.commands.RoadBuilding(playerIndex, x1, y1, dir1, x2, y2, dir2);
			communicator.post('/moves/Road_Building', command);
		},
		
		/**
			<pre>
				Plays a 'Soldier' from your hand, selecting the new robber position and player to rob.
				
				Pre-condition:
					The User must be logged into the client and have joined a game
					It must also be the user's turn
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
			
			@method soldier
			@param {object} soldierJSON <pre>Example:
				{
					"type": "Soldier",
					"playerIndex": -1,
					"victimIndex": -1,
					"location": {
						"x": "",
						"y": ""
					}
				}</pre>
		**/
		soldier: function(playerIndex, victimIndex, x, y) {
			var command = catan.commands.Soldier(playerIndex, victimIndex, x, y);
			communicator.post('/moves/Soldier', command);
		},
		
		/**
			<pre>
				Plays a 'Monopoly' card from you hand to monopolize the requested resource.
				
				Pre-condition:
					The User must be logged into the client and have joined a game
					It must also be the user's turn
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
			
			@method monopoly
			@param {object} monopolyJSON <pre>Example:
				{
					"type": "Monopoly",
					"resource": "",
					"playerIndex": -1
				}</pre>
		**/
		monopoly: function(playerIndex, res) {
			var command = catan.commands.Monopoly(playerIndex, res);
			communicator.post('/moves/Monopoly', command);
		},
		
		/**
			<pre>
				Plays a 'Monument' card from your hand to give you a point.
				
				Pre-condition:
					The User must be logged into the client and have joined a game
					It must also be the user's turn
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
			
			@method monument
			@param {object} monumentJSON <pre>Example:
				{
					"type": "Monument",
					"playerIndex": -1
				}</pre>
		**/
		monument: function(playerIndex) {
			var command = catan.commands.Monument(playerIndex);
			communicator.post('/moves/Monument', command);
		},
		
		/**
			<pre>
				Builds a road for the specified player at the specified spot. 
				Set true to free if it's during the setup.
				
				Pre-condition:
					The User must be logged into the client and have joined a game
					It must also be the user's turn
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
			
			@method buildRoad
			@param {object} buildRoadJSON <pre>Example:
				{
					"type": "buildRoad",
					"playerIndex": -1,
					"roadLocation": {
						"x": -1,
						"y": -1,
						"direction": ""
					},
					"free": false
				}</pre>
		**/
		buildRoad: function(playerIndex, x, y, dir, free) {
			var command = catan.commands.BuildRoad(playerIndex, x, y, dir, free);
			communicator.post('/moves/buildRoad', command);
		},
		
		/**
			<pre>
				Builds a settlement for the specified player at the specified spot. 
				Set true to free if it's during the setup.
				
				Pre-condition:
					The User must be logged into the client and have joined a game
					It must also be the user's turn
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
			
			@method buildSettlement
			@param {object} buildSettlementJSON <pre>Example:
				{
					"type": "buildSettlement",
					"playerIndex": -1,
					"roadLocation": {
						"x": -1,
						"y": -1,
						"direction": ""
					},
				"free": false
				}</pre>
		**/
		buildSettlement: function(playerIndex, x, y, dir, free) {
			var command = catan.commands.BuildSettlement(playerIndex, x, y, dir, free);
			communicator.post('/moves/buildSettlement', command);
		},
		
		/**
			<pre>
				Builds a city for the specified player at the specified spot.
				Set true to free if it's during the setup.
				
				Pre-condition:
					The User must be logged into the client and have joined a game
					It must also be the user's turn
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
			
			@method buildCity
			@param {object} buildCityJSON <pre>Example:
				{
					"type": "buildCity",
					"playerIndex": -1,
					"roadLocation": {
						"x": -1,
						"y": -1,
						"direction": ""
					},
					"free": false
				}</pre>
		**/
		buildCity: function(playerIndex, x, y, dir) {
			var command = catan.commands.BuildCity(playerIndex, x, y, dir);
			communicator.post('/moves/buildCity', command);
		},
		
		/**
			<pre>
				Offers a trade to another player.
				
				Pre-condition:
					The User must be logged into the client and have joined a game
					It must also be the user's turn
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
			
			@method offerTrade
			@param {object} offerTradeJSON <pre>Example:
				{
					"type": "offerTrade",
					"playerIndex": -1,
					"offer": {
						"brick": -1,
						"ore": -1,
						"sheep": -1,
						"wheat": -1,
						"wood": -1
					},
					"receiver": -1
				}</pre>
		**/
		offerTrade: function(playerIndex, brick, ore, sheep, wheat, wood, rcv) {
			var command = catan.commands.OfferTrade(playerIndex, brick, ore, sheep, wheat, wood, rcv);
			communicator.post('/moves/offerTrade', command);
		},
		
		/**
			<pre>
				Accept or reject a trade offered to you.
				
				Pre-condition:
					The User must be logged into the client and have joined a game
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
			
			@method acceptTrade
			@param {object} acceptTradeJSON <pre>Example:
				{
					"type": "acceptTrade",
					"playerIndex": -1,
					"willAccept": false
				}</pre>
		**/
		acceptTrade: function(playerIndex, accept) {
			var command = catan.commands.AcceptTrade(playerIndex, accept);
			communicator.post('/moves/acceptTrade', command);
		},

		/**
			<pre>
				Performs a maritime trade
				
				Pre-condition:
					The User must be logged into the client and have joined a game
					It must also be the user's turn
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
			
			@method maritimeTrade
			@param {object} requestData <pre>Example:
				{
					"type": "maritimeTrade",
					"playerIndex": -1,
					"ratio": -1,
					"inputResource": "",
					"outputResource": ""
				}</pre>
		**/
		maritimeTrade: function(playerIndex, ratio, inres, outres) {
			var command = catan.commands.MaritimeTrade(playerIndex, ratio, inres, outres);
			communicator.post('/moves/maritimeTrade', command);
		},

		/**
			<pre>
				Robs another player
				
				Pre-condition:
					The User must be logged into the client and have joined a game
					It must also be the user's turn
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
			
			@method robPlayer
			@param {object} requestData <pre>Example:
				{
					"type": "robPlayer",
					"playerIndex": -1,
					"victimIndex": -1,
					"robberSpot": {
						"x": "",
						"y": ""
					}
				}</pre>
		**/
		robPlayer: function(playerIndex, victimIndex, x, y) {
			var command = catan.commands.RobPlayer(playerIndex, victimIndex, x, y);
			communicator.post('/moves/robPlayer', command);
		},
		
		/**
			<pre>
				Discards selected cards because of a 7 rolled.
				
				Pre-condition:
					The User must be logged into the client and have joined a game
				Post-condition:
					Either the success or failure callback will be executed asychronously
					The success callback will be passed the server's response
			</pre>
			
			@method discardCards
			@param {object} discardJSON <pre>Example:
				{
					"type": "discardCards",
					"playerIndex": -1,
					"discardedCards": {
						"brick": -1,
						"ore": -1,
						"sheep": -1,
						"wheat": -1,
						"wood": -1
					}
				}</pre>
		**/
		discardCards: function(playerIndex, brick, ore, sheep, wheat, wood) {
			var command = catan.commands.DiscardCards(playerIndex, brick, ore, sheep, wheat, wood);
			communicator.post('/moves/discardCards', command);
		}
	}
}
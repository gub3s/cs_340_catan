/**
	The Map module contains various Map-related components
	@namespace map
	@module catan.map
**/

var catan = catan || {};
catan.map = catan.map || {};

/**
The OrderID Class
<pre>
	Domain:
		id: Number
		
	Invariants:
		id is always initialized as -1
		id is never less than -1 or greater than 3
	
	Constructor Specifications
		OrderID().Pre-conditions:
			N/A
		OrderID().Post-conditions:
			var id === -1
</pre>
@class map.OrderID
@constructor
*/
catan.map.OrderID = function OrderID() {
	var id = -1;

	
	return {
	
		/**
			<pre>
			Returns the Owner Value of this object (-1 if unowned).
				Pre-condition:
					None
				Post-condition:
					result = Number representing owner ID.
			</pre>
			@method getID
			@return {Number} Value of the OwnerID.
		*/
		getOwnerID: function() {
			return id;
		},
		
		/**
			<pre>
			Changes the owner of this location.
				Pre-condition:
					playerID must be between 0 and 3, inclusive
				Post-condition:
					id = playerID
			</pre>
			@method setID
			@param {Number} playerID The ID of the player that id should be set to
			@return {void}
		*/
		setOwnerID: function(playerID) {
			id = playerID;
			return;
		}
	}
};

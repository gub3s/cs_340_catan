/**
	The Game module contains various components of the gameplay logic
	@namespace game
	@module catan.game
**/

var catan = catan || {};
catan.game = catan.game || {};

/**
	<pre>
		The DevelopmentCard class represents a development
		card in the game.
	
		Domain:
			model: object
			canPlayCard: boolean
			
		Invariant:
			none

		Constructor:
			new catan.game.Developmentcard(jsonModel)
				jsonModel: The string defined in catan.definitions.CardTypes that represents
					this development card.

				Pre-condition:
					The JSON model is part of catan.definitions.CardTypes
				Post-condition:
					This DevelopmentCard will be filled out by the model
	</pre>
	
	@class game.DevelopmentCard
**/
catan.game.DevelopmentCard = function(jsonModel)
{
	var type = jsonModel;
	var playable = false;
	
	return {
		/**
			<pre>
				Whether the development card is playable

				Pre-condition:
					None
				Post-condition:
					No changes
			</pre>
	
			@method canPlayCard
			@return {boolean} Whether the development card is playable
		**/
		canPlayCard: function() {
			return playable;
		},
		
		/**
			<pre>
				Returns the type of this card

				Pre-condition:
					jsonModel used in constructor was vaild
				Post-condition:
					No changes
			</pre>
	
			@method getCardType
			@return {catan.definitions.CardTypes} Name of DevCard type
		**/
		getCardType: function() {
			return type;
		},
		
		/**
			<pre>
				Sets playability of development card
				
				Pre-condition:
					None
				Post-condition:
					The card will be playable or not playable depending
					on the parameter 
			</pre>
	
			@method makePlayable
			@param {boolean} playable <pre>The card's new playability
				true: Can now be played
				false: Can now not be played</pre>
		**/
		makePlayable: function(canPlay) {
			playable = canPlay;
		}
	}
}
//STUDENT-EDITABLE-BEGIN
/**
The namespace for the turn tracker

@module catan.turntracker
@namespace turntracker
**/

var catan = catan || {};
catan.turntracker = catan.turntracker || {};

catan.turntracker.Controller = (function turntracker_namespace() {	

	var Controller = catan.core.BaseController;

	var _localPlayerID;
    
	/**
		The controller class for the Turn Tracker
		@class TurnTrackerController 
		@extends misc.BaseController
		@param {turntracker.View} view The view for this object to control.
		@param {models.ClientModel} clientModel The clientModel for this object to control.
		@constructor
	**/
	var TurnTrackerController = (function TurnTrackerController_Class(){
	
		function TurnTrackerController(view, clientModel){
			Controller.call(this,view,clientModel);
		          
            // TODO: This constructor should configure its view by calling view.setClientColor and view.initializePlayer
            // NOTE: The view.updateViewState and view.updatePlayer will not work if called from here.  Instead, these
            //          methods should be called later each time the client model is updated from the server.
            var model = clientModel.model();
			_localPlayerID = clientModel.playerID();

			view.setClientColor(clientModel.playerModel().color);

			for (var i in model.players)
				view.initializePlayer(model.players[i].orderNumber, model.players[i].name, model.players[i].color);
		}

		core.forceClassInherit(TurnTrackerController,Controller);

		TurnTrackerController.prototype.updateModel = function(model)
		{
			var currentPlayerData;

			// Update player displays
		 	for (var i in model.players)
		 	{
		 		var player = model.players[i];
		 		this.View.updatePlayer({
					playerIndex: player.orderNumber,
					score: player.victoryPoints,
					highlight: player.orderNumber == model.turnTracker.currentTurn,
					army: player.orderNumber == model.biggestArmy,
					road: player.orderNumber == model.longestRoad
				});

				if (_localPlayerID == player.playerID)
					currentPlayerData = player;
			}

			if (model.turnTracker.currentTurn == currentPlayerData.orderNumber
					&& window.location.pathname.indexOf("setup") < 0)
				this.View.updateStateView(true, "End Turn");
			else
				this.View.updateStateView(false, "Waiting for other players");
		};

		/**
		 * Called by the view when the local player ends their turn.
		 * @method endTurn
		 * @return void
		 */
		TurnTrackerController.prototype.endTurn = function(){
			catan.models.ClientModel.playedDevCardThisTurn = false;
			catan.serverProxy.finishTurn(catan.models.ClientModel.orderNumber());
		}
		
		return TurnTrackerController;
	} ());

	return TurnTrackerController;
} ());


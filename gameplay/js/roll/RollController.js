//STUDENT-EDITABLE-BEGIN
/**
    This is the namespace the rolling interface
    @module catan.roll
    @namespace roll
*/

var catan = catan || {};
catan.roll = catan.roll || {};

catan.roll.Controller = (function roll_namespace(){

	var Controller = catan.core.BaseController;

	var _countdown;
	var _open = false;
	var _count = 5;
	var _self;
    
	/**
		 * @class RollController
		 * @constructor
		 * @extends misc.BaseController
		 * @param{roll.View} view
		 * @param{roll.ResultView} resultView
		 * @param{models.ClientModel} clientModel
		 */
	var RollController = (function RollController_Class(){
		
		core.forceClassInherit(RollController,Controller);
 
		core.defineProperty(RollController.prototype,"rollResultView");
		
		function RollController(view,resultView, clientModel){
			this.setRollResultView(resultView);
			Controller.call(this,view,clientModel);
			this.rollInterval = false;
			this.showRollResult = false;
		}


		RollController.prototype.updateModel = function(model)
		{
			_self = this;
			if (catan.models.ClientModel.isLocalsTurn() && model.turnTracker.status == "Rolling"
				&& !_open)
			{
				this.View.showModal();
				_open = true;
				if (_countdown)
					return;
				_countdown = setInterval(countdown, 1000);
				countdown();
			}
		};

		function countdown()
		{
			_self.View.changeMessage("Rolling Automatically in... " + _count--);
			if (_count < 0)
				_self.rollDice();
		}
        
		/**
		 * This is called from the roll result view.  It should close the roll result view and allow the game to continue.
		 * @method closeResult
		 * @return void
		**/
		RollController.prototype.closeResult = function(){
			this.View.closeModal();
			this.rollResultView.closeModal();
			_open = false;
		}
		
		/**
		 * This method generates a dice roll
		 * @method rollDice
		 * @return void
		**/
		RollController.prototype.rollDice = function(){
			var self = this;

			var result = catan.models.ClientModel.playerObj().roll();
			clearInterval(_countdown);
			_countdown = undefined;
			_count = 5;
			self.View.closeModal();
			self.rollResultView.setAmount(result);
			catan.serverProxy.rollNumber(catan.models.ClientModel.orderNumber(), result);
			self.rollResultView.showModal();
		};
		
		return RollController;
	}());
	
	return RollController;

}());


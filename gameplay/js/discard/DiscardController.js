//STUDENT-EDITABLE-BEGIN
/**
    This is the namespace for discarding cards
    @module catan.discard
    @namespace discard
*/

var catan = catan || {};
catan.discard = catan.discard || {};

catan.discard.Controller = (function discard_namespace(){

	var Controller = catan.core.BaseController;
    
	var Definitions = catan.definitions;
	var ResourceTypes = catan.definitions.ResourceTypes;
	
    /**
     * @class DiscardController
     * @constructor
     * @extends misc.BaseController
     * @param{discard.DiscardView} view
     * @param{misc.WaitOverlay} waitingView
     * @param{models.ClientModel} clientModel
     */
	var DiscardController = (function DiscardController_Class(){

		var _player;
		var _pmodel;

		var _tmp = {};
		var _countToDiscard = 0;
		var _total = 0;
		var _open = false;

		function DiscardController(view, waitingView, clientModel){
        
            Controller.call(this,view,clientModel);
			
            view.setController(this);
            
            waitingView.setController(this);
            this.setWaitingView(waitingView);
		}

		core.forceClassInherit(DiscardController,Controller);

		core.defineProperty(DiscardController.prototype,"waitingView");



		DiscardController.prototype.updateModel = function(model)
		{
			if (_open)
				return;

			_player = catan.models.ClientModel.playerObj();
			_pmodel = catan.models.ClientModel.playerModel();
			_countToDiscard = _player.mustDiscard();

			if (_countToDiscard && model.turnTracker.status == "Discarding")
			{
				for (var i in _pmodel.resources)
				{
					this.View.setResourceAmountChangeEnabled(i, true, true);
					this.View.setResourceAmount(i, 0);
					_tmp[i] = 0;
				}
				this.View.showModal();
				this.View.setStateMessage(_total + "/" + _countToDiscard);
				this.View.setDiscardButtonEnabled(_total == _countToDiscard);
				_open = true;
			}
		};





		/**
		 Called by the view when the player clicks the discard button.
         It should send the discard command and allow the game to continue.
		 @method discard
		 @return void
		 */	
		DiscardController.prototype.discard = function(){
			catan.serverProxy.discardCards(catan.models.ClientModel.orderNumber(),
					_tmp.brick, _tmp.ore, _tmp.sheep, _tmp.wheat, _tmp.wood);
			this.View.closeModal();
			_open = false;
			_total = 0;
			_countToDiscard = 0;
			_tmp = {};
		}
        
		/**
		 Called by the view when the player increases the amount to discard for a single resource.
		 @method increaseAmount
		 @param {String} resource the resource to discard
		 @return void
		 */
		DiscardController.prototype.increaseAmount = function(resource){
			if (_tmp[resource] >= _pmodel.resources[resource]
					|| _total >= _countToDiscard)
				return;
			this.View.setResourceAmount(resource, ++_tmp[resource]);
			_total++;
			this.View.setStateMessage(_total + "/" + _countToDiscard);
			this.View.setDiscardButtonEnabled(_total == _countToDiscard);
		}
        
		/**
		 Called by the view when the player decreases the amount to discard for a single resource.
		 @method decreaseAmount
		 @param {String} resource the resource to discard
		 @return void
		 */
		DiscardController.prototype.decreaseAmount = function(resource){
			if (_tmp[resource] <= 0)
				return;
			this.View.setResourceAmount(resource, --_tmp[resource]);
			_total--;
			this.View.setStateMessage(_total + "/" + _countToDiscard);
			this.View.setDiscardButtonEnabled(_total == _countToDiscard);
		}
		
		return DiscardController;
	}());
	
    return DiscardController;
}());


//STUDENT-EDITABLE-BEGIN
/**
    This is the namespace for the intitial game round
    @module catan.setup
    @namespace setup
*/

var catan = catan || {};
catan.setup = catan.setup || {};



catan.setup.states = {
	WAITING_FOR_FIRST_ROUND: 1,
	FIRST_ROUND: 2,
	WAITING_FOR_FIRST_SETTLEMENT: 3,
	NEEDS_FIRST_ROAD_MODAL: 4,
	WAITING_FOR_FIRST_ROAD: 5,
	WAITING_FOR_FINISH_TURN_1: 6,
	WAITING_FOR_SECOND_ROUND: 7,
	WAITING_FOR_SECOND_SETTLEMENT: 8,
	NEEDS_SECOND_ROAD_MODAL: 9,
	WAITING_FOR_SECOND_ROAD: 10,
	WAITING_FOR_FINISH_TURN_2: 11
};
catan.setup.state = 0;



catan.setup.Controller = (function(){
	
	var Controller = catan.core.BaseController;
    
	/** 
		@class SetupRoundController
		@constructor 
		@extends misc.BaseController
		@param {models.ClientModel} clientModel
		@param {map.MapController} mapController
	*/
	var SetupRoundController = (function (){
		
		var SetupRoundController = function (clientModel, mapController){
			this.mapController = mapController;
			Controller.call(this,undefined,clientModel);
		};
        
		core.forceClassInherit(SetupRoundController,Controller);
        
		return SetupRoundController;
	}());


	SetupRoundController.prototype.updateModel = function(model)
	{
		if (model.turnTracker.status != "FirstRound" && model.turnTracker.status != "SecondRound")
			return window.location = "catan.html";

		if (!catan.setup.state)
		{
			var me = catan.models.ClientModel.playerModel();
			if (catan.models.ClientModel.isLocalsTurn())
			{
				if (model.turnTracker.status == "FirstRound")
				{
					if (me.roads == 15 && me.settlements == 4)	// needs a road only for this round
						catan.setup.state = catan.setup.states.NEEDS_FIRST_ROAD_MODAL;
					else
						catan.setup.state = catan.setup.states.FIRST_ROUND;	// needs a settlement and a road for this round
				}
				else
				{
					if (me.roads == 14 && me.settlements == 4)	// Needs a settlement and a road
						catan.setup.state = catan.setup.states.WAITING_FOR_SECOND_ROUND;
					else if (me.roads == 14 && me.settlements == 3)	// Needs a road
						catan.setup.state = catan.setup.states.NEEDS_SECOND_ROAD_MODAL;
					else
						catan.setup.state = catan.setup.states.WAITING_FOR_GAMEPLAY;
				}
			}
			else
			{
				if (model.turnTracker.status == "FirstRound")
					catan.setup.state = catan.setup.states.WAITING_FOR_SECOND_ROUND;
				else
					catan.setup.state = catan.setup.states.WAITING_FOR_GAMEPLAY;
			}
		}

		switch(catan.setup.state)
		{
			case catan.setup.states.FIRST_ROUND:
				this.mapController.startMove("Settlement", true, true);
				catan.setup.state = catan.setup.states.WAITING_FOR_FIRST_SETTLEMENT;
				break;
			case catan.setup.states.NEEDS_FIRST_ROAD_MODAL:
				this.mapController.startMove("Road", true, true);
				catan.setup.state = catan.setup.states.WAITING_FOR_FIRST_ROAD;
				break;
			case catan.setup.states.WAITING_FOR_FINISH_TURN_1:
				catan.serverProxy.finishTurn(catan.models.ClientModel.orderNumber());
				catan.setup.state = catan.setup.states.WAITING_FOR_SECOND_ROUND;
				break;
			case catan.setup.states.WAITING_FOR_SECOND_ROUND:
				if (!catan.models.ClientModel.isLocalsTurn())
					break;
				this.mapController.startMove("Settlement", true, true);
				catan.setup.state = catan.setup.states.WAITING_FOR_SECOND_SETTLEMENT;
				break;
			case catan.setup.states.NEEDS_SECOND_ROAD_MODAL:
				this.mapController.startMove("Road", true, true);
				catan.setup.state = catan.setup.states.WAITING_FOR_FIRST_ROAD;
				break;
			case catan.setup.states.WAITING_FOR_FINISH_TURN_2:
				catan.serverProxy.finishTurn(catan.models.ClientModel.orderNumber());
				catan.setup.state = catan.setup.states.WAITING_FOR_GAMEPLAY;
				break;
		}
	};

	return SetupRoundController;
}());


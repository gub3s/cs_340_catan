/**
	This this contains interfaces used by the map and robber views
	@module catan.map
	@namespace map
*/

var catan = catan || {};
catan.map = catan.map || {};

catan.map.Controller = (function catan_controller_namespace() {
	
    var EdgeLoc = catan.map.View.EdgeLoc;
	var VertexLoc = catan.map.View.VertexLoc;
	var PortLoc = catan.map.View.PortLoc;
    
	var HexLocation = catan.models.hexgrid.HexLocation;
	var VertexLocation = catan.models.hexgrid.VertexLocation;
	var EdgeLocation= catan.models.hexgrid.EdgeLocation;
	var VertexDirection = catan.models.hexgrid.VertexDirection;
	var EdgeDirection= catan.models.hexgrid.EdgeDirection;   

	var MapController = (function main_controller_class() {
    
 		core.forceClassInherit(MapController,catan.core.BaseController);
        
		core.defineProperty(MapController.prototype,"robView");
		core.defineProperty(MapController.prototype,"modalView");

		var _road1;
        
        /**
		 * @class MapController
		 * @constructor
		 * @param {MapView} view - The initialized map view
		 * @param {MapOverlay} modalView - The overlay to use for placing items on the board.
		 * @param {ClientModel} model - The client model
		 * @param {RobberOverlay} robView - The robber overlay to be used when the robber is being placed.  This is undefined for the setup round.
		 */
		function MapController(view, modalView, model, robView){
			catan.core.BaseController.call(this,view,model);
			this.setModalView(modalView);
			this.setRobView(robView);
		}
        
		MapController.prototype.updateModel = function(model)
		{
			for (var i in model.map.hexGrid.hexes)
			{
				var hexRow = model.map.hexGrid.hexes[i];
				for (var j in hexRow)
				{
					var hex = hexRow[j];
					var type = hex.landtype ? hex.landtype.toLowerCase() : "desert";
					if (!hex.isLand)
						type = "water";
					this.View.addHex(hex.location, type, true);
				}
			}

			// Add numbers
			for (var n in model.map.numbers)
			{
				var nums = model.map.numbers[n];
				for (var i in nums)
				{
					this.View.addNumber(nums[i], n, true);
				}
			}

			// Add ports
			for (var i in model.map.ports)
			{
				var port = model.map.ports[i];
				var type = port.inputResource || "three";
				port.location.getRotation = function()
				{
					if (port.orientation == "N")
						return 1;
					else if (port.orientation == "NE")
						return 2;
					else if (port.orientation == "SE")
						return 3;
					else if (port.orientation == "S")
						return 4;
					else if (port.orientation == "SW")
						return 5;
					else if (port.orientation == "NW")
						return 6;
				};
				this.View.addPort(port.location, type.toLowerCase(), true);
			}
			
			// Add robber
			var robberLoc = model.map.robber
			this.View.placeRobber(robberLoc,false);

			
			// Robber functionality
			if (catan.models.ClientModel.isLocalsTurn()
				&& model.turnTracker.status == "Robbing")
			{
				this.getModalView().showModal("Robber");
				this.View.startDrop("robber", catan.models.ClientModel.playerModel().color);
			}
			
			
			var hexes = model.map.hexGrid.hexes;
			for (var i = 0; i < hexes.length; i++) {
				for (var j = 0; j < hexes[i].length; j++) {
					var hex = hexes[i][j];

					// Add roads
					if (hex.edges[3].value.ownerID !== -1) {
						var edgeLoc = new EdgeLoc(hex.location.x,hex.location.y,"SE");
						this.View.placeRoad(edgeLoc,model.players[hex.edges[3].value.ownerID].color,false);					
					}
					if (hex.edges[4].value.ownerID !== -1) {
						var edgeLoc = new EdgeLoc(hex.location.x,hex.location.y,"S");
						this.View.placeRoad(edgeLoc,model.players[hex.edges[4].value.ownerID].color,false);					
					}
					if (hex.edges[5].value.ownerID !== -1) {
						var edgeLoc = new EdgeLoc(hex.location.x,hex.location.y,"SW");
						this.View.placeRoad(edgeLoc,model.players[hex.edges[5].value.ownerID].color,false);					
					}

					// Add Settlements
					if (hex.vertexes[0].value.ownerID !== -1 && hex.vertexes[0].value.worth == 1) {
						var vertexLoc = new VertexLoc(hex.location.x,hex.location.y,"W");
						this.View.placeSettlement(vertexLoc,model.players[hex.vertexes[0].value.ownerID].color,false);					
					}
					if (hex.vertexes[3].value.ownerID !== -1 && hex.vertexes[3].value.worth == 1) {
						var vertexLoc = new VertexLoc(hex.location.x,hex.location.y,"E");
						this.View.placeSettlement(vertexLoc,model.players[hex.vertexes[3].value.ownerID].color,false);					
					}

					// Add Cities
					if (hex.vertexes[0].value.ownerID !== -1 && hex.vertexes[0].value.worth == 2) {
						var vertexLoc = new VertexLoc(hex.location.x,hex.location.y,"W");
						this.View.placeCity(vertexLoc,model.players[hex.vertexes[0].value.ownerID].color,false);					
					}
					if (hex.vertexes[3].value.ownerID !== -1 && hex.vertexes[3].value.worth == 2) {
						var vertexLoc = new VertexLoc(hex.location.x,hex.location.y,"E");
						this.View.placeCity(vertexLoc,model.players[hex.vertexes[3].value.ownerID].color,false);					
					}
				}
			}

			

		}

        /**
		 This method is called by the Rob View when a player to rob is selected via a button click.
		 @param {Integer} orderID The index (0-3) of the player who is to be robbed
		 @method robPlayer
		*/
		MapController.prototype.robPlayer = function(orderID) {
			var robber = catan.map2.getRobber();
			if (catan.models.ClientModel.usingSoldier)
			{
				catan.serverProxy.soldier(catan.models.ClientModel.orderNumber(), orderID, robber.getX(), robber.getY());
				catan.models.ClientModel.usingSoldier = false;
			}
			else
				catan.serverProxy.robPlayer(catan.models.ClientModel.orderNumber(), orderID, robber.getX(), robber.getY());
			this.getRobView().closeModal();
		}
        
        /**
		 * Starts the robber movement on the map. The map should pop out and the player should be able
         * move the robber.  This is called when the user plays a "solider" development card.
		 * @method doSoldierAction
		 * @return void
		**/
		MapController.prototype.doSoldierAction = function(){   
			this.getModalView().showModal("Soldier");
			this.View.startDrop("robber");
		}
        
		/**
		 * Pops the map out and prompts the player to place two roads.
         * This is called when the user plays a "road building" progress development card.
		 * @method startDoubleRoadBuilding
		 * @return void
		**/	
		MapController.prototype.startDoubleRoadBuilding = function(){
			this.startMove("road", true, false);
		}
		
        
        /**
		 * Pops the map out and prompts the player to place the appropriate piece
         * @param {String} pieceType - "road", "settlement", or "city
         * @param {boolean} free - Set to true in road building and the initial setup
         * @param {boolean} disconnected - Whether or not the piece can be disconnected. Set to true only in initial setup
		 * @method startMove
		 * @return void
		**/	
		MapController.prototype.startMove = function (pieceType,free,disconnected){
			this.modalView.showModal(pieceType);
			this.View.startDrop(pieceType, catan.models.ClientModel.playerModel().color);
		};
        
		/**
		 * This method is called from the modal view when the cancel button is pressed. 
		 * It should allow the user to continue gameplay without having to place a piece. 
		 * @method cancelMove
		 * @return void
		 * */
		MapController.prototype.cancelMove = function(){
			if (catan.models.ClientModel.usingSoldier)
			{
				catan.models.ClientModel.playedDevCardThisTurn = false;
				catan.models.ClientModel.usingSoldier = false;
			}
			this.View.cancelDrop();
		}

		/**
		 This method is called whenever the user is trying to place a piece on the map. 
         It is called by the view for each "mouse move" event.  
         The returned value tells the view whether or not to allow the piece to be "dropped" at the current location.

		 @param {MapLocation} loc The location being considered for piece placement
		 @param {String} type The type of piece the player is trying to place ("robber","road","settlement","city")
		 @method onDrag
		 @return {boolean} Whether or not the given piece can be placed at the current location.
		*/
		MapController.prototype.onDrag = function (loc, type) {
			if (type.type == "road")
				return catan.map2.canPlaceRoad(catan.map.VertexLocation(loc.x, loc.y, loc.dir), location.pathname.indexOf("setup") > -1);
			if (type.type == "settlement")
				return catan.map2.canPlaceSettlement(catan.map.VertexLocation(loc.x, loc.y, loc.dir), location.pathname.indexOf("setup") > -1);
			if (type.type == "city")
				return catan.map2.canPlaceCity(catan.map.VertexLocation(loc.x, loc.y, loc.dir));
			if (type.type == "robber")
				return catan.map2.canPlaceRobber(loc);
			else
				return true;
		};

		/**
		 This method is called when the user clicks the mouse to place a piece.
         This method should close the modal and possibly trigger the Rob View.

		 @param {MapLocation} loc The location where the piece is being placed
		 @param {String} type The type of piece being placed ("robber","road","settlement","city")
		 @method onDrop
		*/
		MapController.prototype.onDrop = function (loc, type) {
			if (catan.setup)	// if we're in setup.html...
			{
				switch(catan.setup.state)
				{
					case catan.setup.states.WAITING_FOR_FIRST_SETTLEMENT:
						catan.serverProxy.buildSettlement(catan.models.ClientModel.orderNumber(), loc.x, loc.y, loc.dir, true);
						catan.setup.state = catan.setup.states.NEEDS_FIRST_ROAD_MODAL;
						break;
					case catan.setup.states.WAITING_FOR_FIRST_ROAD:
						catan.serverProxy.buildRoad(catan.models.ClientModel.orderNumber(), loc.x, loc.y, loc.dir, true);
						catan.setup.state = catan.setup.states.WAITING_FOR_FINISH_TURN_1;
						break;
					case catan.setup.states.WAITING_FOR_SECOND_SETTLEMENT:
						catan.serverProxy.buildSettlement(catan.models.ClientModel.orderNumber(), loc.x, loc.y, loc.dir, true);
						catan.setup.state = catan.setup.states.WAITING_FOR_SECOND_ROAD_MODAL;
						break;
					case catan.setup.states.WAITING_FOR_SECOND_ROAD:
						catan.serverProxy.buildRoad(catan.models.ClientModel.orderNumber(), loc.x, loc.y, loc.dir, true);
						catan.setup.state = catan.setup.states.WAITING_FOR_GAMEPLAY;
						break;
				}
			}
			else		// in catan.html...
			{
				switch (type.type)
				{
					case "road":
						if (catan.models.ClientModel.freeRoads > 0)
						{
							catan.models.ClientModel.freeRoads--;
							if (!_road1)
								_road1 = loc;
							var self = this;
							setTimeout(function() {
								self.modalView.showModal("road");
								self.View.startDrop("road", catan.models.ClientModel.playerModel().color);
							}, 500);
						}
						else
							catan.serverProxy.buildRoad(catan.models.ClientModel.orderNumber(), loc.x, loc.y, loc.dir, catan.models.ClientModel.freeRoads > 0);
						if (catan.models.ClientModel.freeRoads == 0)
						{
							catan.serverProxy.roadBuilding(catan.models.ClientModel.orderNumber(), _road1.x, _road1.y, _road1.dir, loc.x, loc.y, loc.dir);
							_road1 = undefined;
						}
						break;
					case "settlement":
						catan.serverProxy.buildSettlement(catan.models.ClientModel.orderNumber(), loc.x, loc.y, loc.dir, false);
						break;
					case "city":
						catan.serverProxy.buildCity(catan.models.ClientModel.orderNumber(), loc.x, loc.y, loc.dir);
						break;
					case "robber":
						catan.map2.placeRobber(loc);
						players = catan.map2.getRobbablePlayers(loc);
						robbable = [];
						for (var i = 0; i < players.length; i++)
						{
							var player = catan.models.ClientModel.model().players[players[i]];
							var resCardCount = 0;
							for (var res in player.resources)
								resCardCount += player.resources[res];

							if (players[i] != catan.models.ClientModel.orderNumber()
									&& resCardCount > 0)	
								robbable.push({
									color: player.color,
									name: player.name,
									playerNum: players[i],
									cards: resCardCount
								});
						}
						this.getRobView().setPlayerInfo(robbable);
						this.getRobView().showModal();
	
						break;
				}
			}

			this.modalView.closeModal();
		};
        
		return MapController;
	} ());

	return MapController;

} ());


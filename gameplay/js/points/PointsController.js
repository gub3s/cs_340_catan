//STUDENT-EDITABLE-BEGIN
var catan = catan || {};
catan.points = catan.points || {};
catan.points.Controller = catan.points.Controller || {};

/**
    This is the namespace for point display
    @module catan.points
    @namespace points
*/

catan.points.Controller = (function VPController_Class(){

	var Controller = catan.core.BaseController;

	PointController.prototype = core.inherit(Controller.prototype);

	core.defineProperty(PointController.prototype, "GameFinishedView");

	/** 
		@class PointController
		@constructor 
		@extends misc.BaseController
		@param {points.View} view
		@param {misc.GameFinishedView} gameFinishedView
		@param {models.ClientModel} clientModel
	*/
	function PointController(view, gameFinishedView, clientModel){
		this.setGameFinishedView(gameFinishedView);
		Controller.call(this,view,clientModel);
	}


	PointController.prototype.updateModel = function(model)
	{
		var thisPlayer = catan.models.ClientModel.orderNumber();
		this.View.setPoints(model.players[thisPlayer].victoryPoints);
		
		if (model.winner > -1 && location.pathname.indexOf("setup") == -1)
		{
			for (var count = 0; count < 4; count++)
				if (model.players[count].playerID == model.winner)
					this.GameFinishedView.setWinner(model.players[count].name, model.winner == catan.models.ClientModel.playerID());
			this.GameFinishedView.showModal();
		}
	};
	
	return PointController;	
}());
// STUDENT-REMOVE-END


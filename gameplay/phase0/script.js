var games = [], players = {}, commands = [];

$(function()
{
	loadJson();
	setupImpersonator();
	setupApiTester();
});

function loadJson()
{
	$.getJSON('/games/list', function(data)
	{
		games = data;
		for (var i in data)
		{
			var game = data[i];
			players[game.id] = game.players;
			$('#games').append('<option value="'+game.id+'">'+game.title+'</option>');
		}
	});

	$.getJSON('/phase0/server-REST.json', function(data)
	{
		commands = data;
		for (var i in commands)
		{
			var cmd = commands[i];
			$('#commands').append('<option value="'+i+'">'+cmd.method+' '+cmd.url+'</option>');
		}
	});
}


function setupImpersonator()
{
	$('#games').change(function(e)
	{
		$('#players').val('').change().hide();
		$('#impersonate-output').empty().hide();
		var gameid = $(this).val();
		if (gameid == "")
			return;
		$('#players .dynamic').remove();
		$('#impersonate-output').empty().hide();
		for (var i in players[gameid])
		{
			var player = players[gameid][i];
			$('#players').append('<option value="'+player.name+'" class="dynamic">'+player.name+'</option>');
		}
		$('#players').show().focus();
	});

	$('#players').change(function(e)
	{
		var player = $(this).val();
		if (player == "")
		{
			$('#pwd, #submit1').hide();
			return;
		}
		$('#impersonate-output').empty().hide();
		$('#pwd, #submit1').show();
		$('#pwd').focus();
	});

	$('#impersonate').submit(function(e)
	{
		var data = $(this).serialize();

		$.post('/user/login', data).done(function(resp, status, jqxhr)
		{
			$('#impersonate-output').text(jqxhr.responseText).show();
			$.post('/games/join', {
				id: $('#games').val(),
				color: "red"
			}).done(function(resp, status, jqxhr)
			{
				$('#impersonate-output').text(jqxhr.responseText).show();
			}).fail(function(jqxhr, status, msg)
			{
				$('#impersonate-output').text(jqxhr.responseText).show();
			});
		}).fail(function(jqxhr, status, msg)
		{
			$('#impersonate-output').text(jqxhr.responseText).show();
		});

		e.preventDefault();
		return false;
	});
}


function setupApiTester()
{
	$('#commands').change(function(e)
	{
		$('#args').empty();
		var idx = $(this).val();
		if (idx == "")
		{
			$('#submit2, #tester-output').hide();
			$('#description').empty();
			return;
		}

		var cmd = commands[idx];

		$('#description').text(cmd.description);
		$('#submit2').show();

		if (cmd.args.length > 0)
		{
			for (var i in cmd.args)
			{
				var arg = cmd.args[i];
				var html = '<label><span class="fieldname">'+arg.name+'</span>: &nbsp; ';
				if (arg.type == "BOOLEAN")
					html += '<input type="checkbox" name="'+arg.name+'" value="true"></label><br>';
				else if (arg.type == "ENUMERATION")
				{
					var select =
						'<select size="1" name="'+arg.name+'">' +
						'<option value=""></option>';
					for (var j in arg.values)
						select += '<option value="'+arg.values[j]+'">'+arg.values[j]+'</option>';
					select += '</select></label><br>';
					html += select;
				}
				else
					html += '<input type="text" name="'+arg.name+'" size="50" placeholder="'+arg.description+'"></label><br>';
				$('#args').append(html);
			}
		}
		else if (cmd.type == "JSON")
		{
			$('#args').append('<textarea id="json" rows="10" cols="50"></textarea>');
			$('#json').val(JSON.stringify(cmd.template, undefined, 2));
		}
	});

	$('#apitester').submit(function(e)
	{
		var cmd = commands[$('#commands').val()];
		$(this).attr('method', cmd.method);

		if (cmd.type == "JSON")
		{
			$.ajax(cmd.url, {
				data: $('#json').val(),
				timeout: 2000,
				type: cmd.method,
				dataType: "json",
				contentType: "application/json; charset=utf-8"
			}).done(ajaxDone).fail(ajaxFail);
		}
		else
		{
			$.ajax(cmd.url, {
				data: $(this).serialize(),
				timeout: 2000,
				type: cmd.method
			}).done(ajaxDone).fail(ajaxFail);
		}

		e.preventDefault();
		return false;
	});
}

function ajaxDone(resp, status, jqxhr)
{
	$('#tester-output')
		.text(status+"\n--------\n"+jqxhr.responseText)
		.show();
}

function ajaxFail(jqxhr, status, msg)
{
	$('#tester-output')
		.text(status+" - "+msg+"\n---------------------\n"+jqxhr.responseText)
		.show();
}
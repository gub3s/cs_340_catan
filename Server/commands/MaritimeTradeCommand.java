package commands;

public abstract class MaritimeTradeCommand implements ICommand {
	public int gameID;
	public int playerIndex;
	public int ratio;
	public String inputResource;
	public String outputResource;
}
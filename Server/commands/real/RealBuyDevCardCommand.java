package commands.real;

import commands.BuyDevCardCommand;
import game.GameManager;
import java.util.Random;
import models.Game;
import models.Player;

public class RealBuyDevCardCommand extends BuyDevCardCommand {
	@Override
	public String execute() throws Exception {
		// buy dev card logic
		Random random = new Random();
		
		Game game = GameManager.getGame(gameID);
		Player player = game.getPlayer(playerIndex);
		int rand = random.nextInt(5);
		
		// remove a sheep, wheat, and ore from player resouce
		player.getResources().decrementSheep();
		player.getResources().decrementWheat();
		player.getResources().decrementOre();
		
		switch (rand) {
			case 0:
				// year of plenty
				game.getDeck().drawYearOfPlenty();
				player.getNewDevCards().incrementYearOfPlenty();
				break;
			case 1:
				// monopoly
				game.getDeck().drawMonopoly();
				player.getNewDevCards().incrementMonopoly();
				break;
			case 2:
				// soldier
				game.getDeck().drawSoldier();
				player.getNewDevCards().incrementSoldier();
				break;
			case 3:
				// road building
				game.getDeck().drawRoadBuilding();
				player.getNewDevCards().incrementRoadBuilding();
				break;
			case 4:
				// monument
				game.getDeck().drawMonument();
				player.getOldDevCards().incrementMonument();
				break;
			default:
				System.out.println("Random number generated does not map to dev card");
		}
		game.incrementRevision();
		GameManager.getPersistence().updateGame(gameID, game);
		return "IMPLEMENTED!!!";		
	}
}
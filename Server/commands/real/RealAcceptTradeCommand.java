package commands.real;

import commands.AcceptTradeCommand;

import game.GameManager;
import models.Game;
import models.Player;
import models.Resources;

public class RealAcceptTradeCommand extends AcceptTradeCommand {
	@Override
	public String execute() throws Exception {
		Game game = GameManager.getGame(this.gameID);
		Resources offer = game.getTradeOffer().getOffer();

		if (this.willAccept)
		{
			Player receiver = game.getPlayer(game.getTradeOffer().getReceiver());
			receiver.getResources().addBrick(offer.getBrick());
			receiver.getResources().addWood(offer.getWood());
			receiver.getResources().addSheep(offer.getSheep());
			receiver.getResources().addWheat(offer.getWheat());
			receiver.getResources().addOre(offer.getOre());

			Player sender = game.getPlayer(game.getTradeOffer().getSender());
			sender.getResources().subtractBrick(offer.getBrick());
			sender.getResources().subtractWood(offer.getWood());
			sender.getResources().subtractSheep(offer.getSheep());
			sender.getResources().subtractWheat(offer.getWheat());
			sender.getResources().subtractOre(offer.getOre());
			
			game.getLog().addLine(game.getPlayer(playerIndex).getName(), " accepted the trade");
		}
		else
			game.getLog().addLine(game.getPlayer(playerIndex).getName(), " declined the trade");

		game.setTradeOffer(null);
		game.incrementRevision();
		GameManager.getPersistence().updateGame(gameID, game);
		return "IMPLEMENTED!!!";
	}
}
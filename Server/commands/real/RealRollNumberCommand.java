package commands.real;

import commands.RollNumberCommand;
import game.GameManager;
import java.util.ArrayList;
import models.Coordinate;
import models.Game;
import models.Hex;
import models.Player;
import models.Vertex;

public class RealRollNumberCommand extends RollNumberCommand {
	/*
		Variables from super class
		public int gameID
		public int playerIndex
		public int number
	*/
	public RealRollNumberCommand() {}
	
	@Override
	public String execute() throws Exception {
		Game game = GameManager.getGame(gameID);
		Player currentPlayer = game.getPlayer(playerIndex);
		// If number is seven, rob player
		if (number == 7) {
			// Check if any player has more than seven cards
			if (game.playersMustDiscard()) {
				System.out.println("Status = discarding");
				game.getTurnTracker().setStatus(game.getTurnTracker().DISCARDING);
			}
		}
		else {
			// Get the list of hexes that have the same chit as number rolled
			ArrayList<Coordinate> hexLocs = game.getMap().getNumbers().get(number);
			for (Coordinate hexLoc : hexLocs) {
				Coordinate hexIndex = game.getMap().getHexGrid().convertCoordinateToIndex(hexLoc);
				Hex hex = game.getMap().getHexGrid().getHex(hexIndex);
				String landtype = hex.getLandtype().toLowerCase();
//				System.out.println("landtype is " + landtype);
				for (Vertex vertex : hex.getVertexes()) {
					// Give the player that owns the vertex the resource x worth
					// Don't duplicate for the same settlement/city
					int playerID = vertex.getValue().getOwnerID();
					if (playerID > -1) {
						int worth = vertex.getValue().getWorth();
						Player player = game.getPlayer(playerID);
						addResources(player, worth, landtype);
					}
				}
			}
		}
		game.getLog().addLine(currentPlayer.getName(), " rolled a " + number);
		game.getTurnTracker().setStatus(game.getTurnTracker().PLAYING);
		game.incrementRevision();
		GameManager.getPersistence().updateGame(gameID, game);
		return "IMPLEMENTED!!!";		
	}
	
	private void addResources(Player player, int worth, String landtype) {
		switch(landtype) {
			case "ore":
				player.getResources().addOre(worth);
				break;
			case "brick":
				player.getResources().addBrick(worth);
				break;
			case "sheep":
				player.getResources().addSheep(worth);
				break;
			case "wood":
				player.getResources().addWood(worth);
				break;
			case "wheat":
				player.getResources().addWheat(worth);
				break;
		}
	}
}
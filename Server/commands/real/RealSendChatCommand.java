package commands.real;

import commands.SendChatCommand;
import game.GameManager;
import models.Game;

public class RealSendChatCommand extends SendChatCommand {
	@Override
	public String execute() throws Exception {
		// send chat logic
		Game game = GameManager.getGame(gameID);
		String playerName = game.getPlayers().get(playerIndex).getName();
		game.appendChat(playerName, content);
		game.incrementRevision();
		GameManager.getPersistence().updateGame(gameID, game);
		return "IMPLEMENTED!!!";		
	}
}
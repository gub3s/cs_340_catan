package commands.real;

import commands.BuildRoadCommand;
import game.GameManager;
import models.Coordinate;
import models.Game;
import models.HexGrid;

public class RealBuildRoadCommand extends BuildRoadCommand {
	
	/*
		Inherited from super class
		public int gameID;
		public int playerIndex;
		public EdgeLocation roadLocation;
		public boolean free;
	*/
	
	@Override
	public String execute() throws Exception {
		Game game = GameManager.getGame(gameID);
		
		HexGrid hexGrid = game.getMap().getHexGrid();
		Coordinate arrayIndex = hexGrid.convertCoordinateToIndex(roadLocation);
		int edgeIndex;
		
		switch(roadLocation.getDirection()) {
			case "NW":
				edgeIndex = 0;
				// update at x,y
				hexGrid.getHex(arrayIndex).getEdge(edgeIndex).getValue().setOwnerID(playerIndex);
				// update at x-1,y
				Coordinate northwestIndex = hexGrid.convertCoordinateToIndex(roadLocation.getX() - 1, roadLocation.getY());
				hexGrid.getHex(northwestIndex).getEdge(3).getValue().setOwnerID(playerIndex);
				break;
			case "N":
				edgeIndex = 1;
				// update at x,y
				hexGrid.getHex(arrayIndex).getEdge(edgeIndex).getValue().setOwnerID(playerIndex);
				// update at x,y-1
				Coordinate northIndex = hexGrid.convertCoordinateToIndex(roadLocation.getX(), roadLocation.getY() - 1);
				hexGrid.getHex(northIndex).getEdge(4).getValue().setOwnerID(playerIndex);
				break;
			case "NE":
				edgeIndex = 2;
				// update at x,y
				hexGrid.getHex(arrayIndex).getEdge(edgeIndex).getValue().setOwnerID(playerIndex);
				// update at x+1,y-1
				Coordinate northeastIndex = hexGrid.convertCoordinateToIndex(roadLocation.getX() + 1, roadLocation.getY() - 1);
				hexGrid.getHex(northeastIndex).getEdge(5).getValue().setOwnerID(playerIndex);
				break;
			case "SE":
				edgeIndex = 3;
				// update at x,y
				hexGrid.getHex(arrayIndex).getEdge(edgeIndex).getValue().setOwnerID(playerIndex);
				// update at x+1,y
				Coordinate southeastIndex = hexGrid.convertCoordinateToIndex(roadLocation.getX() + 1, roadLocation.getY());
				hexGrid.getHex(southeastIndex).getEdge(0).getValue().setOwnerID(playerIndex);
				break;
			case "S":
				edgeIndex = 4;
				// update at x,y
				hexGrid.getHex(arrayIndex).getEdge(edgeIndex).getValue().setOwnerID(playerIndex);
				// update at x,y+1
				Coordinate southIndex = hexGrid.convertCoordinateToIndex(roadLocation.getX(), roadLocation.getY() + 1);
				hexGrid.getHex(southIndex).getEdge(1).getValue().setOwnerID(playerIndex);
				break;
			case "SW":
				edgeIndex = 5;
				// update at x,y
				hexGrid.getHex(arrayIndex).getEdge(edgeIndex).getValue().setOwnerID(playerIndex);
				// update at x-1,y+1
				Coordinate southwestIndex = hexGrid.convertCoordinateToIndex(roadLocation.getX() - 1, roadLocation.getY() + 1);
				hexGrid.getHex(southwestIndex).getEdge(2).getValue().setOwnerID(playerIndex);
				break;
			default:
				System.out.println("Look what ya did ya big jerk!");
		}
		
		if (!free) {
			game.getPlayer(playerIndex).getResources().decrementWood();
			game.getPlayer(playerIndex).getResources().decrementBrick();
		}
		game.getPlayer(playerIndex).decrementRoads();
		
		String name = game.getPlayer(playerIndex).getName();
		game.getLog().addLine(name, " built a road");
		game.incrementRevision();
		GameManager.getPersistence().updateGame(gameID, game);
		
		// Longest road calculations
		for (int i = 0; i < 4; i++) {
			if (game.getLongestRoad() == -1 && game.getPlayer(i).getRoads() <= 10) {
				game.setLongestRoad(i);
				game.getPlayer(i).setLongestRoad(true);
				game.getPlayer(i).incrementVictoryPoints();
				game.getPlayer(i).incrementVictoryPoints();
				name = game.getPlayer(i).getName();
				game.getLog().addLine(name, " took the longest road");
			}
			else if (game.getPlayer(i).getRoads() <= 10){
				if (game.getPlayer(i).getRoads() > game.getPlayer(game.getLongestRoad()).getRoads()) {
					game.getPlayer(game.getLongestRoad()).setLongestRoad(false);
					game.getPlayer(game.getLongestRoad()).decrementVictoryPoints();
					game.getPlayer(game.getLongestRoad()).decrementVictoryPoints();
					game.getPlayer(i).setLongestRoad(true);
					game.getPlayer(i).incrementVictoryPoints();
					game.getPlayer(i).incrementVictoryPoints();
					game.setLongestRoad(i);
					name = game.getPlayer(game.getLongestRoad()).getName();
					game.getLog().addLine(name, " took the longest road");
				}
			}
		}
		
		return "IMPLEMENTED!!!";		
	}
}
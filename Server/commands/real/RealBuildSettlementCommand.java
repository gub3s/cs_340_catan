package commands.real;

import commands.BuildSettlementCommand;
import game.GameManager;
import models.Coordinate;
import models.Game;
import models.HexGrid;

public class RealBuildSettlementCommand extends BuildSettlementCommand {
	
	/*
		Inherited from super class
		public int gameID;
		public int playerIndex;
		public VertexLocation vertexLocation;
		public boolean free;
	*/
	
	@Override
	public String execute() throws Exception {
		Game game = GameManager.getGame(gameID);
		
//		System.out.println("Real build settlement");
		
		HexGrid hexGrid = game.getMap().getHexGrid();
		Coordinate arrayIndex = hexGrid.convertCoordinateToIndex(vertexLocation);
		if (vertexLocation.getDirection().equalsIgnoreCase("W")) {
			buildSettlementWest(hexGrid, arrayIndex);
		}
		else if (vertexLocation.getDirection().equalsIgnoreCase("E")) {
			buildSettlementEast(hexGrid, arrayIndex);
		}
		
		if (!free) {
			game.getPlayer(playerIndex).getResources().decrementBrick();
			game.getPlayer(playerIndex).getResources().decrementSheep();
			game.getPlayer(playerIndex).getResources().decrementWheat();
			game.getPlayer(playerIndex).getResources().decrementWood();
		}
		game.getPlayer(playerIndex).decrementSettlements();
		game.getPlayer(playerIndex).incrementVictoryPoints();
		
		String name = game.getPlayer(playerIndex).getName();
		game.getLog().addLine(name, " built a settlement");
		game.incrementRevision();
		GameManager.getPersistence().updateGame(gameID, game);
		
		return "IMPLEMENTED!!!";
	}
	
	private void buildSettlementWest(HexGrid hexGrid, Coordinate arrayIndex) {
		int vertexIndex = 0;
		hexGrid.getHex(arrayIndex).getVertex(vertexIndex).getValue().setWorth(1);
		hexGrid.getHex(arrayIndex).getVertex(vertexIndex).getValue().setOwnerID(playerIndex);
		
		// Update northwest hex (i-1, j)
		Coordinate northwestIndex = hexGrid.convertCoordinateToIndex(vertexLocation.getX() - 1, vertexLocation.getY());
		hexGrid.getHex(northwestIndex).getVertex(vertexIndex + 4).getValue().setWorth(1);
		hexGrid.getHex(northwestIndex).getVertex(vertexIndex + 4).getValue().setOwnerID(playerIndex);
		
		// Update southwest hex (i-1, j+1)
		Coordinate southwestIndex = hexGrid.convertCoordinateToIndex(vertexLocation.getX() - 1, vertexLocation.getY() + 1);
		hexGrid.getHex(southwestIndex).getVertex(vertexIndex + 1).getValue().setWorth(1);
		hexGrid.getHex(southwestIndex).getVertex(vertexIndex + 1).getValue().setOwnerID(playerIndex);
	}
	
	private void buildSettlementEast(HexGrid hexGrid, Coordinate arrayIndex) {
		int vertexIndex = 3;
		hexGrid.getHex(arrayIndex).getVertex(vertexIndex).getValue().setWorth(1);
		hexGrid.getHex(arrayIndex).getVertex(vertexIndex).getValue().setOwnerID(playerIndex);
		
		// Update northeast hex (i+1, j-1)
		Coordinate northeastIndex = hexGrid.convertCoordinateToIndex(vertexLocation.getX() + 1, vertexLocation.getY() - 1);
		hexGrid.getHex(northeastIndex).getVertex(vertexIndex + 2).getValue().setWorth(1);
		hexGrid.getHex(northeastIndex).getVertex(vertexIndex + 2).getValue().setOwnerID(playerIndex);
		
		// Update southwest hex (i+1, j)
		Coordinate southeastIndex = hexGrid.convertCoordinateToIndex(vertexLocation.getX() + 1, vertexLocation.getY());
		hexGrid.getHex(southeastIndex).getVertex(vertexIndex - 2).getValue().setWorth(1);
		hexGrid.getHex(southeastIndex).getVertex(vertexIndex - 2).getValue().setOwnerID(playerIndex);
	}
}
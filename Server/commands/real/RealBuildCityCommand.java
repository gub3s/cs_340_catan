package commands.real;

import commands.BuildCityCommand;
import game.GameManager;
import models.Coordinate;
import models.Game;
import models.HexGrid;

public class RealBuildCityCommand extends BuildCityCommand {
	/*
		Member variables from superclass
		public int gameID;
		public int playerIndex;
		public VertexLocation vertexLocation;
		public boolean free;
	*/
	
	@Override
	public String execute() throws Exception {
		// build city logic
		
		// get game from GameManager
		Game game = GameManager.getGame(gameID);
		// get the hexgrid
		HexGrid hexGrid = game.getMap().getHexGrid();
		Coordinate arrayIndex = hexGrid.convertCoordinateToIndex(vertexLocation);
		if (vertexLocation.getDirection().equalsIgnoreCase("west")) {
			buildCityWest(hexGrid, arrayIndex);
		}
		else if (vertexLocation.getDirection().equalsIgnoreCase("east")) {
			buildCityEast(hexGrid, arrayIndex);
		}
		
		if (!free) {
			game.getPlayer(playerIndex).getResources().decrementOre();
			game.getPlayer(playerIndex).getResources().decrementOre();
			game.getPlayer(playerIndex).getResources().decrementOre();
			game.getPlayer(playerIndex).getResources().decrementWheat();
			game.getPlayer(playerIndex).getResources().decrementWheat();
		}
		game.getPlayer(playerIndex).decrementCities();
		game.getPlayer(playerIndex).incrementSettlements();
		game.getPlayer(playerIndex).incrementVictoryPoints();
		
		String name = game.getPlayer(playerIndex).getName();
		game.getLog().addLine(name, " built a city");
		game.incrementRevision();
		GameManager.getPersistence().updateGame(gameID, game);
		
		return "IMPLEMENTED!!!";
	}
	
	private void buildCityWest(HexGrid hexGrid, Coordinate arrayIndex) {
		int vertexIndex = 0;
		hexGrid.getHex(arrayIndex).getVertex(vertexIndex).getValue().setWorth(2);
		
		// Update northwest hex (i-1, j)
		Coordinate northwestIndex = hexGrid.convertCoordinateToIndex(vertexLocation.getX() - 1, vertexLocation.getY());
		hexGrid.getHex(northwestIndex).getVertex(vertexIndex + 4).getValue().setWorth(2);
		
		// Update southwest hex (i-1, j+1)
		Coordinate southwestIndex = hexGrid.convertCoordinateToIndex(vertexLocation.getX() - 1, vertexLocation.getY() + 1);
		hexGrid.getHex(southwestIndex).getVertex(vertexIndex + 1).getValue().setWorth(2);
	}
	
	private void buildCityEast(HexGrid hexGrid, Coordinate arrayIndex) {
		int vertexIndex = 3;
		hexGrid.getHex(arrayIndex).getVertex(vertexIndex).getValue().setWorth(2);
		
		// Update northeast hex (i+1, j-1)
		Coordinate northeastIndex = hexGrid.convertCoordinateToIndex(vertexLocation.getX() + 1, vertexLocation.getY() - 1);
		hexGrid.getHex(northeastIndex).getVertex(vertexIndex + 2).getValue().setWorth(2);
		
		// Update southwest hex (i+1, j)
		Coordinate southeastIndex = hexGrid.convertCoordinateToIndex(vertexLocation.getX() + 1, vertexLocation.getY());
		hexGrid.getHex(southeastIndex).getVertex(vertexIndex - 2).getValue().setWorth(2);
	}
}
package commands.real;

import commands.DiscardCardsCommand;
import game.GameManager;
import models.Game;
import models.Player;

public class RealDiscardCardsCommand extends DiscardCardsCommand {
	/*
		public int gameID;
		public int playerIndex;
		public Resources discardedCards;
	*/
	
	@Override
	public String execute() throws Exception {
		System.out.println("Discard cards command");
		Game game = GameManager.getGame(gameID);
		Player player = game.getPlayer(playerIndex);
		// Remove discardedCards from player's resources
		player.getResources().removeBrick(discardedCards.getBrick());
		player.getResources().removeOre(discardedCards.getOre());
		player.getResources().removeSheep(discardedCards.getSheep());
		player.getResources().removeWheat(discardedCards.getWheat());
		player.getResources().removeWood(discardedCards.getWood());
		
		// If no other players have more than 7 cards
			// Change game status to robbing
		if (!game.playersMustDiscard()) {
			game.getTurnTracker().setStatus(game.getTurnTracker().ROBBING);
		}
		
		game.incrementRevision();
		GameManager.getPersistence().updateGame(gameID, game);
		return "NOT IMPLEMENTED!!!";		
	}
}
package commands.real;

import commands.MaritimeTradeCommand;
import game.GameManager;
import models.Game;
import models.Player;

public class RealMaritimeTradeCommand extends MaritimeTradeCommand {
	/*
		Inherited from super class
		public int gameID;
		public int playerIndex;
		public Resources ratio;
		public String inputResource;
		public String outputResource;
	*/
	@Override
	public String execute() throws Exception {
		Game game = GameManager.getGame(gameID);
		Player player = game.getPlayer(playerIndex);
		int amount;
		if (inputResource.equalsIgnoreCase("brick")) {
			amount = player.getResources().getBrick();
			player.getResources().setBrick(amount - ratio);
		}
		else if (inputResource.equalsIgnoreCase("wood")) {
			amount = player.getResources().getWood();
			player.getResources().setWood(amount - ratio);
		}
		else if (inputResource.equalsIgnoreCase("sheep")) {
			amount = player.getResources().getSheep();
			player.getResources().setSheep(amount - ratio);
		}
		else if (inputResource.equalsIgnoreCase("wheat")) {
			amount = player.getResources().getWheat();
			player.getResources().setWheat(amount - ratio);
		}
		else if (inputResource.equalsIgnoreCase("ore")) {
			amount = player.getResources().getOre();
			player.getResources().setOre(amount - ratio);
		}
		givePlayerResource(player);
		game.incrementRevision();
		GameManager.getPersistence().updateGame(gameID, game);
		return "IMPLEMENTED!!!";		
	}
	
	private void givePlayerResource(Player player) {
		int amount;
		if (outputResource.equalsIgnoreCase("brick")) {
			amount = player.getResources().getBrick();
			player.getResources().setBrick(amount + 1);
		}
		else if (outputResource.equalsIgnoreCase("wood")) {
			amount = player.getResources().getWood();
			player.getResources().setWood(amount + 1);
		}
		else if (outputResource.equalsIgnoreCase("sheep")) {
			amount = player.getResources().getSheep();
			player.getResources().setSheep(amount + 1);
		}
		else if (outputResource.equalsIgnoreCase("wheat")) {
			amount = player.getResources().getWheat();
			player.getResources().setWheat(amount + 1);
		}
		else if (outputResource.equalsIgnoreCase("ore")) {
			amount = player.getResources().getOre();
			player.getResources().setOre(amount + 1);
		}
	}
}
package commands.real;

import commands.OfferTradeCommand;
import commands.AcceptTradeCommand;
import commands.real.RealAcceptTradeCommand;
import game.GameManager;
import game.GameProxy;
import models.Game;
import models.Player;
import models.TradeOffer;

public class RealOfferTradeCommand extends OfferTradeCommand {
	@Override
	public String execute() throws Exception {
		Game game = GameManager.getGame(this.gameID);
		Player sender = game.getPlayer(this.playerIndex);
		Player rcv = game.getPlayer(this.receiver);

		game.getLog().addLine(sender.getName(), " offered a trade to " + rcv.getName());

		TradeOffer trade = new TradeOffer(this.playerIndex, this.receiver, this.offer);
		game.setTradeOffer(trade);

		if (rcv.isAI)
		{
			// AI accepts the trade if it can
			AcceptTradeCommand cmd = new RealAcceptTradeCommand();
			cmd.gameID = this.gameID;
			cmd.playerIndex = this.receiver;
			cmd.willAccept = game.getTradeOffer().satisfiedBy(rcv.getResources());
			System.out.println("AI accepting trade: " + cmd.willAccept);
			GameProxy.acceptTrade(cmd);
		}

		game.incrementRevision();
		GameManager.getPersistence().updateGame(gameID, game);
		return "IMPLEMENTED!!!";
	}
}
package commands.real;

import commands.FinishTurnCommand;
import commands.RollNumberCommand;
import commands.real.RealFinishTurnCommand;
import commands.real.RealRollNumberCommand;
import game.GameManager;
import game.GameProxy;
import models.DevCards;
import models.Game;
import models.Player;
import models.TurnTracker;
import java.util.Random;

public class RealFinishTurnCommand extends FinishTurnCommand {
	private TurnTracker turnTracker;
	private Game game;

	@Override
	public String execute() throws Exception {		
		game = GameManager.getGame(gameID);
		turnTracker = game.getTurnTracker();
		
		if (turnTracker.isFirstRound()) {
			if (turnTracker.getCurrentTurn() == 3)
			{
				turnTracker.setStatus(turnTracker.SECOND_ROUND);
				finishAITurn();
			}
			else
			{
				if (!game.getCurrentPlayer().isAI)
				{
					turnEnded(game.getCurrentPlayer().getName());
					turnTracker.incrementTurn();
				}

				while (game.getCurrentPlayer().isAI && turnTracker.getCurrentTurn() < 3)
				{
					turnEnded(game.getCurrentPlayer().getName());
					turnTracker.incrementTurn();
				}

				if (game.getCurrentPlayer().isAI && turnTracker.getCurrentTurn() == 3)
				{
					turnTracker.setStatus(turnTracker.SECOND_ROUND);
					finishAITurn();
				}
			}
		}
		else if (turnTracker.isSecondRound()) {
			if (turnTracker.getCurrentTurn() == 0)
			{
				turnTracker.setStatus(turnTracker.ROLLING);
				finishAITurn();
			}
			else
			{
				if (!game.getCurrentPlayer().isAI)
				{
					turnEnded(game.getCurrentPlayer().getName());
					turnTracker.decrementTurn();
				}

				while (game.getCurrentPlayer().isAI && turnTracker.getCurrentTurn() > 0)
				{
					turnEnded(game.getCurrentPlayer().getName());
					turnTracker.decrementTurn();
				}

				if (game.getCurrentPlayer().isAI && turnTracker.getCurrentTurn() == 0)
				{
					turnTracker.setStatus(turnTracker.ROLLING);
					finishAITurn();
				}
			}
		}
		else {
			// move newDevCards to oldDevCards
			Player player = game.getPlayer(playerIndex);
			DevCards oldDevCards = player.getOldDevCards();
			DevCards newDevCards = player.getNewDevCards();
			oldDevCards.addMonopoly(newDevCards.getMonopoly());
			oldDevCards.addMonument(newDevCards.getMonument());
			oldDevCards.addRoadBuilding(newDevCards.getRoadBuilding());
			oldDevCards.addSoldier(newDevCards.getSoldier());
			oldDevCards.addYearOfPlenty(newDevCards.getYearOfPlenty());
			player.emptyNewDevCards();

			turnEnded(player.getName());
			turnTracker.nextTurn();
	
			while (game.getCurrentPlayer().isAI)
			{
				// The current player is AI, so just roll,
				// which collects resources, then end turn.
				// TODO: Matt plans to use Google Guice here to create the command
				Random rand = new Random();
				RollNumberCommand cmd = new RealRollNumberCommand();
				cmd.number = rand.nextInt(12) + 1;
				cmd.playerIndex = game.getCurrentPlayer().getOrderNumber();
				cmd.gameID = gameID;
				GameProxy.rollNumber(cmd);

				turnEnded(game.getCurrentPlayer().getName());
				turnTracker.nextTurn();
			}
		}
		
		game.incrementRevision();
		GameManager.getPersistence().updateGame(gameID, game);
		return "IMPLEMENTED!!!";
	}

	private void finishAITurn()
	{
		if (game.getCurrentPlayer().isAI)
		{
			// TODO: Matt intends to use Google Guice here
			FinishTurnCommand cmd = new RealFinishTurnCommand();
			cmd.playerIndex = turnTracker.getCurrentTurn();
			cmd.gameID = gameID;
			GameProxy.finishTurn(cmd);
		}
	}

	private void turnEnded(String pName)
	{
		game.getLog().addLine(pName, "'s turn just ended");
	}
}
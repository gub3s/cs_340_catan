package commands;

import models.EdgeLocation;

public abstract class BuildRoadCommand implements ICommand {
	public int gameID;
	public int playerIndex;
	public EdgeLocation roadLocation;
	public boolean free;
}
package commands;

public abstract class PerformCommandsCommand implements ICommand {
	
	protected String[] commands;
	
	public PerformCommandsCommand(String[] commands) {
		this.commands = commands;
	}
}
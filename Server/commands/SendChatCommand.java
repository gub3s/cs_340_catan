package commands;

public abstract class SendChatCommand implements ICommand {
	public int gameID;
	public int playerIndex;
	public String content;
	public String type;
}
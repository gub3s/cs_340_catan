package commands;

import models.EdgeLocation;

public abstract class RoadBuildingCommand implements ICommand {
	public int gameID;
	public int playerIndex;
	public EdgeLocation spot1;
	public EdgeLocation spot2;
}
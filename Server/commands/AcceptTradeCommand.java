package commands;

public abstract class AcceptTradeCommand implements ICommand {
	public int gameID;
	public int playerIndex;
	public boolean willAccept;
}
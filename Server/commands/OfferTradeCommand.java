package commands;

import models.Resources;

public abstract class OfferTradeCommand implements ICommand {
	public int gameID;
	public int playerIndex;
	public Resources offer;
	public int receiver;
}
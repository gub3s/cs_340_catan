package commands;

public abstract class RollNumberCommand implements ICommand {
	public int gameID;
	public int playerIndex;
	public int number;
}
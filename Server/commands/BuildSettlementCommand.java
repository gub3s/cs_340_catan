package commands;

import models.VertexLocation;

public abstract class BuildSettlementCommand implements ICommand {
	public int gameID;
	public int playerIndex;
	public VertexLocation vertexLocation;
	public boolean free;
}
package commands.mock;

import commands.PerformCommandsCommand;

public class MockPerformCommandsCommand extends PerformCommandsCommand {
	public MockPerformCommandsCommand(String[] commands) {
		super(commands);
	}
	
	@Override
	public String execute() throws Exception {
		// update the server by applying the commands
		return "Bad request";
	}
}
package commands.mock;

import commands.MaritimeTradeCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class MockMaritimeTradeCommand extends MaritimeTradeCommand {
	@Override
	public String execute() throws Exception {
		// maritime trade logic
		String fileName = "sample/MovesMaritimeTradeExample.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);
		return response;
	}
}
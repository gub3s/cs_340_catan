package commands.mock;

import commands.AcceptTradeCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class MockAcceptTradeCommand extends AcceptTradeCommand {
	public String execute() throws Exception{
		// accept trade logic
		String fileName = "sample/model.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);
		return response;
	}
}
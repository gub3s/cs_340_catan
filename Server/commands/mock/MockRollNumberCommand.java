package commands.mock;

import commands.RollNumberCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;

public class MockRollNumberCommand extends RollNumberCommand {
	public MockRollNumberCommand() {}
	
	@Override
	public String execute() throws Exception {
		// roll number logic
		String fileName = "sample/MovesRollNumberExample.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);
		return response;
	}
}
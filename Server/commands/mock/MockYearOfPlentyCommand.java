package commands.mock;

import commands.YearOfPlentyCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class MockYearOfPlentyCommand extends YearOfPlentyCommand {
	@Override
	public String execute() throws Exception {
		// year of plenty logic
		String fileName = "sample/MovesYearOfPlentyExample.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);
		return response;
	}
}
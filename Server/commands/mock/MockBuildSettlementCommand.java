package commands.mock;

import commands.BuildSettlementCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class MockBuildSettlementCommand extends BuildSettlementCommand {
	@Override
	public String execute() throws Exception {
		// build settlement logic
		String fileName = "sample/model.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);
		return response;
	}
}
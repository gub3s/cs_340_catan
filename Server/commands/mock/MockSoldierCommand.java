package commands.mock;

import commands.SoldierCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class MockSoldierCommand extends SoldierCommand {
	@Override
	public String execute() throws Exception {
		// soldier logic
		String fileName = "sample/MovesSoldierExample.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);
		return response;
	}
}
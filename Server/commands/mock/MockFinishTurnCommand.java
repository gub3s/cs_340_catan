package commands.mock;

import commands.FinishTurnCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class MockFinishTurnCommand extends FinishTurnCommand {
	@Override
	public String execute() throws Exception {
		// finish turn logic
		String fileName = "sample/MovesFinishTurnExample.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);
		return response;
	}
}
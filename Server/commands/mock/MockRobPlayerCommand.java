package commands.mock;

import commands.RobPlayerCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;

public class MockRobPlayerCommand extends RobPlayerCommand {
	@Override
	public String execute() throws Exception {
		// rob player logic
		String fileName = "sample/MovesRobPlayerExample.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);
		return response;
	}
}
package commands.mock;

import commands.OfferTradeCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class MockOfferTradeCommand extends OfferTradeCommand {
	@Override
	public String execute() throws Exception {
		// offer Trade logic
		String fileName = "sample/MovesOfferTradeExample.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);
		return response;
	}
}
package commands.mock;

import commands.BuildRoadCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class MockBuildRoadCommand extends BuildRoadCommand {
	@Override
	public String execute() throws Exception {
		// build road logic
		String fileName = "sample/model.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);
		return response;
	}
}
package commands.mock;

import commands.RoadBuildingCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class MockRoadBuildingCommand extends RoadBuildingCommand {
	@Override
	public String execute() throws Exception {
		// road building logic
		String fileName = "sample/MovesRoadBuildingExample.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);
		return response;
	}
}
package commands.mock;

import commands.MonopolyCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class MockMonopolyCommand extends MonopolyCommand {
	@Override
	public String execute() throws Exception {
		// monopoly logic
		String fileName = "sample/MovesMonopolyExample.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);
		return response;
	}
}
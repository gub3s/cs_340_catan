package commands.mock;

import commands.BuildCityCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;

public class MockBuildCityCommand extends BuildCityCommand {
	@Override
	public String execute() throws Exception {
		// build city logic
		String fileName = "sample/model.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);
		return response;
	}
}
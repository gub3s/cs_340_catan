package commands.mock;

import commands.DiscardCardsCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class MockDiscardCardsCommand extends DiscardCardsCommand {
	@Override
	public String execute() throws Exception {
		// discard cards logic
		String fileName = "sample/MovesDiscardCardExample.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);
		return response;
	}
}
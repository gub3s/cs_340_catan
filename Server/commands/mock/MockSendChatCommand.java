package commands.mock;

import commands.SendChatCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;

public class MockSendChatCommand extends SendChatCommand {	
	@Override
	public String execute() throws Exception {
		// send chat logic
		String fileName = "sample/MovesSendChatExample.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);
		return response;
	}
}
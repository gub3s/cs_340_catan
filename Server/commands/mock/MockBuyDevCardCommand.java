package commands.mock;

import commands.BuyDevCardCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class MockBuyDevCardCommand extends BuyDevCardCommand {
	@Override
	public String execute() throws Exception {
		// buy dev card logic
		String fileName = "sample/MovesBuyDevCardExample.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);
		return response;
	}
}
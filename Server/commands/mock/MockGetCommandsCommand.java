package commands.mock;

import commands.GetCommandsCommand;

public class MockGetCommandsCommand extends GetCommandsCommand {
	public MockGetCommandsCommand() {}
	
	@Override
	public String execute() throws Exception {
		// get the list of commands sent to the server
		return "";
	}
}
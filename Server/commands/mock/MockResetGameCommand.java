package commands.mock;

import commands.ResetGameCommand;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class MockResetGameCommand extends ResetGameCommand {
	public MockResetGameCommand() {}
	
	public String execute() throws Exception {
		// reset the game
		String fileName = "sample/GameResetExample.json";		
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileName));
		byte[] bytes = new byte[is.available()];
		int countedBytes = is.read(bytes);
		String response = new String(bytes, 0, countedBytes);		
		return response;
	}
}
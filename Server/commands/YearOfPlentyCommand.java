package commands;

public abstract class YearOfPlentyCommand implements ICommand {
	public int gameID;
	public int playerIndex;
	public String resource1;
	public String resource2;
}
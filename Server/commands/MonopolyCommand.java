package commands;

public abstract class MonopolyCommand implements ICommand {
	public int gameID;
	public String resource;
	public int playerIndex;
}
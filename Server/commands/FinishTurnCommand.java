package commands;

public abstract class FinishTurnCommand implements ICommand {
	public int gameID;
	public int playerIndex;
}
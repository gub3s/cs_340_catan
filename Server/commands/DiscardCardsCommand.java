package commands;

import models.Resources;

public abstract class DiscardCardsCommand implements ICommand {
	public int gameID;
	public int playerIndex;
	public Resources discardedCards;
}
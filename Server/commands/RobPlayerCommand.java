package commands;

import models.Coordinate;

public abstract class RobPlayerCommand implements ICommand {
	public int gameID;
	public int playerIndex;
	public int victimIndex;
	public Coordinate robberSpot;
}
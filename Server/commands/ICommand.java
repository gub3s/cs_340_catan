package commands;

public interface ICommand {
	public String execute() throws Exception;
}
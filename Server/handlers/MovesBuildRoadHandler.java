package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.BuildRoadCommand;
import commands.real.RealBuildRoadCommand;

import game.GameProxy;

public class MovesBuildRoadHandler extends BaseHandler
{
	public void handle(HttpExchange exchange)
	{
		System.out.println("Build road");
		
		BuildRoadCommand cmd = this.fromJSON(exchange, RealBuildRoadCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.buildRoad(cmd);

		this.respondWithModel(exchange);
	}
}
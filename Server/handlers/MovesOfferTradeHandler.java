package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.OfferTradeCommand;
import commands.real.RealOfferTradeCommand;

import game.GameProxy;

public class MovesOfferTradeHandler extends BaseHandler
{
	public void handle(HttpExchange exchange)
	{
		System.out.println("Offer trade");
		
		OfferTradeCommand cmd = this.fromJSON(exchange, RealOfferTradeCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.offerTrade(cmd);

		this.respondWithModel(exchange);
	}
}
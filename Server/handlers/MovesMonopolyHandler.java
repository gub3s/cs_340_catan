package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.MonopolyCommand;
import commands.real.RealMonopolyCommand;

import game.GameProxy;

public class MovesMonopolyHandler extends BaseHandler
{
	public void handle(HttpExchange exchange)
	{
		System.out.println("Monopoly");
		
		MonopolyCommand cmd = this.fromJSON(exchange, RealMonopolyCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.monopoly(cmd);

		this.respondWithModel(exchange);
	}
}
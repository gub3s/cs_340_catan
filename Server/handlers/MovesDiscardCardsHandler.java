package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.DiscardCardsCommand;
import commands.real.RealDiscardCardsCommand;

import game.GameProxy;

public class MovesDiscardCardsHandler extends BaseHandler
{	
	public void handle(HttpExchange exchange)
	{
		System.out.println("Discard cards");
		
		DiscardCardsCommand cmd = this.fromJSON(exchange, RealDiscardCardsCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.discardCards(cmd);

		this.respondWithModel(exchange);
	}
}
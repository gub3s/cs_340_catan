package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.RobPlayerCommand;
import commands.real.RealRobPlayerCommand;

import game.GameProxy;

public class MovesRobPlayerHandler extends BaseHandler
{	
	public void handle(HttpExchange exchange)
	{
		System.out.println("Rob player");

		RobPlayerCommand cmd = this.fromJSON(exchange, RealRobPlayerCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.robPlayer(cmd);

		this.respondWithModel(exchange);
	}
}
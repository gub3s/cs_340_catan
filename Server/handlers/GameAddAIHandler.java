package handlers;

import java.net.HttpURLConnection;
import com.sun.net.httpserver.HttpExchange;
import java.util.HashMap;

import game.GameManager;
import models.Game;
import models.Player;

public class GameAddAIHandler extends BaseHandler
{
	public void handle(HttpExchange exchange)
	{
		System.out.println("Add AI");

		HashMap<String,String> data = this.decodeFormBody(exchange);
		String aiType = data.get("AIType");
		
		// Add an AI player to the game
		Game game = GameManager.getGame(this.currentGame(exchange));
		String playerName = game.chooseAiPlayerName(GameManager.aiNames);
		GameManager.joinGame(game.chooseAiColor(), game.getID(), -2, playerName, true);
		this.respond(exchange, HttpURLConnection.HTTP_OK, "");
	}
}
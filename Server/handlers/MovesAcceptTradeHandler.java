package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.AcceptTradeCommand;
import commands.real.RealAcceptTradeCommand;

import game.GameProxy;

public class MovesAcceptTradeHandler extends BaseHandler
{	
	public void handle(HttpExchange exchange)
	{
		System.out.println("Accept trade");
		
		AcceptTradeCommand cmd = this.fromJSON(exchange, RealAcceptTradeCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.acceptTrade(cmd);

		this.respondWithModel(exchange);
	}
}
package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.YearOfPlentyCommand;
import commands.real.RealYearOfPlentyCommand;

import game.GameProxy;

public class MovesYearOfPlentyHandler extends BaseHandler
{
	public void handle(HttpExchange exchange)
	{
		System.out.println("Year of Plenty");
		
		YearOfPlentyCommand cmd = this.fromJSON(exchange, RealYearOfPlentyCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.yearOfPlenty(cmd);

		this.respondWithModel(exchange);
	}
}
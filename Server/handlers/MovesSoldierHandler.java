package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.SoldierCommand;
import commands.real.RealSoldierCommand;

import game.GameProxy;

public class MovesSoldierHandler extends BaseHandler
{
	public void handle(HttpExchange exchange)
	{
		System.out.println("Move Soldier");
		
		SoldierCommand cmd = this.fromJSON(exchange, RealSoldierCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.soldier(cmd);

		this.respondWithModel(exchange);
	}
}
package handlers;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.HttpURLConnection;
import com.sun.net.httpserver.HttpExchange;

/**
 * Serves static files given a root or base directory
**/
public class FileHandler extends BaseHandler
{	
	/**
	 * Default root folder, passed in from the server,
	 * from which we should serve static files.
	**/
	private String root;

	/**
	 * Default file to serve, if none specified
	**/
	private static final String DEFAULT_FILE = "index.html";


	public FileHandler(String root)
	{
		this.root = root;
	}


	public void handle(HttpExchange exchange)
	{
		try
		{
			URI uri = exchange.getRequestURI();
			String path = this.root + uri.getPath();
			File file = new File(path).getCanonicalFile();

			if (!file.isFile())		// try the default file...
				file = new File(path + "/" + FileHandler.DEFAULT_FILE).getCanonicalFile();

			if (!file.isFile())		// ...or a JSON file (for swagger docs)
				file = new File(path + ".json").getCanonicalFile();

			if (!file.isFile())
				this.respond(exchange, HttpURLConnection.HTTP_NOT_FOUND, "404 Not Found");
			else if (!file.canRead())
				this.respond(exchange, HttpURLConnection.HTTP_FORBIDDEN, "403 Forbidden");
			else
				this.respond(exchange, HttpURLConnection.HTTP_OK, file);
		}
		catch (IOException e)
		{
			this.respond(exchange, HttpURLConnection.HTTP_INTERNAL_ERROR, "500 Internal Server Error");
			System.err.println("**CRASH** Could not load requested static file");
			System.err.println(e.getMessage());
			System.err.println(e.getStackTrace());
		}
	}
}
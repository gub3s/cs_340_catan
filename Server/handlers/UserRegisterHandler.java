package handlers;

import java.util.HashMap;
import java.net.HttpURLConnection;
import com.sun.net.httpserver.HttpExchange;

import game.GameManager;
import models.LoggedInUser;

public class UserRegisterHandler extends BaseHandler
{	
	public void handle(HttpExchange exchange)
	{
		System.out.println("User register");

		HashMap<String,String> req = this.decodeFormBody(exchange);
		
		int playerID = GameManager.registerUser(req.get("username"), req.get("password"));

		if (playerID < 0)
			this.respond(exchange, HttpURLConnection.HTTP_BAD_REQUEST, "Could not create user");
		else
		{
			LoggedInUser u = new LoggedInUser(req.get("username"), playerID);
			this.setCookie(exchange, "catan.user", this.toJSON(u));
			this.respond(exchange, HttpURLConnection.HTTP_OK, "");
		}
	}
}
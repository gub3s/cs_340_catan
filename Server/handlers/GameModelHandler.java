package handlers;

import java.net.HttpURLConnection;
import com.sun.net.httpserver.HttpExchange;
import java.util.HashMap;

import game.GameManager;

public class GameModelHandler extends BaseHandler
{
	public void handle(HttpExchange exchange)
	{
		//System.out.println("Game model");
		
		HashMap<String, String> req = this.decodeQueryString(exchange);
		int currentGame = this.toInt(this.getCookie(exchange, "catan.game"));
		int revision = this.toInt(req.get("revision"));

		// TODO: If revision is specified and matches the current revision, return true,
		// otherwise, if revision is -1 (not specified) or does not match the current
		// revision, return the current model.

		this.respondJSON(exchange, HttpURLConnection.HTTP_OK, GameManager.getGame(currentGame));
	}
}

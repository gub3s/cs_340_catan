package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.MaritimeTradeCommand;
import commands.real.RealMaritimeTradeCommand;

import game.GameProxy;

public class MovesMaritimeTradeHandler extends BaseHandler
{
	public void handle(HttpExchange exchange)
	{
		System.out.println("Maritime Trade");
		
		MaritimeTradeCommand cmd = this.fromJSON(exchange, RealMaritimeTradeCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.maritimeTrade(cmd);

		this.respondWithModel(exchange);
	}
}

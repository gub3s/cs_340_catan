package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.BuildCityCommand;
import commands.real.RealBuildCityCommand;

import game.GameProxy;

public class MovesBuildCityHandler extends BaseHandler
{	
	public void handle(HttpExchange exchange)
	{
		System.out.println("Build city");
		
		BuildCityCommand cmd = this.fromJSON(exchange, RealBuildCityCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.buildCity(cmd);

		this.respondWithModel(exchange);
	}
}
package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.FinishTurnCommand;
import commands.real.RealFinishTurnCommand;

import game.GameProxy;

public class MovesFinishTurnHandler extends BaseHandler
{
	public void handle(HttpExchange exchange)
	{
		System.out.println("Finish Turn");
		
		FinishTurnCommand cmd = this.fromJSON(exchange, RealFinishTurnCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.finishTurn(cmd);

		this.respondWithModel(exchange);
	}
}
package handlers;

import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.HashMap;
import java.net.URLEncoder;
import java.net.URLDecoder;
import java.net.HttpURLConnection;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.google.gson.Gson;

import game.GameManager;
import models.LoggedInUser;

public class BaseHandler implements HttpHandler
{
	private Gson gson = new Gson();

	/**
	 * Debug/default handler, responding with request headers
	**/
	public void handle(HttpExchange exchange) throws IOException {
		String requestMethod = exchange.getRequestMethod();
		if (requestMethod.equalsIgnoreCase("GET")) {
			OutputStream responseBody = exchange.getResponseBody();
			Headers requestHeaders = exchange.getRequestHeaders();
			Set<String> keySet = requestHeaders.keySet();
			Iterator<String> iter = keySet.iterator();
			String outputString = "";
			while (iter.hasNext()) {
				String key = iter.next();
				List values = requestHeaders.get(key);
				outputString += key + "=" + values.toString() + "\n";
			}
			this.respond(exchange, HttpURLConnection.HTTP_OK, outputString);
		}
	}

	/**
	 * Responds with the game model as specified by the user's cookie in the exchange
	**/
	protected void respondWithModel(HttpExchange exchange)
	{
		int gameID = this.gameID(exchange);
		this.respondJSON(exchange, HttpURLConnection.HTTP_OK, GameManager.getGame(gameID));
	}

	/**
	 * Gets the current game ID based on the user's cookie
	**/
	protected int gameID(HttpExchange exchange)
	{
		return this.toInt(this.getCookie(exchange, "catan.game"));
	}

	/**
	 * Responds to the client with a File and closes the exchange
	**/
	protected void respond(HttpExchange exchange, int status, File file)
	{
		OutputStream responseStream = exchange.getResponseBody();

		try
		{
			BufferedInputStream fstream = new BufferedInputStream(new FileInputStream(file));
			byte[] bytes = new byte[(int)file.length()];
			fstream.read(bytes, 0, bytes.length);
			fstream.close();
			exchange.getResponseHeaders().set("Content-Type", this.guessContentType(file));
			exchange.sendResponseHeaders(status, bytes.length);
			responseStream.write(bytes, 0, bytes.length);
			responseStream.flush();
			responseStream.close();
		}
		catch (IOException e)
		{
			this.respond(exchange, HttpURLConnection.HTTP_INTERNAL_ERROR, "500 Internal Server Error");
			System.err.println("**CRASH** Could not respond with file");
			System.err.println(e.getMessage());
			System.err.println(e.getStackTrace());
		}
		finally
		{
			exchange.close();
		}
	}

	/**
	 * Responds to the client with a String body and closes the exchange.
	**/
	protected void respond(HttpExchange exchange, int status, String body)
	{
		OutputStream responseStream = exchange.getResponseBody();

		try
		{
			exchange.getResponseHeaders().set("Content-Type", this.guessContentType(body));
			exchange.sendResponseHeaders(status, body.length());
			responseStream.write(body.getBytes());
			responseStream.flush();
			responseStream.close();
		}
		catch (IOException e)
		{
			System.err.println("**CRASH** Could not respond with string");
			System.err.println(e.getMessage());
			System.err.println(e.getStackTrace());
			try
			{
				exchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, 0);
			}
			catch (IOException e2)
			{
				System.err.println("**CRASH** Arrghh! Again, really!?? Geez. I give up...");
				System.err.println(e.getMessage());
				System.err.println(e.getStackTrace());
			}
		}
		finally
		{
			exchange.close();
		}
	}

	protected void respondJSON(HttpExchange exchange, int status, Object obj)
	{
		this.respond(exchange, status, this.toJSON(obj));
	}

	protected String getBody(HttpExchange exchange)
	{
		try
		{
			InputStreamReader inputStream = new InputStreamReader(exchange.getRequestBody());
			BufferedReader bufReader = new BufferedReader(inputStream);
			StringBuilder sb = new StringBuilder(512);
			int ch;
			while ((ch = bufReader.read()) != -1)
				sb.append((char)ch);
			bufReader.close();
			inputStream.close();
			return sb.toString();
		}
		catch (IOException e)
		{
			System.err.println("***CRASH*** Could not read body");
			System.err.println(e.getMessage());
			System.err.println(e.getStackTrace());
			return "";
		}
	}

	protected String toJSON(Object obj)
	{
		return this.gson.toJson(obj);
	}

	protected <T> T fromJSON(HttpExchange exchange, Class<T> c)
	{
		try
		{
			// Read the body into a string
			InputStreamReader inputStream = new InputStreamReader(exchange.getRequestBody());
			BufferedReader bufReader = new BufferedReader(inputStream);
			StringBuilder sb = new StringBuilder(512);
			int ch;
			while ((ch = bufReader.read()) != -1)
				sb.append((char)ch);
			bufReader.close();
			inputStream.close();
			return this.fromJSON(sb.toString(), c);
		}
		catch (IOException e)
		{
			System.err.println("***CRASH*** Could not read JSON body");
			System.err.println(e.getMessage());
			System.err.println(e.getStackTrace());
			return null;
		}
	}

	protected <T> T fromJSON(String json, Class<T> c)
	{
		return this.gson.fromJson(json, c);
	}

	protected void setCookie(HttpExchange exchange, String name, String value)
	{
		this.setCookie(exchange, name, value, "/");	// Default path is "/"
	}

	protected void setCookie(HttpExchange exchange, String name, String value, String path)
	{
		name = this.urlEncode(name);
		value = this.urlEncode(value);
		exchange.getResponseHeaders().add("Set-Cookie", name+"="+value+"; path="+path+";");
	}

	protected String getCookie(HttpExchange exchange, String name)
	{
		if (name.equals(""))
			return "";
 		
		// Cookies are apparently in the "Cookie" header
		String cookies = exchange.getRequestHeaders().get("Cookie").get(0);

		// Cookie name comes before '=' characters
		name = this.urlEncode(name);
		int start = cookies.indexOf(name+"=") + name.length() + 1;

		// Cookie values end with ';' or end of string
		if (start == name.length())
			return "";
		int end = cookies.indexOf(";", start);
		if (end < 0)
			end = cookies.length();

		// Cookie values are URL-encoded, so decode them
		return this.urlDecode(cookies.substring(start, end));
	}

	protected LoggedInUser currentPlayer(HttpExchange exchange)
	{
		return this.fromJSON(this.getCookie(exchange, "catan.user"), LoggedInUser.class);
	}

	protected int currentGame(HttpExchange exchange)
	{
		try
		{
			return Integer.parseInt(this.getCookie(exchange, "catan.game"));
		}
		catch (NumberFormatException e)
		{
			return -1;
		}
	}

	protected HashMap<String,String> decodeFormBody(HttpExchange exchange)
	{
		return this.decodeQueryString(this.getBody(exchange));
	}

	protected HashMap<String,String> decodeQueryString(HttpExchange exchange)
	{
		return this.decodeQueryString(exchange.getRequestURI().getQuery());
	}

	protected HashMap<String,String> decodeQueryString(String qs)
	{
		HashMap<String,String> data = new HashMap<String,String>();

		if (qs == null)
			return data;

		// Split each paramater
		String[] pairs = qs.split("&");

		// Now save each key/value pair
		for (int i = 0; i < pairs.length; i++)
		{
			String[] keyval = pairs[i].split("=");
			if (keyval.length < 2)
				continue;
			data.put(this.urlDecode(keyval[0]), this.urlDecode(keyval[1]));
		}

		return data;
	}

	protected int toInt(String s)
	{
		int result;
		try
		{
			result = Integer.parseInt(s);
		}
		catch (NumberFormatException e)
		{
			result = -1;
		}
		return result;
	}

	private String urlDecode(String str)
	{
		try
		{
			return URLDecoder.decode(str, "UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			System.err.println("***Crash*** (urlDecode) Unsupported character encoding");
			return str;
		}
	}

	private String urlEncode(String str)
	{
		try
		{
			return URLEncoder.encode(str, "UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			System.err.println("***Crash*** (urlEncode) Unsupported encoding");
			return str;
		}
	}

	private String guessContentType(String content)
	{
		if ((content.startsWith("{") && content.endsWith("}"))
			|| (content.startsWith("[") && content.endsWith("]")))
			return "application/json";
		else
			return "text/plain";
	}

	private String guessContentType(File file)
	{
		String name = file.getPath();
		if (name.endsWith(".html") || name.endsWith(".htm"))
			return "text/html";
		else if (name.endsWith(".jpg") || name.endsWith(".jpeg"))
			return "image/jpeg";
		else if (name.endsWith(".gif"))
			return "image/gif";
		else if (name.endsWith(".png"))
			return "image/png";
		else if (name.endsWith(".json"))
			return "application/json";
		else if (name.endsWith(".ico"))
			return "image/x-icon";
		else if (name.endsWith(".js"))
			return "text/javascript";
		else if (name.endsWith(".css"))
			return "text/css";
		else if (name.endsWith(".xml"))
			return "text/xml";
		else if (name.endsWith(".jar"))
			return "application/octet-stream";
		else
			return "text/plain";
	}
}
package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.RoadBuildingCommand;
import commands.real.RealRoadBuildingCommand;

import game.GameProxy;

public class MovesRoadBuildingHandler extends BaseHandler
{	
	public void handle(HttpExchange exchange)
	{
		System.out.println("Road building");
		
		RoadBuildingCommand cmd = this.fromJSON(exchange, RealRoadBuildingCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.roadBuilding(cmd);

		this.respondWithModel(exchange);
	}
}
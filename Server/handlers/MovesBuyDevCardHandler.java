package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.BuyDevCardCommand;
import commands.real.RealBuyDevCardCommand;

import game.GameProxy;

public class MovesBuyDevCardHandler extends BaseHandler
{	
	public void handle(HttpExchange exchange)
	{
		System.out.println("Buy Dev Card");
		
		BuyDevCardCommand cmd = this.fromJSON(exchange, RealBuyDevCardCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.buyDevCard(cmd);

		this.respondWithModel(exchange);
	}
}

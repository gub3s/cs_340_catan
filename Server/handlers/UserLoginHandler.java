package handlers;

import java.net.HttpURLConnection;
import java.util.HashMap;
import com.sun.net.httpserver.HttpExchange;

import game.GameProxy;
import game.GameManager;
import models.LoggedInUser;

public class UserLoginHandler extends BaseHandler
{
	public void handle(HttpExchange exchange)
	{
		System.out.println("User login");

		HashMap<String,String> req = this.decodeFormBody(exchange);
		
		int playerID = GameManager.loginUser(req.get("username"), req.get("password"));

		if (playerID < 0) {
			this.respond(exchange, HttpURLConnection.HTTP_BAD_REQUEST, "Invalid username/password combination");			
		}
		else
		{
			LoggedInUser u = new LoggedInUser(req.get("username"), playerID);
			this.setCookie(exchange, "catan.user", this.toJSON(u));
			this.respond(exchange, HttpURLConnection.HTTP_OK, "success");
		}
	}
}

package handlers;

import java.net.HttpURLConnection;
import com.sun.net.httpserver.HttpExchange;

import game.GameManager;

public class GamesListHandler extends BaseHandler
{	
	public void handle(HttpExchange exchange)
	{
		System.out.println("List games");
		this.respondJSON(exchange, HttpURLConnection.HTTP_OK, GameManager.listGames());
	}

	
}
package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.MonumentCommand;
import commands.real.RealMonumentCommand;

import game.GameProxy;

public class MovesMonumentHandler extends BaseHandler
{	
	public void handle(HttpExchange exchange)
	{
		System.out.println("Monument");
		
		MonumentCommand cmd = this.fromJSON(exchange, RealMonumentCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.monument(cmd);

		this.respondWithModel(exchange);
	}
}
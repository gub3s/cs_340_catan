package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.SendChatCommand;
import commands.real.RealSendChatCommand;

import game.GameProxy;

public class MovesSendChatHandler extends BaseHandler
{
	public void handle(HttpExchange exchange)
	{
		System.out.println("Send chat");

		SendChatCommand cmd = this.fromJSON(exchange, RealSendChatCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.sendChat(cmd);
		
		this.respondWithModel(exchange);
	}
}
package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.RollNumberCommand;
import commands.real.RealRollNumberCommand;

import game.GameProxy;

public class MovesRollNumberHandler extends BaseHandler
{	
	public void handle(HttpExchange exchange)
	{
		System.out.println("Roll number");
		
		RollNumberCommand cmd = this.fromJSON(exchange, RealRollNumberCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.rollNumber(cmd);

		this.respondWithModel(exchange);
	}
}
package handlers;

import com.sun.net.httpserver.HttpExchange;

import commands.BuildSettlementCommand;
import commands.real.RealBuildSettlementCommand;

import game.GameProxy;

public class MovesBuildSettlementHandler extends BaseHandler
{
	public void handle(HttpExchange exchange)
	{
		System.out.println("Build settlement");
		
		BuildSettlementCommand cmd = this.fromJSON(exchange, RealBuildSettlementCommand.class);
		cmd.gameID = this.gameID(exchange);
		GameProxy.buildSettlement(cmd);

		this.respondWithModel(exchange);
	}
}
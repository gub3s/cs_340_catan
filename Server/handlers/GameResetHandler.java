package handlers;

import com.sun.net.httpserver.HttpExchange;

import game.GameProxy;

public class GameResetHandler extends BaseHandler
{	
	public void handle(HttpExchange exchange)
	{
		System.out.println("Game Reset");
		GameProxy.resetGame(this.gameID(exchange));
		this.respondWithModel(exchange);
	}
}
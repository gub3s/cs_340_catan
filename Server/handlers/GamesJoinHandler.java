package handlers;

import com.sun.net.httpserver.HttpExchange;
import java.util.HashMap;
import java.net.HttpURLConnection;

import game.GameProxy;
import game.GameManager;
import models.Game;

public class GamesJoinHandler extends BaseHandler
{
	public void handle(HttpExchange exchange)
	{
		System.out.println("Join a game");
		int gameID = -1;
		HashMap<String, String> req = this.decodeFormBody(exchange);

		try
		{
			gameID = Integer.parseInt(req.get("id"));
		}
		catch (NumberFormatException e)
		{
			this.respond(exchange, HttpURLConnection.HTTP_BAD_REQUEST, "Game ID must be an integer");
			return;
		}

		if (!GameManager.joinGame(req.get("color"), gameID, this.currentPlayer(exchange).playerID, this.currentPlayer(exchange).name, false))
		{
			this.respond(exchange, HttpURLConnection.HTTP_BAD_REQUEST, "Game was full or does not exist");
			return;
		}
		else
		{
			this.setCookie(exchange, "catan.game", req.get("id"));
			this.respond(exchange, HttpURLConnection.HTTP_OK, "Joined game");
			return;
		}
	}
}
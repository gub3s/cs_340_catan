package persistence;

import java.util.List;
import java.io.File;
import java.io.IOException;

import models.Game;
import models.User;

/**
 * File storage class
 */
public class FileStorage implements IPersistentStore {

	public static String usersFilename = "users.json";
	public static String gamesFilename = "games.json";

	/**
	 * The driver for this file storage
	 */
	private DatabaseDriver driver;

	/**
	 * The user DAO for this file storage
	 */
	private UserFileDAO userDao;

	/**
	 * The game DAO for this file storage
	 */
	private GameFileDAO gameDao;

	/**
	 * Object for facilitating transactions
	 */
	private IDatabaseTransactionObject transactionObject;


	/**
	 * Initializes the persistent storage
	 */
	@Override
	public void init() {
		userDao = new UserFileDAO();
		gameDao = new GameFileDAO();
	}

	@Override
	public IUserDAO getUserDAO() {
		return userDao;
	}

	@Override
	public IGameDAO getGameDAO() {
		return gameDao;
	}

	@Override
	public void resetDatabase() {
		new File(this.usersFilename).delete();
		new File(this.gamesFilename).delete();
	}
	
	@Override
	public List<UserDTO> getAllUsers() {
		List<UserDTO> results = userDao.getAllUsers();
		return results;
	}
	
	@Override
	public void addUser(User user) {
		userDao.addUser(user);
	}
	
	@Override
	public List<GameDTO> getAllGames() {
		List<GameDTO> results = gameDao.getAllGames();
		return results;
	}
	
	@Override
	public void addGame(Game game) {
		gameDao.addGame(game);
	}
	
	@Override
	public void updateGame(int gameID, Game game) {
		gameDao.updateGame(gameID, game);
	}
}
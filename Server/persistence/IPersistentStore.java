package persistence;

import java.util.List;
import models.User;
import models.Game;

/**
 * Some high-level methods for the database and DAOs
 */
public interface IPersistentStore {
	/**
	 * Initializes the persistent storage.
	 * Can be either a relational or file storage
	 * Precondition: Storage is not yet initialized
	 * Postcondition: Storage is ready for transactions
	**/
	public void init();

	/**
	 * Gets the user DAO
	 * @return A user DAO
	 */
	public IUserDAO getUserDAO();

	/**
	 * Gets the game DAO
	 * @return A game DAO
	 */
	public IGameDAO getGameDAO();

	/**
	 * Resets the database
	 * Precondition: Storage is initialized
	 * Postcondition: The database has all tables but every table is empty
	**/
	public void resetDatabase();
	
	public List<UserDTO> getAllUsers();
	
	public void addUser(User user);
	
	public List<GameDTO> getAllGames();
	
	public void addGame(Game game);
	
	public void updateGame(int gameID, Game game);
}
package persistence;

import java.util.List;
import models.User;

/**
 * Some methods to facilitate user storage and retrieval
 */
public interface IUserDAO {
	
	/**
	 * Gets all users from storage
	 * @return The list of users stored in the database.  
	 * If no users exist in storage, returns an empty list.
	 */
	public List<UserDTO> getAllUsers();

	/**
	 * Adds a user to database.
	 * Pre-conditions: user cannot be null or empty. 
	 * user cannot already exist in the database.
	 * Post-conditions: user is added to the database.
	 * @param user The user to add.
	 * @return True if user is successfully stored in the database,
	 * and if the caller of this method can now safely
	 * assume the user is now in the database;
	 * false if preconditions are not met or error occurs
	 */
	public boolean addUser(User user);
}
package persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Game DAO for relational persistence
**/
public class RelationalDataTransactionObject implements IDatabaseTransactionObject {
	
	private RelationalStorage parent;
//	private final String DATABASE_URL = "jdbc:sqlite:" + "C:\\Source\\Catan\\catan.sqlite";
	private final String DATABASE_URL = "jdbc:sqlite:catan.sqlite";

	
	public RelationalDataTransactionObject(RelationalStorage parent) {
		this.parent = parent;
	}
	
	/**
	 * Starts a transaction
	 * Precondition: Database is initialized
	 * Postcondition: Transaction is started
	**/
	@Override
	public void startTransaction() {
		try {
			Connection connection = DriverManager.getConnection(DATABASE_URL);
			assert(connection != null);
			connection.setAutoCommit(false);
			parent.setConnection(connection);
		} catch (SQLException e) {
			System.out.println("ERROR: Failed to start transaction");
			e.printStackTrace();
		}
	}

	/**
	 * Ends a transaction
	 * Precondition: Database is initialized and transaction has been started
	 * Postcondition: Transaction is ended
	 * @param success Whether or not the transaction was successful
	**/
	@Override
	public void endTransaction(boolean success) {
		Connection connection = parent.getConnection();
		
		try {
			assert(!connection.isClosed());
			if (success) {
				connection.commit();
			}
			else {
				connection.rollback();
			}
		}
		catch(SQLException e) {
			System.out.println("Failed to commit or rollback transaction");
			e.printStackTrace();
		}
		finally {
			try {
				connection.close();
				parent.setConnection(connection);
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
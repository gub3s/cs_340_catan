package persistence;

import com.google.gson.Gson;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import models.Game;

/**
 * Game DAO for relational persistence
**/
public class GameRelationalDAO implements IGameDAO {
	
	public RelationalStorage parent;
	
	public GameRelationalDAO(RelationalStorage parent) {
		this.parent = parent;
	}

	// Javadoc inherited from super class
	@Override
	public List<GameDTO> getAllGames() {
		Connection connection = parent.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<GameDTO> results = new ArrayList<>();
		Gson gson = new Gson();
		
		try {
			String sql = "SELECT id, game FROM games";
			stmt = connection.prepareStatement(sql);
			
			rs = stmt.executeQuery();
			if (!rs.isBeforeFirst()) {
				//Empty.  No users available
				return results;
			}
			while (rs.next()) {
				GameDTO gameDto = new GameDTO(gson.fromJson(rs.getString(2), Game.class));
				results.add(gameDto);
			}
			return results;
		}
		catch (SQLException e) {
			e.printStackTrace();
			return results;
		}
		finally {
			try {
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			} catch(SQLException e) { e.printStackTrace(); }
		}
	}

	// Javadoc inherited from super class
	@Override
	public boolean addGame(Game game) {
		if (game == null)
			return false;
		
		Connection connection = parent.getConnection();
		PreparedStatement stmt = null;
		Gson gson = new Gson();
		try {
			String sql = "INSERT INTO games (id, game) VALUES (?, ?)";
			stmt = connection.prepareStatement(sql);
			
			String gameString = gson.toJson(game);
			
			stmt.setInt(1, game.getID());
			stmt.setString(2, gameString);
			
			if (stmt.executeUpdate() == 1) {
				return true;
			}
			else {
				return false;
			}
		}
		catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
			return false;
		}
		finally {
			try { stmt.close(); } 
			catch (SQLException e) { e.printStackTrace(); }
		}
	}

	// Javadoc inherited from super class
	@Override
	public boolean updateGame(int gameID, Game game) {
		if (game == null) {
			return false;
		}
		
		Connection connection = parent.getConnection();
		PreparedStatement stmt = null;
		Gson gson = new Gson();
		try {
			String sql = "UPDATE games SET game = ? WHERE id = ?";
			stmt = connection.prepareStatement(sql);
			
			String gameString = gson.toJson(game);
			
			stmt.setString(1, gameString);
			stmt.setInt(2, gameID);
			
			if (stmt.executeUpdate() == 1) {
				return true;
			}
			else {
				return false;
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		finally {
			try { stmt.close(); } 
			catch (SQLException e) { e.printStackTrace(); }
		}
	}
}
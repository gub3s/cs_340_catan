package persistence;

import java.util.ArrayList;
import models.CatanMap;
import models.Chat;
import models.DevCards;
import models.Game;
import models.Log;
import models.Player;
import models.Resources;
import models.TradeOffer;
import models.TurnTracker;

/**
 * This class encapsulates a Game object for 
 * transfer between the model and database
 */
public class GameDTO {

	// Member Variables
	private int gameId;
	private String name;
	private DevCards deck;
	private CatanMap map;
	private ArrayList<Player> players;
	private Log log;
	private Chat chat;
	private Resources bank;
	private TurnTracker turnTracker;
	private int biggestArmy;
	private int longestRoad;
	private int winner;
	private TradeOffer tradeOffer;
	private int revision;
	
	public GameDTO(Game game) {
		this.gameId = game.getID();
		this.name = game.getName();
		this.deck = game.getDeck();
		this.map = game.getMap();
		this.players = game.getPlayers();
		this.log = game.getLog();
		this.chat = game.getChat();
		this.bank = game.getBank();
		this.turnTracker = game.getTurnTracker();
		this.biggestArmy = game.getBiggestArmy();
		this.longestRoad = game.getLongestRoad();
		this.winner = game.getWinner();
		this.tradeOffer = game.getTradeOffer();
		this.revision = game.getRevision();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return this.gameId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the deck
	 */
	public DevCards getDeck() {
		return deck;
	}

	/**
	 * @return the map
	 */
	public CatanMap getMap() {
		return map;
	}

	/**
	 * @return the players
	 */
	public ArrayList<Player> getPlayers() {
		return players;
	}

	/**
	 * @return the log
	 */
	public Log getLog() {
		return log;
	}

	/**
	 * @return the chat
	 */
	public Chat getChat() {
		return chat;
	}

	/**
	 * @return the bank
	 */
	public Resources getBank() {
		return bank;
	}

	/**
	 * @return the turnTracker
	 */
	public TurnTracker getTurnTracker() {
		return turnTracker;
	}

	/**
	 * @return the biggestArmy
	 */
	public int getBiggestArmy() {
		return biggestArmy;
	}

	/**
	 * @return the longestRoad
	 */
	public int getLongestRoad() {
		return longestRoad;
	}

	/**
	 * @return the winner
	 */
	public int getWinner() {
		return winner;
	}

	/**
	 * @return the tradeOffer
	 */
	public TradeOffer getTradeOffer() {
		return tradeOffer;
	}

	/**
	 * @return the revision
	 */
	public int getRevision() {
		return revision;
	}
}

package persistence;

import models.User;

/**
 * This class encapsulates a User object for 
 * transfer between the model and database
 */
public class UserDTO extends User {
	
	public UserDTO(User user) {
		super(user);
	}

}

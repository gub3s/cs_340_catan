package persistence;

import java.io.*;
import java.util.List;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import models.User;

/**
 * User DAO for file storage persistence
**/
public class UserFileDAO implements IUserDAO {

	private Gson gson = new Gson();
	
	@Override
	public List<UserDTO> getAllUsers() {
		List<UserDTO> users = new ArrayList<UserDTO>();

		// Load the existing user "database"
		try
		{
			File file = new File(FileStorage.usersFilename);
			BufferedInputStream fstream = new BufferedInputStream(new FileInputStream(file));
			byte[] bytes = new byte[(int)file.length()];
			fstream.read(bytes, 0, bytes.length);
			fstream.close();

			// Load all persisted users into a list
			users = this.gson.fromJson(new String(bytes, "UTF-8"), new TypeToken<List<UserDTO>>(){}.getType());
		}
		catch (Exception e)
		{
			System.err.println(FileStorage.usersFilename + " not found; creating it");
		}

		return users;
	}

	/**
	 * Adds a user to storage; if the user is already in the database,
	 * the database copy is updated.
	 * Precondition: The user is not null and is not already in the database
	 * Postcondition: The user is in the database
	 * @param user The user to add
	 * @return True if the caller of this method can now safely
	 * assume the user is now in the database; false otherwise.
	**/
	@Override
	public boolean addUser(User user) {
		if (user == null)
			return false;
		
		List<UserDTO> users = this.getAllUsers();

		// Find a user with this ID to replace that one with this newer one
		for (int i = 0; i < users.size(); i++)
		{
			if (users.get(i).getUserID() == user.getUserID())
			{
				users.remove(i);
				break;
			}
		}

		// Save the user
		users.add(new UserDTO(user));

		// Serialize the list
		String jsonOut = this.gson.toJson(users);

		// Write the JSON back out to a file
		try
		{
			PrintWriter writer = new PrintWriter(FileStorage.usersFilename, "UTF-8");
			writer.println(jsonOut);
			writer.close();
			return true;
		}
		catch (Exception e)
		{
			System.err.println("I hate my life. :P");
			return false;
		}
	}
}
package persistence;

import com.google.gson.Gson;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.User;

/**
 * User DAO for relational persistence
**/
public class UserRelationalDAO implements IUserDAO {
	
	public RelationalStorage parent;
	
	public UserRelationalDAO(RelationalStorage parent) {
		this.parent = parent;
	}
	
	// Javadoc inherited from super class
	@Override
	public List<UserDTO> getAllUsers() {
		Connection connection = parent.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<UserDTO> results = new ArrayList<>();
		Gson gson = new Gson();
		
		try {
			String sql = "select id, player from users";
			stmt = connection.prepareStatement(sql);
			
			rs = stmt.executeQuery();
			if (!rs.isBeforeFirst()) {
				//Empty.  No users available
				return results;
			}
			while (rs.next()) {
				UserDTO userDto = new UserDTO(gson.fromJson(rs.getString(2), User.class));
				results.add(userDto);
			}
			return results;
		}
		catch (SQLException e) {
			e.printStackTrace();
			return results;
		}
		finally {
			try {
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			} catch(SQLException e) { e.printStackTrace(); }
		}
	}

	// Javadoc inherited from super class
	@Override
	public boolean addUser(User user) {
		if (user == null) {
			return false;
		}
		
		Connection connection = parent.getConnection();
		PreparedStatement stmt = null;
		Gson gson = new Gson();
		try {
			String sql = "INSERT INTO users (id, player) VALUES (?, ?)";
			stmt = connection.prepareStatement(sql);
			
			String userString = gson.toJson(user);
			
			stmt.setInt(1, user.getUserID());
			stmt.setString(2, userString);
			
			if (stmt.executeUpdate() == 1) {
				return true;
			}
			else {
				return false;
			}
		}
		catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
			return false;
		}
		finally {
			try { stmt.close(); } 
			catch (Exception e) { e.printStackTrace(); }
		}
	}
}
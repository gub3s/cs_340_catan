package persistence;

import java.io.*;
import java.util.List;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import models.Game;

/**
 * Game DAO for file persistence
**/
public class GameFileDAO implements IGameDAO {

	private Gson gson = new Gson();
	
	@Override
	public List<GameDTO> getAllGames() {
		List<Game> games = new ArrayList<Game>();
		List<GameDTO> gameDtos = new ArrayList<GameDTO>();

		// Load the existing game "database"
		try
		{
			File file = new File(FileStorage.gamesFilename);
			BufferedInputStream fstream = new BufferedInputStream(new FileInputStream(file));
			byte[] bytes = new byte[(int)file.length()];
			fstream.read(bytes, 0, bytes.length);
			fstream.close();

			// Load all persisted games into a list
			games = this.gson.fromJson(new String(bytes, "UTF-8"), new TypeToken<List<Game>>(){}.getType());
		}
		catch (Exception e)
		{
			System.err.println(FileStorage.gamesFilename + " not found; creating it");
		}

		for (Game game : games)
			gameDtos.add(new GameDTO(game));

		return gameDtos;
	}

	/**
	 * Adds a game to storage; game must not already be in storage
	 * Precondition: Game is a valid game name and is not null
	 * Postcondition: Game is in the storage (or false will be returned)
	 * @param game The game to add
	 * @return True if the game is in storage, false otherwise
	**/
	@Override
	public boolean addGame(Game game) {
		if (game == null)
			return false;

		List<GameDTO> games = this.getAllGames();

		// Make sure the game does not already exist
		for (int i = 0; i < games.size(); i++)
			if (games.get(i).getId() == game.getID())
				return false;

		// Save the game
		games.add(new GameDTO(game));

		// Serialize the list
		String jsonOut = this.gson.toJson(games);

		// Write the JSON back out to a file
		try
		{
			PrintWriter writer = new PrintWriter(FileStorage.gamesFilename, "UTF-8");
			writer.println(jsonOut);
			writer.close();
			return true;
		}
		catch (Exception e)
		{
			System.err.println("I hate my life. :P");
			return false;
		}
	}

	/**
	 * Updates a game
	 * Precondition: The game is a valid game, the game ID exists for a game, and is not null
	 * Postcondition: The game in storage has the latest info (or false is returned)
	 * @param gameID The id of the game to be updated
	 * @param game The updated game information
	 * @return True if storage now has the updated game info, false otherwise
	**/
	@Override
	public boolean updateGame(int gameID, Game game) {
		if (game == null)
			return false;

		List<GameDTO> games = this.getAllGames();

		// Make sure the game exists in the file and remove it
		boolean exists = false;
		for (int i = 0; i < games.size(); i++)
		{
			if (games.get(i).getId() == game.getID())
			{
				games.remove(i);
				exists = true;
				break;
			}
		}

		if (!exists)
			return false;

		// Save the game
		games.add(new GameDTO(game));

		// Serialize the list
		String jsonOut = this.gson.toJson(games);

		// Write the JSON back out to a file
		try
		{
			PrintWriter writer = new PrintWriter(FileStorage.gamesFilename, "UTF-8");
			writer.println(jsonOut);
			writer.close();
			return true;
		}
		catch (Exception e)
		{
			System.err.println("I hate my life. :P");
			return false;
		}
	}
}
package persistence;

import com.google.gson.Gson;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * DatabaseDriver facilitates some high-level database operations
 * like connecting and clearing
 */
public class DatabaseDriver {
	
	public RelationalStorage parent;
	
	public DatabaseDriver(RelationalStorage parent) {
		this.parent = parent;
	}
	
	/**
	 * Connects to the database
	 * Precondition: Database is initialized
	 * Postcondition: Database is connected, ready to use
	**/
	public void connect() {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			System.out.println("Could not initialize database driver");
			e.printStackTrace();
		}
	}

	/**
	 * Clears the database by erasing all tuples while 
	 * leaving the tables intact
	 * Precondition: Database is initialized
	 * Postcondition: Database is empty/reset
	**/
	public void clear() {
		Connection connection = parent.getConnection();
		PreparedStatement stmt = null;
		try {
			String sql = "DELETE FROM users";
			stmt = connection.prepareStatement(sql);
			
			stmt.executeUpdate();
			
			sql = "DELETE FROM games";
			stmt = connection.prepareStatement(sql);
			
			stmt.executeUpdate();
			
			sql = "DELETE FROM commands";
			stmt = connection.prepareStatement(sql);
			
			stmt.executeUpdate();
		}
		catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
		finally {
			try { stmt.close(); } 
			catch (Exception e) { e.printStackTrace(); }
		}
	}
}
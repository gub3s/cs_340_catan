package persistence;


/**
 * Methods to enable transactions
**/
public interface IDatabaseTransactionObject {
	/**
	 * Starts a transaction
	 * Precondition: Database is initialized
	 * Postcondition: Transaction is started
	**/
	public void startTransaction();

	/**
	 * Ends a transaction
	 * Precondition: Database is initialized and transaction has been started
	 * Postcondition: Transaction is ended
	 * @param success Whether or not the transaction was successful
	**/
	public void endTransaction(boolean success);
}
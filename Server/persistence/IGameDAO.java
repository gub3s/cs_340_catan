package persistence;

import java.util.List;
import models.Game;

/**
 * Some methods to facilitate game storage and retrieval
 */
public interface IGameDAO {
	
	/**
	 * Gets all games from storage
	 * @return The list of games stored in the database.  If no games
	 * exist in storage, returns an empty list
	 */
	public List<GameDTO> getAllGames();

	/**
	 * Adds a game to the database.
	 * Pre-conditions: game cannot be null or empty. 
	 * Game cannot already exist in the database
	 * Post-conditions: game is added to the database
	 * @param game The game to add.
	 * @return True if game is successfully stored in the database,
	 * false if preconditions are not met or error occurs
	 */
	public boolean addGame(Game game);

	/**
	 * Updates the game stored in the database specified by the given id
	 * Pre-conditions: game cannot be null or empty.  game must already
	 * exist in the database.
	 * Post-conditions: the specified game is updated
	 * @param gameID The id of the game to be updated
	 * @param game The updated game information
	 * @return True if the game tuple is updated with the given information,
	 * false if preconditions not met or error occurs
	 */
	public boolean updateGame(int gameID, Game game);
}
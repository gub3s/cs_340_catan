package persistence;


public class FileDataTransactionObject implements IDatabaseTransactionObject {
	/**
	 * Starts a transaction
	 * Precondition: Database is initialized
	 * Postcondition: Transaction is started
	**/
	@Override
	public void startTransaction() {
		
	}

	/**
	 * Ends a transaction
	 * Precondition: Database is initialized and transaction has been started
	 * Postcondition: Transaction is ended
	 * @param success Whether or not the transaction was successful
	**/
	@Override
	public void endTransaction(boolean success) {
		if (success) {
			
		}
		else {
			
		}
	}
}

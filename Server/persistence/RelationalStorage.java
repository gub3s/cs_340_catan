package persistence;

import java.sql.Connection;
import java.util.List;
import models.Game;
import models.User;

/**
 * Relational storage class
 */
public class RelationalStorage implements IPersistentStore {
	/**
	 * The driver for this relational database
	 */
	private DatabaseDriver driver;

	/**
	 * The user DAO for this relational database
	 */
	private UserRelationalDAO userDao;

	/**
	 * The game DAO for this relational database
	 */
	private GameRelationalDAO gameDao;

	/**
	 * Object for facilitating transactions
	 */
	private IDatabaseTransactionObject transObj;
	
	/**
	 * Database connection
	 */
	private Connection connection;
	
	public RelationalStorage() {
		this.init();
	}

	@Override
	public void init() {
		System.out.println("Init RelationalStorage");
		driver = new DatabaseDriver(this);
		transObj = new RelationalDataTransactionObject(this);
		connection = null;
		userDao = new UserRelationalDAO(this);
		gameDao = new GameRelationalDAO(this);
		driver.connect();
	}
	
	private void test() {
		// Test adding user
		User user1 = new User("James", "james", 0);
		User user2 = new User("Sam", "sam", 1);
		getTransactionObject().startTransaction();
		if (userDao.addUser(user1) && userDao.addUser(user2)) {
			getTransactionObject().endTransaction(true);
		}
		else {
			getTransactionObject().endTransaction(false);
		}
		
		getTransactionObject().startTransaction();
		
		// Test getting users
		List<UserDTO> users = userDao.getAllUsers();
		getTransactionObject().endTransaction(true);
		if (users.isEmpty()) {
			System.out.println("No users to print");
		}
		else {
			for (UserDTO player : users) {
				System.out.println(player.getUsername());
			}
		}
		
		// Test adding game
		Game game1 = new Game("game1", 0);
		Game game2 = new Game("game2", 1);
		getTransactionObject().startTransaction();
		if (gameDao.addGame(game1) && gameDao.addGame(game2)) {
			getTransactionObject().endTransaction(true);
		}
		else {
			getTransactionObject().endTransaction(false);
		}
		
		// Test getting games
		getTransactionObject().startTransaction();
		List<GameDTO> games = gameDao.getAllGames();
		getTransactionObject().endTransaction(true);
		if (games.isEmpty()) {
			System.out.println("No games to print");
		}
		else {
			for (GameDTO game : games) {
				System.out.println(game.getName());
			}
		}
		
		// Test updating game
		getTransactionObject().startTransaction();
		game1.setName("game1.1");
		if (gameDao.updateGame(game1.getID(), game1)) {
			getTransactionObject().endTransaction(true);
		}
		else {
			getTransactionObject().endTransaction(false);
		}
		
		getTransactionObject().startTransaction();
		games = gameDao.getAllGames();
		getTransactionObject().endTransaction(true);
		if (games.isEmpty()) {
			System.out.println("No games to print");
		}
		else {
			for (GameDTO game : games) {
				System.out.println(game.getName());
			}
		}
	}

	@Override
	public IUserDAO getUserDAO() {
		return userDao;
	}

	@Override
	public IGameDAO getGameDAO() {
		return gameDao;
	}
	
	@Override
	public void resetDatabase() {
		getTransactionObject().startTransaction();
		driver.clear();
		getTransactionObject().endTransaction(true);
	}
	
	public Connection getConnection() {
		assert(connection != null);
		return connection;
	}
	
	public void setConnection (Connection connection) {
		this.connection = connection;
	}
	
	/**
	 *  For test purposes only. Do not run user for release.
	 */
	public static void main(String[] args) {
		RelationalStorage relStor = new RelationalStorage();
	}

	/**
	 * @return the transObj
	 */
	public IDatabaseTransactionObject getTransactionObject() {
		return transObj;
	}
	
	@Override
	public List<UserDTO> getAllUsers() {
		List<UserDTO> results = null;
		
		transObj.startTransaction();
		results = userDao.getAllUsers();
		transObj.endTransaction(true);
		
		return results;
	}
	
	@Override
	public void addUser(User user) {
		transObj.startTransaction();
		if (userDao.addUser(user)) {
			transObj.endTransaction(true);
		}
		else {
			transObj.endTransaction(false);
		}
	}
	
	@Override
	public List<GameDTO> getAllGames() {
		List<GameDTO> results = null;
		
		transObj.startTransaction();
		results = gameDao.getAllGames();
		transObj.endTransaction(true);
		
		return results;
	}
	
	@Override
	public void addGame(Game game) {
		transObj.startTransaction();
		if (gameDao.addGame(game)) {
			transObj.endTransaction(true);
		}
		else {
			transObj.endTransaction(false);
		}
	}
	
	@Override
	public void updateGame(int gameID, Game game) {
		transObj.startTransaction();
		if (gameDao.updateGame(gameID, game)) {
			transObj.endTransaction(true);
		}
		else {
			transObj.endTransaction(false);
		}
	}
}
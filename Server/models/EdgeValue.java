package models;

public class EdgeValue
{
	private int ownerID;
	
	public EdgeValue() {
		this.ownerID = -1;
	}

	/**
	 * @return the ownerID
	 */
	public int getOwnerID() {
		return ownerID;
	}

	/**
	 * @param ownerID the ownerID to set
	 */
	public void setOwnerID(int ownerID) {
		this.ownerID = ownerID;
	}
}
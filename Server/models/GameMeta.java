package models;

import java.util.ArrayList;

// Utility class just used for JSON serialization
public class GameMeta
{
	public String title;
	public int id;
	public ArrayList<PlayerMeta> players = new ArrayList<PlayerMeta>();

	public GameMeta(String name, int gameID)
	{
		this.title = name;
		this.id = gameID;
	}

	public void addPlayer(Player p)
	{
		players.add(new PlayerMeta(p.getColor(), p.getName(), p.getID()));
	}

	// WOW. Their joinGame.html page is a mess.
	// This hack is required in order to get their
	// JS front-end to function properly.
	public void addEmptyPlayers()
	{
		while (players.size() < 4)
			players.add(new PlayerMeta());
	}

	private class PlayerMeta
	{
		public String color;
		public String name;
		public Integer id;

		public PlayerMeta() { }	// Only exists because of necessary hack...

		public PlayerMeta(String color, String name, int id)
		{
			this.color = color;
			this.name = name;
			this.id = id;
		}
	}
}
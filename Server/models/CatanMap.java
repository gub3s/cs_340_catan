package models;

import java.util.HashMap;
import java.util.ArrayList;

public class CatanMap
{
	private HexGrid hexGrid = new HexGrid();
	private int radius = 4;
	// numbers should be a map (hopefully) with 2-12 as the keys and arrays as the values
	private HashMap<Integer, ArrayList<Coordinate>> numbers = new HashMap<Integer, ArrayList<Coordinate>>();
	private Port[] ports = new Port[9];
	private Coordinate robber = new Coordinate();
	
	public CatanMap() {
		
	}

	public HexGrid getHexGrid() {
		return hexGrid;
	}

	public int getRadius() {
		return radius;
	}

	public HashMap<Integer, ArrayList<Coordinate>> getNumbers() {
		return numbers;
	}

	public Port[] getPorts() {
		return ports;
	}

	public Coordinate getRobber() {
		return robber;
	}

	public void setRadius(int newRadius) {
		radius = newRadius;
	}
}
package models;

import java.util.ArrayList;
import java.util.Random;
import persistence.GameDTO;

public class Game
{
	private int gameId;
	private String name;
	private DevCards deck;
	private CatanMap map;
	private ArrayList<Player> players;
	private Log log;
	private Chat chat;
	private Resources bank;
	private TurnTracker turnTracker;
	private int biggestArmy;
	private int longestRoad;
	private int winner;
	private TradeOffer tradeOffer;
	private int revision;

	public Game(String name, int id) {
		this.gameId = id;
		this.name = name;
		this.tradeOffer = null;
	}
	
	public Game(GameDTO gameDto) {
		this.gameId = gameDto.getId();
		this.name = gameDto.getName();
		this.deck = gameDto.getDeck();
		this.map = gameDto.getMap();
		this.players = gameDto.getPlayers();
		this.log = gameDto.getLog();
		this.chat = gameDto.getChat();
		this.bank = gameDto.getBank();
		this.turnTracker = gameDto.getTurnTracker();
		this.biggestArmy = gameDto.getBiggestArmy();
		this.longestRoad = gameDto.getLongestRoad();
		this.winner = gameDto.getWinner();
		this.tradeOffer = gameDto.getTradeOffer();
		this.revision = gameDto.getRevision();
	}

	/**
	 * @return the id
	 */
	public int getID() {
		return this.gameId;
	}

	public void setID(int id) {
		this.gameId = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the biggestArmy
	 */
	public int getBiggestArmy() {
		return biggestArmy;
	}

	/**
	 * @param biggestArmy the biggestArmy to set
	 */
	public void setBiggestArmy(int biggestArmy) {
		this.biggestArmy = biggestArmy;
	}

	/**
	 * @return the longestRoad
	 */
	public int getLongestRoad() {
		return longestRoad;
	}

	/**
	 * @param longestRoad the longestRoad to set
	 */
	public void setLongestRoad(int longestRoad) {
		this.longestRoad = longestRoad;
	}

	/**
	 * @return the winner
	 */
	public int getWinner() {
		return winner;
	}

	/**
	 * @param winner the winner to set
	 */
	public void setWinner(int winner) {
		this.winner = winner;
	}

	/**
	 * @return the revision
	 */
	public int getRevision() {
		return revision;
	}

	/**
	 * @param revision the revision to set
	 */
	public void setRevision(int revision) {
		this.revision = revision;
	}
	
	public void incrementRevision() {
		revision++;
	}

	public void appendChat(String source, String message) {
		chat.addLine(new Line(source, message));
	}

	public boolean addPlayer(Player p) {
		if (this.players.size() < 4)
		{
			this.players.add(p);
			return true;
		}
		else
			return false;

	}

	public ArrayList<Player> getPlayers() {
		return players;
	}
	
	public Player getPlayer(int index) {
		return players.get(index);
	}

	/**
	 * @return the map
	 */
	public CatanMap getMap() {
		return map;
	}

	/**
	 * @param map the map to set
	 */
	public void setMap(CatanMap map) {
		this.map = map;
	}

	/**
	 * Chooses a valid AI player name that isn't used
	**/
	public String chooseAiPlayerName(String[] names)
	{
		Random rand = new Random();
		int randomNum = rand.nextInt((names.length - 1) + 1);
		while (this.hasPlayerWithName(names[randomNum]))
			randomNum = randomNum < names.length - 1 ? randomNum + 1 : 0;
		return names[randomNum];
	}
	
	/**
	 * Tells you if this game has a player with a name
	**/
	public boolean hasPlayerWithName(String name)
	{
		for (Player p : this.players)
			if (p.getName().equals(name))
				return true;
		return false;
	}

	public String chooseAiColor()
	{
		String[] colors = {
			"red",
			"green",
			"blue",
			"yellow",
			"puce",
			"brown",
			"white",
			"purple",
			"orange"
		};

		for (String c : colors)
			if (!this.colorTaken(c))
				return c;
		return "NONE_AVAILABLE";
	}

	/**
	 * Whether the color is already used in this game
	**/
	public boolean colorTaken(String color)
	{
		for (Player p : this.players)
			if (p.getColor().equals(color))
				return true;
		return false;
	}
	
	/**
	 * @return the deck
	 */
	public DevCards getDeck() {
		return deck;
	}

	/**
	 * @return the log
	 */
	public Log getLog() {
		return log;
	}

	/**
	 * @return the chat
	 */
	public Chat getChat() {
		return chat;
	}

	/**
	 * @return the bank
	 */
	public Resources getBank() {
		return bank;
	}

	/**
	 * @return the turnTracker
	 */
	public TurnTracker getTurnTracker() {
		return turnTracker;
	}

	public Player getCurrentPlayer() {
		return this.players.get(this.turnTracker.getCurrentTurn());
	}
	
	public boolean playersMustDiscard() {
		for (Player player : players) {
			if (player.getResources().getTotal() > 7) {
				return true;
			}
		}
		return false;
	}

	public boolean isAllAi()
	{
		for (Player p : this.players)
			if (!p.isAI)
				return false;
		return true;
	}

	public TradeOffer getTradeOffer()
	{
		return this.tradeOffer;
	}

	public void setTradeOffer(TradeOffer to)
	{
		this.tradeOffer = to;
	}
}
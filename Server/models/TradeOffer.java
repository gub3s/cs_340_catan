package models;

// Utility class just used for JSON serialization
public class TradeOffer
{
	private int sender;
	private int receiver;
	private Resources offer = new Resources();

	public TradeOffer(int s, int r, Resources res)
	{
		this.sender = s;
		this.receiver = r;
		this.offer = res;
	}

	public void setOffer(Resources res)
	{
		this.offer = res;
	}

	public Resources getOffer()
	{
		return this.offer;
	}

	public int getSender()
	{
		return this.sender;
	}

	public int getReceiver()
	{
		return this.receiver;
	}

	public boolean satisfiedBy(Resources res)
	{
		return res.getBrick() >= this.offer.getBrick() * -1
				&& res.getWood() >= this.offer.getWood() * -1
				&& res.getSheep() >= this.offer.getSheep() * -1
				&& res.getWheat() >= this.offer.getWheat() * -1
				&& res.getOre() >= this.offer.getOre() * -1;

	}
}
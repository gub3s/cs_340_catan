package models;

public class Line
{
	// private variables
	private String source;
	private String message;
	
	public Line() {
		source = "";
		message = "";
	}
	
	public Line(String source, String message) {
		this.source = source;
		this.message = message;
	}

	public void setSource(String newSource) {
		source = newSource;
	}

	public void setMessage(String newMessage) {
		message = newMessage;
	}

	public String getSource() {
		return source;
	}

	public String getMessage() {
		return message;
	}
}
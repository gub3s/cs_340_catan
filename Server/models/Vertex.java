package models;

public class Vertex
{
	private VertexValue value;
	//private VertexLocation vertexLocation;
	
	public Vertex()	{
		value = new VertexValue();
		//vertexLocation = new VertexLocation();
	}

	public VertexValue getValue() {
		return value;
	}

//	public VertexLocation getVertexLocation() {
//		return vertexLocation;
//	}
}
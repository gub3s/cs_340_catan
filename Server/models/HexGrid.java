package models;

public class HexGrid
{
	private Hex[][] hexes;
	private int[] offsets;
	private int radius;
	private int x0;
	private int y0;
	
	public HexGrid() {
		hexes = new Hex[7][];
		hexes[0] = new Hex[4];
		hexes[1] = new Hex[5];
		hexes[2] = new Hex[6];
		hexes[3] = new Hex[7];
		hexes[4] = new Hex[6];
		hexes[5] = new Hex[5];
		hexes[6] = new Hex[4];
		offsets = new int[]{ 3, 2, 1, 0, 0, 0, 0 };
		radius = 3;
		x0 = 0;
		y0 = 0;
	}

	public Hex[][] getHexes() {
		return hexes;
	}
	
	public Hex getHex(int i, int j) {
		return hexes[i][j];
	}

	// Expects the coordinate to have i,j values, not actual x,y values
	public Hex getHex(Coordinate coord) {
		return hexes[coord.getX()][coord.getY()];
	}

	public int[] getOffsets() {
		return offsets;
	}

	public int getRadius() {
		return radius;
	}

	public int getX0() {
		return x0;
	}

	public int getY0() {
		return y0;
	}

	public void setRadius(int newRadius) {
		radius = newRadius;
	}

	public void setX0(int newX0) {
		x0 = newX0;
	}

	public void setY0(int newY0) {
		y0 = newY0;
	}

	public Coordinate convertCoordinateToIndex(int x, int y) {
		return convertCoordinateToIndex(new Coordinate(x, y));
	}
	
	public Coordinate convertCoordinateToIndex(Coordinate loc) {
		int i = loc.getY() + 3;
		int myOffset = 0;
		if (i > 3)
			myOffset = i - 3;
		int j = loc.getX() + loc.getY() - myOffset + 3;
		return new Coordinate(i, j);
	}
}
package models;

public class Edge
{
	private EdgeValue value;
	
	public Edge() {
		this.value = new EdgeValue();
	}

	/**
	 * @return the value
	 */
	public EdgeValue getValue() {
		return value;
	}
}
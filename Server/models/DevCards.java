package models;

public class DevCards
{
	// private variables
	private int yearOfPlenty;
	private int monopoly;
	private int soldier;
	private int roadBuilding;
	private int monument;
	
	public DevCards() {
		this.yearOfPlenty = 0;
		this.monopoly = 0;
		this.soldier	= 0;
		this.roadBuilding = 0;
		this.monument = 0;
	}

	/**
	 * @return the yearOfPlenty
	 */
	public int getYearOfPlenty() {
		return yearOfPlenty;
	}

	/**
	 * @param yearOfPlenty the yearOfPlenty to set
	 */
	public void setYearOfPlenty(int yearOfPlenty) {
		this.yearOfPlenty = yearOfPlenty;
	}
	
	/**
	 * Adds the specified amount of cards to this year of plenty
	 * @param amount The amount to add
	 */
	public void addYearOfPlenty(int amount) {
		yearOfPlenty += amount;
	}
	
	public void drawYearOfPlenty() {
		yearOfPlenty--;
	}
	
	public void incrementYearOfPlenty() {
		yearOfPlenty++;
	}

	/**
	 * @return the monopoly
	 */
	public int getMonopoly() {
		return monopoly;
	}
	
	/**
	 * @param monopoly the monopoly to set
	 */
	public void setMonopoly(int monopoly) {
		this.monopoly = monopoly;
	}
	
	public void addMonopoly(int amount) {
		monopoly += amount;
	}
	
	public void drawMonopoly() {
		monopoly--;
	}
	
	public void incrementMonopoly() {
		monopoly++;
	}

	/**
	 * @return the soldier
	 */
	public int getSoldier() {
		return soldier;
	}

	/**
	 * @param soldier the soldier to set
	 */
	public void setSoldier(int soldier) {
		this.soldier = soldier;
	}
	
	public void addSoldier(int amount) {
		soldier += amount;
	}
	
	public void drawSoldier() {
		soldier--;
	}
	
	public void incrementSoldier() {
		soldier++;
	}

	/**
	 * @return the roadBuilding
	 */
	public int getRoadBuilding() {
		return roadBuilding;
	}

	/**
	 * @param roadBuilding the roadBuilding to set
	 */
	public void setRoadBuilding(int roadBuilding) {
		this.roadBuilding = roadBuilding;
	}
	
	public void addRoadBuilding(int amount) {
		roadBuilding += amount;
	}
	
	public void drawRoadBuilding() {
		roadBuilding--;
	}
	
	public void incrementRoadBuilding() {
		roadBuilding++;
	}

	/**
	 * @return the monument
	 */
	public int getMonument() {
		return monument;
	}

	/**
	 * @param monument the monument to set
	 */
	public void setMonument(int monument) {
		this.monument = monument;
	}
	
	public void addMonument(int amount) {
		monument += amount;
	}
	
	public void drawMonument() {
		monument--;
	}
	
	public void incrementMonument() {
		monument++;
	}
}
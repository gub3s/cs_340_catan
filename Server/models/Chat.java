package models;

import java.util.ArrayList;

public class Chat
{
	// private variables
	private ArrayList<Line> lines;
	
	public Chat() {
		lines = null;
	}

	/**
	 * @return the lines
	 */
	public ArrayList<Line> getLines() {
		return lines;
	}

	/**
	 * @param lines the lines to set
	 */
	public void setLines(ArrayList<Line> lines) {
		this.lines = lines;
	}
	
	/**
	 * Adds a line into this chat object
	 * @param line The line to add
	 */
	public void addLine(Line line) {
		lines.add(line);
	}
}
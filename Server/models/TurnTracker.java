package models;

public class TurnTracker
{
	/*
	Possible Statuses
	*/
	public transient final String ROLLING = "Rolling";
	public transient final String PLAYING = "Playing";
	public transient final String ROBBING = "Robbing";
	public transient final String DISCARDING = "Discarding";
	public transient final String FIRST_ROUND = "FirstRound";
	public transient final String SECOND_ROUND = "SecondRound";
	
	// private variables
	private String status;
	private int currentTurn;

	public TurnTracker() {
		status = "INVALID";
		currentTurn = -1;
	}

	public String getStatus() {
		return status;
	}

	public int getCurrentTurn() {
		return currentTurn;
	}

	public void setStatus(String newStatus) {
		status = newStatus;
	}

	public void setCurrentTurn(int newCurrentTurn) {
		currentTurn = newCurrentTurn;
	}
	
	public void incrementTurn() {
		currentTurn++;
	}
	
	public void decrementTurn() {
		currentTurn--;
	}
	
	public void nextTurn() {
		currentTurn = (++currentTurn) % 4;
		status = ROLLING;
	}
	
	public boolean isFirstRound() {
		return status.equals(FIRST_ROUND);
	}
	
	public boolean isSecondRound() {
		return status.equals(SECOND_ROUND);
	}
}
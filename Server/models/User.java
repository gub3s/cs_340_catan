package models;

import java.io.Serializable;

public class User  implements Serializable {
	private String username;
	private String password;
	private int userID;
	
	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public User(String username, String password, int id) {
		this.username = username;
		this.password = password;
		this.userID = id;
	}

	public User(User user)
	{
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.userID = user.getUserID();
	}

	public String getUsername()
	{
		return this.username;
	}
	
	public void setUserID(int userID) {
		this.userID = userID;
	}
	
	public int getUserID() {
		return this.userID;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public int validateCredentials(String username, String password) {
		if (this.username.equals(username) && this.password.equals(password))
			return this.userID;
		else
			return -1;
	}
}
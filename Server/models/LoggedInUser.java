package models;

// Utility class just used for JSON serialization
public class LoggedInUser
{
	public String name;
	public int playerID;

	public LoggedInUser(String username, int id)
	{
		this.name = username;
		this.playerID = id;
	}
}
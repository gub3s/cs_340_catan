package models;

import java.util.ArrayList;

public class Log
{
	// private variables
	private ArrayList<Line> lines;
	
	public Log() {
		lines = new ArrayList<Line>();
	}

	public ArrayList<Line> getLines() {
		return lines;
	}
	
	public void addLine(String name, String message) {
		Line line = new Line(name, name + message);
		lines.add(line);
	}
}
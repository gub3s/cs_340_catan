package models;

public class VertexValue
{
	private int worth;
	private int ownerID;
	
	public VertexValue() {
		worth = 0;
		ownerID = -1;
	}

	public int getWorth() {
		return worth;
	}

	public int getOwnerID() {
		return ownerID;
	}

	public void setWorth(int newWorth) {
		worth = newWorth;
	}

	public void setOwnerID(int newOwnerID) {
		ownerID = newOwnerID;
	}
}
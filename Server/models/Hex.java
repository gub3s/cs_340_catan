package models;

import java.util.ArrayList;

public class Hex
{
	private boolean isLand;
	private String landtype;
	private Coordinate location;
	private ArrayList<Vertex> vertexes;
	private ArrayList<Edge> edges;
	
	public Hex() {
		this.isLand = false;
		this.location = new Coordinate();
		this.vertexes = new ArrayList<Vertex>();
		this.edges = new ArrayList<Edge>();
	}

	/**
	 * @return the isLand
	 */
	public boolean isLand() {
		return isLand;
	}

	/**
	 * @param isLand the isLand to set
	 */
	public void setIsLand(boolean isLand) {
		this.isLand = isLand;
	}

	/**
	 * @return the location
	 */
	public Coordinate getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(Coordinate location) {
		this.location = location;
	}

	/**
	 * @return the vertexes
	 */
	public ArrayList<Vertex> getVertexes() {
		return vertexes;
	}
	
	public Vertex getVertex(int index) {
		return vertexes.get(index);
	}

	/**
	 * @param vertexes the vertexes to set
	 */
	public void setVertexes(ArrayList<Vertex> vertexes) {
		this.vertexes = vertexes;
	}
	
	public void addVertex(Vertex vertex) {
		vertexes.add(vertex);
	}

	/**
	 * @return the edges
	 */
	public ArrayList<Edge> getEdges() {
		return edges;
	}
	
	public Edge getEdge(int index) {
		return edges.get(index);
	}

	/**
	 * @param edges the edges to set
	 */
	public void setEdges(ArrayList<Edge> edges) {
		this.edges = edges;
	}
	
	public void addEdge(Edge edge) {
		edges.add(edge);
	}

	public String getLandtype() {
		return this.landtype;
	}

	public void setLandtype(String lt) {
		this.landtype = lt;
	}
}
package models;

public class VertexLocation extends Coordinate {
	
	private String direction;
	
	public VertexLocation() {
		super();
		this.direction = null;
	}
	
	public VertexLocation(int x, int y, String dir) {
		super(x, y);
		this.direction = dir;
	}

	/**
	 * @return the direction
	 */
	public String getDirection() {
		return direction;
	}

	/**
	 * @param direction the direction to set
	 */
	public void setDirection(String direction) {
		this.direction = direction;
	}
}

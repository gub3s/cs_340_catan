package models;

public class EdgeLocation extends Coordinate {
	
	private String direction;
	
	public EdgeLocation() {
		super();
		direction = null;
	}
	
	public EdgeLocation(int x, int y, String dir) {
		super(x, y);
		direction = dir;
	}

	/**
	 * @return the direction
	 */
	public String getDirection() {
		return direction;
	}

	/**
	 * @param direction the direction to set
	 */
	public void setDirection(String direction) {
		this.direction = direction;
	}
}

package models;

public class Port
{
	private int ratio;
	private DirCoordinate validVertex1;
	private DirCoordinate validVertex2;
	private String orientation;
	private Coordinate location;
	
	public Port() {
		ratio = 4;
		validVertex1 = new DirCoordinate();
		validVertex2 = new DirCoordinate();
		orientation = "INVALID";
		location = new Coordinate();
	}

	public int getRatio() {
		return ratio;
	}

	public DirCoordinate getValidVertex1() {
		return validVertex1;
	}

	public DirCoordinate getValidVertex2() {
		return validVertex2;
	}

	public String getOrientation() {
		return orientation;
	}

	public Coordinate getLocation() {
		return location;
	}

	public void setRatio(int newRatio) {
		ratio = newRatio;
	}

	public void setOrientation(String newOrientation) {
		orientation = newOrientation;
	}
}
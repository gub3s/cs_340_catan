package models;

public class Resources
{
	private int brick;
	private int wood;
	private int sheep;
	private int wheat;
	private int ore;
	
	public Resources() {
		brick = 0;
		wood = 0;
		sheep = 0;
		wheat = 0;
		ore = 0;
	}

	public int getBrick() {
		return brick;
	}

	public int getWood() {
		return wood;
	}

	public int getSheep() {
		return sheep;
	}

	public int getWheat() {
		return wheat;
	}

	public int getOre() {
		return ore;
	}

	public void setBrick(int newBrick) {
		brick = newBrick;
	}

	public void setWood(int newWood) {
		wood = newWood;
	}

	public void setSheep(int newSheep) {
		sheep = newSheep;
	}

	public void setWheat(int newWheat) {
		wheat = newWheat;
	}

	public void setOre(int newOre) {
		ore = newOre;
	}
	
	public void addBrick(int amount) {
		brick += amount;
	}
	
	public void addWood(int amount) {
		wood += amount;
	}
	
	public void addSheep(int amount) {
		sheep += amount;
	}
	
	public void addWheat(int amount) {
		wheat += amount;
	}
	
	public void addOre(int amount) {
		ore += amount;
	}

	public void subtractBrick(int amount) {
		brick -= amount;
	}
	
	public void subtractWood(int amount) {
		wood -= amount;
	}
	
	public void subtractSheep(int amount) {
		sheep -= amount;
	}
	
	public void subtractWheat(int amount) {
		wheat -= amount;
	}
	
	public void subtractOre(int amount) {
		ore -= amount;
	}
	
	public void removeBrick(int amount) {
		brick -= amount;
	}
	
	public void removeWood(int amount) {
		wood -= amount;
	}
	
	public void removeSheep(int amount) {
		sheep -= amount;
	}
	
	public void removeWheat(int amount) {
		wheat -= amount;
	}
	
	public void removeOre(int amount) {
		ore -= amount;
	}
	
	public void decrementBrick() {
		brick--;
	}
	
	public void decrementWood() {
		wood--;
	}
	
	public void decrementSheep() {
		sheep--;
	}
	
	public void decrementWheat() {
		wheat--;
	}
	
	public void decrementOre() {
		ore--;
	}
	
	public int getTotal() {
		return brick + wood + sheep + wheat + ore;
	}
}
package models;

public class Player
{
	public static final int MAX_GAME_POINTS = 10;

	// private variables
	private Resources resources = new Resources();
	/* Dev cards that can be played */
	private DevCards oldDevCards = new DevCards();
	/* Dev cards that were just drawn and can't be played */
	private DevCards newDevCards = new DevCards();
	/* How many they can still build */
	private int roads = 15;
	/* How many they can still build */
	private int cities = 4;
	/* How many they can still build */
	private int settlements = 5; 
	/* How many soldier cards the player has played */
	private int soldiers = 0;
	private int victoryPoints = 0;
	/* How many monument cards the player has */
	private int monuments = 0;
	private boolean longestRoad;
	private boolean largestArmy;
	private boolean playedDevCard;
	private boolean discarded;
	private int playerID = -1;
	private int orderNumber = -1;
	private String name;
	private String color = "INVALID";
	public transient boolean isAI;

	public Player(boolean ai)
	{
		this.isAI = ai;
	}

	public Player() { }

	public Resources getResources() {
		return resources;
	}

	public DevCards getOldDevCards() {
		return oldDevCards;
	}

	public DevCards getNewDevCards() {
		return newDevCards;
	}

	public int getRoads() {
		return roads;
	}

	public int getCities() {
		return cities;
	}

	public int getSettlements() {
		return settlements;
	}

	public int getSoldiers() {
		return soldiers;
	}

	public int getVictoryPoints() {
		return victoryPoints;
	}

	public int getMonuments() {
		return monuments;
	}

	public boolean getLongestRoad() {
		return longestRoad;
	}

	public boolean getLargestArmy() {
		return largestArmy;
	}

	public boolean getPlayedDevCard() {
		return playedDevCard;
	}

	public boolean getDiscarded() {
		return discarded;
	}

	public int getID() {
		return playerID;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public String getName() {
		return name;
	}

	public String getColor() {
		return color;
	}

	public void setRoads(int newRoads) {
		roads = newRoads;
	}
	
	public void decrementRoads() {
		roads--;
	}

	public void setCities(int newCities) {
		cities = newCities;
	}
	
	public void decrementCities() {
		cities--;
	}

	public void setSettlements(int newSettlements) {
		settlements = newSettlements;
	}
	
	public void incrementSettlements() {
		settlements++;
	}
	
	public void decrementSettlements() {
		settlements--;
	}

	public void setSoldiers(int newSoldiers) {
		soldiers = newSoldiers;
	}

	public void setVictoryPoints(int newVictoryPoints) {
		victoryPoints = newVictoryPoints;
	}
	
	public void incrementVictoryPoints() {
		victoryPoints++;
	}
	
	public void decrementVictoryPoints() {
		victoryPoints--;
	}

	public void setMonumnets(int newMonuments) {
		monuments = newMonuments;
	}

	public void setLongestRoad(boolean newLongestRoad) {
		longestRoad = newLongestRoad;
	}

	public void setLargestArmy(boolean newLargestArmy) {
		largestArmy = newLargestArmy;
	}

	public void setPlayedDevCard(boolean newPlayedDevCard) {
		playedDevCard = newPlayedDevCard;
	}

	public void setDiscarded(boolean newDiscarded) {
		discarded = newDiscarded;
	}

	public void setPlayerID(int newPlayerID) {
		playerID = newPlayerID;
	}

	public void setOrderNumber(int newOrderNumber) {
		orderNumber = newOrderNumber;
	}

	public void setName(String newName) {
		name = newName;
	}

	public void setColor(String newColor) {
		color = newColor;
	}
	
	public void emptyNewDevCards() {
		newDevCards = new DevCards();
	}
}
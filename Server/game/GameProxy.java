package game;

import java.util.ArrayList;
import com.google.gson.Gson;
import models.Game;
import commands.*;
import commands.real.*;
import commands.mock.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import commands.*;

/**
 * This class is basically a facade between the game logic
 * and the server handlers. It should know nothing of the
 * server or its handlers, and should only know how to
 * interface with the GameManager and other model classes.
**/
public class GameProxy
{
	private static ICommand command;
	
	public static String resetGame(int gameID)
	{
		String response = "";
		try {
			// TODO...
			command = new MockResetGameCommand();
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String performCommands(String[] commands)
	{
		String response = "";
		try {
			// TODO...
			command = new MockPerformCommandsCommand(commands);
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String getCommands()
	{
		String response = "";
		try {
			// TODO...
			command = new MockGetCommandsCommand();
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	// public static String addAI(String type)
	// {
	// 	String response = "";
	// 	try {
	// 	// add the AI to the game
	// 		command = new MockAddAICommand(type);
	// 		response = command.execute();
	// 	} catch (Exception e) {
	// 		e.printStackTrace();
	// 		response = "0";
	// 	} finally {
	// 		return response;
	// 	}
	// }
	
	// public static String listAI()
	// {
	// 	String response = "";
	// 	try {
	// 	//return a list of AI players for the game
	// 		command = new RealListAICommand();
	// 		response = command.execute();
	// 	} catch (Exception e) {
	// 		e.printStackTrace();
	// 		response = "0";
	// 	} finally {
	// 		return response;
	// 	}
	// }
	
	// public static String changeLogLevel(String level)
	// {
	// 	String response = "";
	// 	try {
	// 		// update the log level
	// 		command = new MockChangeLogLevelCommand(level);
	// 		response = command.execute();
	// 	} catch (Exception e) {
	// 		e.printStackTrace();
	// 		response = "0";
	// 	} finally {
	// 		return response;
	// 	}
	// }
	
	public static String sendChat(SendChatCommand command)
	{
		String response = "";
		try {
			// send chat
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String rollNumber(RollNumberCommand command)
	{
		String response = "";
		try {
			// roll number
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String robPlayer(RobPlayerCommand command)
	{
		String response = "";
		try {
			// rob player
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String finishTurn(FinishTurnCommand command)
	{
		String response = "";
		try {
			// finish turn
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String buyDevCard(BuyDevCardCommand command)
	{
		String response = "";
		try {
			// buy dev card
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String yearOfPlenty(YearOfPlentyCommand command)
	{
		String response = "";
		try {
			// year of plenty
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String roadBuilding(RoadBuildingCommand command)
	{
		String response = "";
		try {
			// road building
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String soldier(SoldierCommand command)
	{
		String response = "";
		try {
			// soldier
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String monopoly(MonopolyCommand command)
	{
		String response = "";
		try {
			// monopoly
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String monument(MonumentCommand command)
	{
		String response = "";
		try {
			// monument
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String buildRoad(BuildRoadCommand cmd)
	{
		String response = "";
		try {
			// build road
			response = cmd.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String buildSettlement(BuildSettlementCommand cmd)
	{
		String response = "";
		try {
			// build settlement
			response = cmd.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}

	public static String buildCity(BuildCityCommand command)
	{
		String response = "";
		try {
			// build city
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String offerTrade(OfferTradeCommand command)
	{
		String response = "";
		try {
			// offer Trade
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String acceptTrade(AcceptTradeCommand command)
	{
		String response = "";
		try {
			// accept trade
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String maritimeTrade(MaritimeTradeCommand command)
	{
		String response = "";
		try {
			// maritime trade
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
	
	public static String discardCards(DiscardCardsCommand command)
	{
		String response = "";
		try {
			// discard cards
			response = command.execute();
		} catch (Exception e) {
			e.printStackTrace();
			response = "0";
		} finally {
			return response;
		}
	}
}
package game;

import models.Game;
import models.User;
import models.GameMeta;
import models.Player;
import persistence.IPersistentStore;
import persistence.UserDTO;
import persistence.GameDTO;

import java.io.FileReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import commands.FinishTurnCommand;
import commands.real.RealFinishTurnCommand;

import com.google.gson.Gson;
import java.util.List;

/**
 * This whole class is static so that it acts like a glorified
 * global variable. Basically, it provides access to all the games
 * and users.
**/
public class GameManager
{
	private static ArrayList<Game> games = new ArrayList<Game>();
	private static ArrayList<User> users = new ArrayList<User>();
	private static IPersistentStore persistence = null;

	public static String[] aiNames = {
		"Don't Trust The Specs",
		"Why Do I Have an F So Far",
		"So Dislike",
		"Very Project Deadline",
		"TA's Client Is Broken",
		"How Not To Write Specs",
		"HELLLPPPPPP!!!",
		"My Hands Are Typing Words",
		"HAAAAANNNNDS",
		"asdf blah blah",
		"Wow. Such fail.",
		"What are the requirements, again?",
		"AIs just roll and end turn",
		"Too windy outside",
		"BACON",
		"Bonus points for AI"
	};

	/**
	 * Can't be constructed (everything will be static; we only need one of these)
	**/
	private GameManager() { }

	/**
	 * Logs a user in.
	 * @return The user's ID if successful, otherwise -1
	**/
	public static int loginUser(String username, String password)
	{
		int userID = -1;
		for (User user : users) {
			userID = user.validateCredentials(username, password);
			if (userID != -1) {
				break;
			}
		}
		return userID;
	}
	
	/**
	 * Registers a user.
	 * @return The user's ID if successful, otherwise -1.
	**/
	public static int registerUser(String username, String password)
	{
		for (User user : users) {
			System.out.println(user.getUsername());
			if (user.getUsername().equals(username)) {
				// Duplicate username; register fails
				System.out.println("Duplicate found");
				return -1;
			}
		}
		User user = new User(username, password);
		user.setUserID(users.size());
		users.add(user);
		persistence.addUser(user);
		return user.getUserID();
	}

	public static int getNumGames()
	{
		return games.size();
	}
	
	/**
	 * @return The game by the ID
	**/
	public static Game getGame(int gameID)
	{
		return games.get(gameID);
	}

	/**
	 * Gets a list of the games
	 * @return The list of games
	**/
	public static ArrayList<GameMeta> listGames()
	{
		ArrayList<GameMeta> list = new ArrayList<GameMeta>();
		for (Game g : games)
		{
			GameMeta meta = new GameMeta(g.getName(), g.getID());
			for (Player p : g.getPlayers())
				meta.addPlayer(p);
			meta.addEmptyPlayers();
			list.add(meta);
		}
		return list;
	}

	/**
	 * Creates a game
	 * @return The new game's ID
	**/
	public static GameMeta createGame(boolean randomTiles, boolean randomNumbers, boolean randomPorts, String name)
	{
		Game game = new Game(name, games.size());
		GameMeta meta = new GameMeta(name, game.getID());
		
		meta.addEmptyPlayers();	// Hack... see GameMeta.java for details

		try
		{
			File file = new File("server/game/default.json");
			game = new Gson().fromJson(new FileReader(file), Game.class);
		}
		catch (FileNotFoundException e)
		{
			System.out.println("***Crash*** Default server model not found; using uninitialized (empty) game");
		}
		finally
		{
			game.setName(name);
			game.setID(games.size());
			games.add(game);
		}
		
		assert(game != null);
		assert(persistence != null);
		persistence.addGame(game);

		return meta;
	}

	public static boolean joinGame(String color, int gameID, int playerID, String playerName, boolean isAi)
	{
		Game game = GameManager.getGame(gameID);

		// Is player already in the game?
		ArrayList<Player> gamePlayers = game.getPlayers();
		for (Player player : gamePlayers)
			if (player.getName().equals(playerName))
				return true;

		if (gamePlayers.size() < 4)
		{
			Player newPlayer = new Player(isAi);
			newPlayer.setPlayerID(playerID);
			newPlayer.setName(playerName);
			newPlayer.setColor(color);
			newPlayer.setOrderNumber(gamePlayers.size());
			game.addPlayer(newPlayer);

			if (gamePlayers.size() == 4
				&& game.getCurrentPlayer().isAI
				&& !game.isAllAi())
			{
				// Runs through the AIs until it's the real player's turn
				FinishTurnCommand cmd = new RealFinishTurnCommand();
				cmd.playerIndex = game.getTurnTracker().getCurrentTurn();
				cmd.gameID = gameID;
				GameProxy.finishTurn(cmd);
			}
			return true;
		}

		return false;
	}

	public static IPersistentStore getPersistence()
	{
		return persistence;
	}

	public static void setPersistence(IPersistentStore newStore)
	{
		persistence = newStore;
		persistence.init();
	}
	
	public static void loadSavedData() {
		List<UserDTO> userDtos = persistence.getAllUsers();
		for (UserDTO userDto : userDtos) {
			User user = new User(userDto);
			users.add(user);
		}
		
		List<GameDTO> gameDtos = persistence.getAllGames();
		for (GameDTO gameDto : gameDtos) {
			Game game = new Game(gameDto);
			games.add(game);
		}
	}
}
package server;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import com.sun.net.httpserver.HttpServer;

import handlers.*;
import game.GameManager;
import models.Game;
import java.util.jar.*;
import java.io.*;
import java.util.Map;
import persistence.RelationalStorage;
import persistence.FileStorage;


public class CatanServer
{
	private static final int DEFAULT_PORT = 8081;
	private static final int MAX_WAITING_CONNECTIONS = 10;
	private static int port;
	private static String gameplayRoot;
	private HttpServer server;

	/**
	 * Entry point
	 * @param args
	 */
	public static void main(String[] args) throws IOException
	{
		if (args.length < 1)
		{
			System.out.println("No port specified (-Dargs.port=8081)... using default: " + Integer.toString(DEFAULT_PORT));
			CatanServer.port = DEFAULT_PORT;
		}
		else
		{
			if (args.length == 5)
			{
				if (args[3].equals("null"))
				{
					System.out.println("Ignore persistence requested");
				}
				else if (args[3].equals("sql"))
				{
					GameManager.setPersistence(new RelationalStorage());
					System.out.println("Loading relational persistence plugin");
				}
				else if (args[3].equals("file"))
				{
					GameManager.setPersistence(new FileStorage());
					System.out.println("Loading file persistence plugin");
				}
				else
				{
					GameManager.setPersistence(null);
					System.out.println("Unrecognized persistence plugin requested");
				}
			}
			else
			{
				System.out.println("Running without persistence");
			}
			
			if (args[4].equals("reset")) {
				GameManager.getPersistence().resetDatabase();
				System.out.println("Reset Persistence: " + args[3]);
				return;
			}

			try
			{
				CatanServer.port = Integer.parseInt(args[0]);
			}
			catch (NumberFormatException e)
			{
				System.err.println("Bad port number... using default: " + Integer.toString(DEFAULT_PORT));
				CatanServer.port = DEFAULT_PORT;
			}
		}

		// (Root path stuff is a little sensitive; be careful if changing it)
		CatanServer.gameplayRoot = new File(args[1]).getCanonicalPath().toString();

		// Set up server defaults for convenience
		if (GameManager.getPersistence() == null) {
			CatanServer.doStuffs();
		}
		else {
			GameManager.loadSavedData();
		}

		new CatanServer().run(CatanServer.port);
	}

	/**
	 * Initializes the GameManager with some default players/games
	**/
	private static void doStuffs()
	{
		int defaultGameCount = 5;
		GameManager.registerUser("Sam", "sam");
		GameManager.registerUser("Brooke", "brooke");
		GameManager.registerUser("Pete", "pete");
		GameManager.registerUser("Mark", "mark");

		for (int i = 0; i < defaultGameCount; i++)
			GameManager.createGame(false, false, false, "Default Game " + i);

		for (int i = 0; i < GameManager.getNumGames(); i++)
		{
			Game game = GameManager.getGame(i);
			for (int j = 0; j < GameManager.getNumGames() - i - 1; j++)
				GameManager.joinGame(game.chooseAiColor(), i, -2, game.chooseAiPlayerName(GameManager.aiNames), true);
		}
	}
	
	/**
	 * Initializes and starts the server listening on a port
	 */
	private void run(int port)
	{
		try
		{
			// Server definition
			this.server = HttpServer.create(new InetSocketAddress(port), MAX_WAITING_CONNECTIONS);
		}
		catch (IOException e)
		{
			System.err.println("Could not create HTTP server: " + e.getMessage());
			e.printStackTrace();
			return;
		}

		// Establish endpoints
		this.wireup();

		// Begin multi-threaded server (note: our games aren't mutexed...)
		this.server.setExecutor(null);	// Use default thread manager	TODO: Executors.newCachedThreadPool()?
		this.server.start();

		System.out.println("Server listening on port " + port);
	}

	private void wireup()
	{
		// Debug endpoint
		this.server.createContext("/about", new BaseHandler());


		// Static file endpoints
		this.server.createContext("/docs", new FileHandler(CatanServer.gameplayRoot + "/.."));
		this.server.createContext("/", new FileHandler(CatanServer.gameplayRoot));


		// API endpoints
		this.server.createContext("/user/login", new UserLoginHandler());
		this.server.createContext("/user/register", new UserRegisterHandler());

		this.server.createContext("/games/list", new GamesListHandler());
		this.server.createContext("/games/create", new GamesCreateHandler());
		this.server.createContext("/games/join", new GamesJoinHandler());

		this.server.createContext("/game/model", new GameModelHandler());
		this.server.createContext("/game/reset", new GameResetHandler());
		this.server.createContext("/game/commands", new GameCommandsHandler());
		this.server.createContext("/game/addAI", new GameAddAIHandler());
		this.server.createContext("/game/listAI", new GameListAIHandler());

		this.server.createContext("/util/changeLogLevel", new UtilChangeLogLevelHandler());

		this.server.createContext("/moves/sendChat", new MovesSendChatHandler());
		this.server.createContext("/moves/rollNumber", new MovesRollNumberHandler());
		this.server.createContext("/moves/robPlayer", new MovesRobPlayerHandler());
		this.server.createContext("/moves/finishTurn", new MovesFinishTurnHandler());
		this.server.createContext("/moves/buyDevCard", new MovesBuyDevCardHandler());
		this.server.createContext("/moves/Year_of_Plenty", new MovesYearOfPlentyHandler());
		this.server.createContext("/moves/Road_Building", new MovesRoadBuildingHandler());
		this.server.createContext("/moves/Soldier", new MovesSoldierHandler());
		this.server.createContext("/moves/Monopoly", new MovesMonopolyHandler());
		this.server.createContext("/moves/Monument", new MovesMonumentHandler());
		this.server.createContext("/moves/buildRoad", new MovesBuildRoadHandler());
		this.server.createContext("/moves/buildSettlement", new MovesBuildSettlementHandler());
		this.server.createContext("/moves/buildCity", new MovesBuildCityHandler());
		this.server.createContext("/moves/offerTrade", new MovesOfferTradeHandler());
		this.server.createContext("/moves/acceptTrade", new MovesAcceptTradeHandler());
		this.server.createContext("/moves/maritimeTrade", new MovesMaritimeTradeHandler());
		this.server.createContext("/moves/discardCards", new MovesDiscardCardsHandler());
	}
}

package modules;

import com.google.inject.AbstractModule;
import commands.*;
import commands.real.*;

public class RealCommandModule extends AbstractModule {
	@Override
	protected void configure() {
		bind(AcceptTradeCommand.class).to(RealAcceptTradeCommand.class);
		bind(BuildCityCommand.class).to(RealBuildCityCommand.class);
		bind(BuildRoadCommand.class).to(RealBuildRoadCommand.class);
		bind(BuildSettlementCommand.class).to(RealBuildSettlementCommand.class);
		bind(BuyDevCardCommand.class).to(RealBuyDevCardCommand.class);
		bind(DiscardCardsCommand.class).to(RealDiscardCardsCommand.class);
		bind(FinishTurnCommand.class).to(RealFinishTurnCommand.class);
		bind(GetCommandsCommand.class).to(RealGetCommandsCommand.class);
		bind(MaritimeTradeCommand.class).to(RealMaritimeTradeCommand.class);
		bind(MonopolyCommand.class).to(RealMonopolyCommand.class);
		bind(MonumentCommand.class).to(RealMonumentCommand.class);
		bind(OfferTradeCommand.class).to(RealOfferTradeCommand.class);
		bind(PerformCommandsCommand.class).to(RealPerformCommandsCommand.class);
		bind(ResetGameCommand.class).to(RealResetGameCommand.class);
		bind(RoadBuildingCommand.class).to(RealRoadBuildingCommand.class);
		bind(RobPlayerCommand.class).to(RealRobPlayerCommand.class);
		bind(RollNumberCommand.class).to(RealRollNumberCommand.class);
		bind(SendChatCommand.class).to(RealSendChatCommand.class);
		bind(SoldierCommand.class).to(RealSoldierCommand.class);
		bind(YearOfPlentyCommand.class).to(RealYearOfPlentyCommand.class);
	}
}
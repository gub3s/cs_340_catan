package modules;

import com.google.inject.AbstractModule;
import commands.*;
import commands.mock.*;

public class MockCommandModule extends AbstractModule {
	@Override
	protected void configure() {
		bind(AcceptTradeCommand.class).to(MockAcceptTradeCommand.class);
		bind(BuildCityCommand.class).to(MockBuildCityCommand.class);
		bind(BuildRoadCommand.class).to(MockBuildRoadCommand.class);
		bind(BuildSettlementCommand.class).to(MockBuildSettlementCommand.class);
		bind(BuyDevCardCommand.class).to(MockBuyDevCardCommand.class);
		bind(DiscardCardsCommand.class).to(MockDiscardCardsCommand.class);
		bind(FinishTurnCommand.class).to(MockFinishTurnCommand.class);
		bind(GetCommandsCommand.class).to(MockGetCommandsCommand.class);
		bind(MaritimeTradeCommand.class).to(MockMaritimeTradeCommand.class);
		bind(MonopolyCommand.class).to(MockMonopolyCommand.class);
		bind(MonumentCommand.class).to(MockMonumentCommand.class);
		bind(OfferTradeCommand.class).to(MockOfferTradeCommand.class);
		bind(PerformCommandsCommand.class).to(MockPerformCommandsCommand.class);
		bind(ResetGameCommand.class).to(MockResetGameCommand.class);
		bind(RoadBuildingCommand.class).to(MockRoadBuildingCommand.class);
		bind(RobPlayerCommand.class).to(MockRobPlayerCommand.class);
		bind(RollNumberCommand.class).to(MockRollNumberCommand.class);
		bind(SendChatCommand.class).to(MockSendChatCommand.class);
		bind(SoldierCommand.class).to(MockSoldierCommand.class);
		bind(YearOfPlentyCommand.class).to(MockYearOfPlentyCommand.class);
	}
}
Settlers of Catan
==================

Welcome. This project was a flop the first semester they taught it. Good thing our group is awesome!

Have fun!


Instructions
=============

1. Install ant
2. To run *their* (the TA's) server, run: `ant server`
3. To run *our* (group) server, run: `ant our-server`
4. To generate Javascript docs, run: `ant make-js-doc`


*Our* Server
-------------

If you need to build our server, run `ant build-our-server`.

To specify a port when running our server, use the `-Dargs.port=` argument.



Directory structure
--------------------

It's kind of a mess; that's the TA's/professor's fault.

- `docs` contains the YUI and Swagger documentation (Javascript and server API)
- `gameplay` contains the the client files (the actual Javascript front-end)
- `sample` contains example JSON files
- `server` contains our server (Java back-end)

Our server serves up static files under `docs` and `gameplay`.


Tips
-----

Don't pay attention to the TA's docs. They're misleading and incomplete most of the time. For that matter, don't pay attention to ours, either, since it's probably not current.
drop table if exists users;
drop table if exists games;
drop table if exists commands;

create table users
(
	id integer not null primary key unique,
	player text not null unique
);

create table games
(
	id integer not null primary key unique,
	game text not null unique
);

create table commands
(
	id integer not null primary key autoincrement,
	gameId integer not null,
	revision integer not null,
	command text not null
);